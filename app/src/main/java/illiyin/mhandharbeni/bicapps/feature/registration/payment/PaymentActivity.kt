package illiyin.mhandharbeni.bicapps.feature.registration.payment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.animation.AnimationUtils
import android.widget.Toast
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.util.DialogBuilder
import illiyin.mhandharbeni.bicapps.util.RequestCode
import kotlinx.android.synthetic.main.activity_payment.*
import kotlinx.android.synthetic.main.partial_payment_confirmation.*
import kotlinx.android.synthetic.main.partial_payment_information.*

class PaymentActivity : AppCompatActivity(), PaymentContract.View {

    override lateinit var presenter: PaymentPresenter
    private var mViewState = STATE_INFORMATION

    companion object {
        private const val STATE_INFORMATION = 11
        private const val STATE_CONFIRMATION = 12
        private const val EXTRA_TITLE = "title"
        private const val EXTRA_SUBTITLE = "subtitle"

        fun getIntent(context: Context, title: String, subtitle: String): Intent {
            val intent = Intent(context, PaymentActivity::class.java)
            intent.putExtra(EXTRA_TITLE, title)
            intent.putExtra(EXTRA_SUBTITLE, subtitle)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)
        setupActionBar()

        presenter = PaymentPresenter(this)

        view_flipper.isAutoStart = false

        button_later.setOnClickListener { finish() }
        button_confirm.setOnClickListener { showConfirmationPage() }
        button_upload.setOnClickListener { presenter.pickImageFromStorage() }
        button_finish.setOnClickListener { presenter.uploadPayment() }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RequestCode.PICK_PHOTO && resultCode == Activity.RESULT_OK) {
            presenter.processSelectedImage(data!!)
        } else super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onBackPressed() {
        if (mViewState == STATE_CONFIRMATION) showInformationPage()
        else super.onBackPressed()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onUploadSucceed() {
        DialogBuilder(this).showInformationToast(
                R.string.general_msg_under_development,
                Toast.LENGTH_LONG
        )
        finish()
    }

    override fun onUploadFailed(messageRes: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun openStorage() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "image/*"
        startActivityForResult(Intent.createChooser(
                intent,
                getString(R.string.general_action_pilih_gambar)
        ), RequestCode.PICK_PHOTO)
    }

    override fun showPaymentImagePath(path: String) {
        text_image_path.text = path
    }

    override fun setLoadingIndicator(active: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun setupActionBar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = intent.getStringExtra(EXTRA_TITLE)
        supportActionBar?.subtitle = intent.getStringExtra(EXTRA_SUBTITLE)
    }

    private fun showInformationPage() {
        with(view_flipper) {
            inAnimation = AnimationUtils.loadAnimation(
                    this@PaymentActivity,
                    R.anim.in_from_top
            )
            outAnimation = AnimationUtils.loadAnimation(
                    this@PaymentActivity,
                    R.anim.out_to_bottom
            )

            showPrevious()
        }

        mViewState = STATE_INFORMATION
    }

    private fun showConfirmationPage() {
        with(view_flipper) {
            inAnimation = AnimationUtils.loadAnimation(
                    this@PaymentActivity,
                    R.anim.in_from_bottom
            )
            outAnimation = AnimationUtils.loadAnimation(
                    this@PaymentActivity,
                    R.anim.out_to_top
            )

            showNext()
        }

        mViewState = STATE_CONFIRMATION
    }
}
