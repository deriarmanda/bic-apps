package illiyin.mhandharbeni.bicapps.feature.mybic.evaluasi.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.model.Question
import illiyin.mhandharbeni.bicapps.data.repository.EvaluasiRepository
import illiyin.mhandharbeni.bicapps.feature.test.TestActivity
import illiyin.mhandharbeni.bicapps.util.DialogBuilder
import illiyin.mhandharbeni.bicapps.util.TestType
import illiyin.mhandharbeni.bicapps.util.TimeFormatter
import kotlinx.android.synthetic.main.base_activity_init_test.*
import java.util.*

class DetailEvaluasiActivity : AppCompatActivity(), DetailEvaluasiContract.View {

    override lateinit var presenter: DetailEvaluasiPresenter

    private lateinit var mSelectedEvaluasi: String
    private lateinit var mLoadingDialog: DialogBuilder

    companion object {
        private const val EXTRA_SELECTED_EVALUASI = "evaluasi"

        fun getIntent(context: Context, evaluasi: String): Intent {
            val intent = Intent(context, DetailEvaluasiActivity::class.java)
            intent.putExtra(EXTRA_SELECTED_EVALUASI, evaluasi)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.base_activity_init_test)

        mSelectedEvaluasi = intent.getStringExtra(EXTRA_SELECTED_EVALUASI)
        presenter = DetailEvaluasiPresenter(
                this,
                EvaluasiRepository.getInstance(this),
                mSelectedEvaluasi
        )
        mLoadingDialog = DialogBuilder(this)
                .buildProgressDialog(R.string.test_msg_loading)

        supportActionBar?.title = mSelectedEvaluasi
        with(button_mulai) {
            text = getString(R.string.test_action_mulai, mSelectedEvaluasi)
            setOnClickListener { presenter.goToTestPage() }
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onLoadListQuestionSucceed(duration: Long) {
        text_rules.text = getString(
                R.string.test_msg_rules,
                mSelectedEvaluasi,
                TimeFormatter.millisToMinutes(duration)
        )
    }

    override fun onLoadListQuestionFailed(messageRes: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun openTestPage(type: TestType, list: ArrayList<Question>) {
        startActivity(TestActivity.getIntent(
                this,
                type,
                list
        ))
    }

    override fun setLoadingIndicator(active: Boolean) {
        if (active) mLoadingDialog.show()
        else mLoadingDialog.dismiss()
    }
}
