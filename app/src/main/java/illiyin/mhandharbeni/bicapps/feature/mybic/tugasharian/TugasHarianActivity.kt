package illiyin.mhandharbeni.bicapps.feature.mybic.tugasharian

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.repository.TugasHarianRepository
import illiyin.mhandharbeni.bicapps.feature.mybic.tugasharian.TugasHarianRvAdapter.OnItemClickListener
import illiyin.mhandharbeni.bicapps.feature.mybic.tugasharian.sublist.SubTugasHarianActivity
import kotlinx.android.synthetic.main.base_activity_list.*

class TugasHarianActivity : AppCompatActivity(), TugasHarianContract.View {

    override lateinit var presenter: TugasHarianPresenter

    private val mItemClickListener = object : OnItemClickListener {
        override fun onItemClick(tugasHarian: String) {
            presenter.goToSubListPage(tugasHarian)
        }
    }
    private val mAdapter = TugasHarianRvAdapter(this, mItemClickListener)

    companion object {
        fun getIntent(context: Context) = Intent(context, TugasHarianActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.base_activity_list)

        presenter = TugasHarianPresenter(this, TugasHarianRepository.getInstance(this))

        with(refresh_layout) {
            setColorSchemeResources(R.color.primary)
            setOnRefreshListener { presenter.loadList() }
        }
        with(recycler_view) {
            layoutManager = LinearLayoutManager(this@TugasHarianActivity)
            adapter = mAdapter
        }
        text_empty_list.setText(R.string.tugas_harian_msg_empty)
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun showList(list: List<String>) {
        mAdapter.mTugasHarianList = list
        text_empty_list.visibility = View.GONE
    }

    override fun showEmptyListMessage() {
        text_empty_list.visibility = View.VISIBLE
    }

    override fun onLoadListFailed(messageRes: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun openSubListPage(tugasHarian: String) {
        startActivity(SubTugasHarianActivity.getIntent(this, tugasHarian))
    }

    override fun setLoadingIndicator(active: Boolean) {
        refresh_layout.isRefreshing = active
    }
}