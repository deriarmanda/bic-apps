package illiyin.mhandharbeni.bicapps.feature.signup

import android.support.annotation.StringRes
import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView

interface SignUpContract {

    interface Presenter : BasePresenter {

        fun validateForm(fullname: String, username: String, email: String, password: String)
        fun signUp(fullname: String, username: String, email: String, password: String)
        fun goToSignInPage()
    }

    interface View : BaseView<Presenter> {

        fun onValidationSucceed()
        fun onFullnameValidationError(@StringRes messageRes: Int)
        fun onUsernameValidationError(@StringRes messageRes: Int)
        fun onEmailValidationError(@StringRes messageRes: Int)
        fun onPasswordValidationError(@StringRes messageRes: Int)
        fun onSignUpSucceed(name: String)
        fun onSignUpFailed(message: String)
        fun openSignInPage()
    }
}