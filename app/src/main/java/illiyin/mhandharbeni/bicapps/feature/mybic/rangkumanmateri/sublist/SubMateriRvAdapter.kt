package illiyin.mhandharbeni.bicapps.feature.mybic.rangkumanmateri.sublist

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import illiyin.mhandharbeni.bicapps.R
import kotlinx.android.synthetic.main.base_item_list_bimbel.view.*

class SubMateriRvAdapter(
        private val mContext: Context,
        private val mClickListener: OnItemClickListener
) : RecyclerView.Adapter<SubMateriRvAdapter.ItemViewHolder>() {

    var mSubMateriList: List<String> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.base_item_list_bimbel, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.fetch(mSubMateriList[position])
    }

    override fun getItemCount() = mSubMateriList.size

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun fetch(subMateri: String) {
            itemView.text_title.text = subMateri
            itemView.text_subtitle.text = mContext.getString(
                    R.string.sub_materi_subtitle_list,
                    subMateri
            )

            itemView.setOnClickListener { mClickListener.onItemClick(subMateri) }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(subMateri: String)
    }
}