package illiyin.mhandharbeni.bicapps.feature.mybic.bukupretes.sublist

import android.support.annotation.StringRes
import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView

interface SubPretesContract {

    interface Presenter : BasePresenter {

        fun loadSubList()
        fun goToDetailPage(subPretes: String)
    }

    interface View : BaseView<Presenter> {

        fun showSubList(list: List<String>)
        fun showEmptySubListMessage()
        fun onLoadSubListFailed(@StringRes messageRes: Int)
        fun openDetailPage(subPretes: String)
    }
}