package illiyin.mhandharbeni.bicapps.feature.klinik

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.util.DialogBuilder
import illiyin.mhandharbeni.bicapps.util.KlinikPage
import illiyin.mhandharbeni.bicapps.util.KlinikPage.values
import kotlinx.android.synthetic.main.base_activity_grid.*

class KlinikActivity : AppCompatActivity() {

    private val mMenuList = values()

    companion object {
        fun getIntent(context: Context) = Intent(context, KlinikActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_klinik)

        with(grid_view) {
            adapter = GridMenuAdapter()
            setOnItemClickListener { _, _, position, _ ->
                openPage(mMenuList[position])
            }
        }
    }

    private fun openPage(page: KlinikPage) {
        when {
            page.clazz == KlinikActivity::class.java ->
                DialogBuilder(this).showUnderDevelopmentDialog()
            else -> startActivity(Intent(this, page.clazz))
        }
    }

    inner class GridMenuAdapter : BaseAdapter() {

        override fun getView(position: Int, recycledView: View?, parent: ViewGroup?): View {
            val view = (recycledView ?: layoutInflater
                    .inflate(R.layout.base_item_grid_menu, parent, false)) as TextView

            view.setText(mMenuList[position].titleRes)

            val drawableTop = ResourcesCompat.getDrawable(
                    resources, mMenuList[position].logoRes, theme)
            view.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    null, drawableTop, null, null)

            return view
        }

        override fun getItem(position: Int) = mMenuList[position]

        override fun getItemId(position: Int) = position.toLong()

        override fun getCount() = mMenuList.size
    }

}
