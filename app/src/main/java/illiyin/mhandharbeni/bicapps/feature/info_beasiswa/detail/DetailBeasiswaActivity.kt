package illiyin.mhandharbeni.bicapps.feature.info_beasiswa.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.squareup.picasso.Picasso
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.model.Beasiswa
import kotlinx.android.synthetic.main.activity_detail_beasiswa.*

class DetailBeasiswaActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_BEASISWA = "beasiswa"

        fun getIntent(context: Context, beasiswa: Beasiswa): Intent {
            val intent = Intent(context, DetailBeasiswaActivity::class.java)
            intent.putExtra(EXTRA_BEASISWA, beasiswa)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_beasiswa)

        val beasiswa = intent.getParcelableExtra<Beasiswa>(EXTRA_BEASISWA)
        fetch(beasiswa)
    }

    private fun fetch(beasiswa: Beasiswa) {
        supportActionBar?.title = beasiswa.title
        text_title.text = beasiswa.title
        text_content.text = beasiswa.content

        Picasso.get().load(R.drawable.img_beasiswa).into(image_detail)
    }
}
