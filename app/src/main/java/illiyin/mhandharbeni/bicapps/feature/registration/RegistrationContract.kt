package illiyin.mhandharbeni.bicapps.feature.registration

import android.content.Intent
import android.support.annotation.StringRes
import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView
import illiyin.mhandharbeni.bicapps.data.model.Program
import illiyin.mhandharbeni.bicapps.data.model.RegistrationForm

interface RegistrationContract {

    interface Presenter : BasePresenter {

        fun loadDataForSpinners()
        fun validateForm(form: RegistrationForm)
        fun submitForm(form: RegistrationForm)
        fun pickImageFromStorage(requestCode: Int)
        fun processSelectedImage(requestCode: Int, data: Intent)
    }

    interface View : BaseView<Presenter> {

        fun onLoadListProgramSucceed(list: List<Program>)
        fun onLoadListJurusanSucceed(list: List<String>)
        fun onLoadListUnivSucceed(list: List<String>)
        fun onLoadDataForSpinnersFailed(@StringRes messageRes: Int)
        fun showPersonalImagePath(path: String)
        fun showFathersImagePath(path: String)
        fun showMothersImagePath(path: String)
        fun openStorage(requestCode: Int)
        fun onRegistrationSucceed()
        fun onRegistrationFailed(@StringRes messageRes: Int)
    }
}