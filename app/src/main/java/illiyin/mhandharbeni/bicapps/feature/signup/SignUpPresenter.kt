package illiyin.mhandharbeni.bicapps.feature.signup

import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.base.BaseCallback
import illiyin.mhandharbeni.bicapps.data.repository.UserRepository

class SignUpPresenter(
        private val mView: SignUpContract.View,
        private val mUserRepo: UserRepository
) : SignUpContract.Presenter {

    override fun validateForm(fullname: String, username: String, email: String, password: String) {
        mView.setLoadingIndicator(true)
        var valid = true

        with(fullname) {
            if (isBlank()) {
                valid = false
                mView.onFullnameValidationError(R.string.general_error_blankform)
            }
        }
        with(username) {
            if (isBlank()) {
                valid = false
                mView.onUsernameValidationError(R.string.general_error_blankform)
            }
        }
        with(email) {
            if (isBlank()) {
                valid = false
                mView.onEmailValidationError(R.string.general_error_blankform)
            }
        }
        with(password) {
            if (isBlank()) {
                valid = false
                mView.onPasswordValidationError(R.string.general_error_blankform)
            }
        }

        if (valid) {
            mView.onValidationSucceed()
            signUp(fullname, username, email, password)
        }
        else mView.setLoadingIndicator(false)
    }

    override fun signUp(fullname: String, username: String, email: String, password: String) {
        mView.setLoadingIndicator(true)
        mUserRepo.register(
                fullname, username, email, password,
                object : BaseCallback<String> {
                    override fun onError(message: String) {
                        mView.setLoadingIndicator(false)
                        mView.onSignUpFailed(message)
                    }

                    override fun onSuccess(data: String) {
                        mView.setLoadingIndicator(false)
                        mView.onSignUpSucceed(data)
                    }
                }
        )
    }

    override fun goToSignInPage() = mView.openSignInPage()

    override fun start() { }
}