package illiyin.mhandharbeni.bicapps.feature.klinik.psikotes.result

import illiyin.mhandharbeni.bicapps.data.repository.PsikotesRepository
import illiyin.mhandharbeni.bicapps.util.TestType

class ResultPsikotesPresenter(
        private val mView: ResultPsikotesContract.View,
        private val mPsikotesRepo: PsikotesRepository
) : ResultPsikotesContract.Presenter {

    private val mTest: TestType = TestType.PSIKOTES

    override fun loadResultDescription() {
        mView.setLoadingIndicator(true)
        val desc = mPsikotesRepo.getResultDescription()
        mTest.score = 90

        mView.onLoadResultSucceed(mTest, desc)
        mView.setLoadingIndicator(false)
    }

    override fun start() = loadResultDescription()
}