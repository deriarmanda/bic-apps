package illiyin.mhandharbeni.bicapps.feature.mybic.rangkumanmateri.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import illiyin.mhandharbeni.bicapps.R
import kotlinx.android.synthetic.main.activity_detail_materi.*

class DetailMateriActivity : AppCompatActivity() {

    private lateinit var mTitle: String

    companion object {
        private const val EXTRA_APPBAR_TITLE = "title"

        fun getIntent(context: Context, title: String): Intent {
            val intent = Intent(context, DetailMateriActivity::class.java)
            intent.putExtra(EXTRA_APPBAR_TITLE, title)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_materi)

        mTitle = intent.getStringExtra(EXTRA_APPBAR_TITLE)

        text_title.setText(R.string.general_msg_dummy_short)
        text_content.setText(R.string.general_msg_dummy_very_long)
        image_header.setImageResource(R.drawable.img_materi)

        supportActionBar?.title = mTitle
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
