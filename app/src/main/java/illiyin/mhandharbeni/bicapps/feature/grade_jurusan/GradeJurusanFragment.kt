package illiyin.mhandharbeni.bicapps.feature.grade_jurusan


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.model.Kampus
import illiyin.mhandharbeni.bicapps.data.model.Ranking
import illiyin.mhandharbeni.bicapps.util.DialogBuilder
import kotlinx.android.synthetic.main.fragment_grade_jurusan.*

/**
 * A simple [Fragment] subclass.
 *
 */
class GradeJurusanFragment : Fragment(), GradeJurusanContract.View {

    override lateinit var presenter: GradeJurusanPresenter

    private lateinit var mSpinnerAdapter: ArrayAdapter<Kampus>
    private lateinit var mLoadingDialog: DialogBuilder
    private val mListAdapter = GradeJurusanRvAdapter()

    companion object {
        fun getInstance() = GradeJurusanFragment()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        presenter.bind()
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_grade_jurusan, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mLoadingDialog = DialogBuilder(context!!)
                .buildProgressDialog(R.string.ranking_msg_loading)
        mSpinnerAdapter = ArrayAdapter(context!!, android.R.layout.simple_list_item_1)

        with(view) {
            with(spinner_kampus) {
                adapter = mSpinnerAdapter
                onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {}

                    override fun onItemSelected(
                            p0: AdapterView<*>?,
                            p1: View?,
                            position: Int,
                            p3: Long) {
                        presenter.loadListJurusan(spinner_kampus.selectedItem as Kampus)
                    }
                }
            }
            with(list_jurusan) {
                adapter = mListAdapter
                layoutManager = LinearLayoutManager(context)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun showListKampus(kampusList: List<Kampus>) {
        mSpinnerAdapter.clear()
        mSpinnerAdapter.addAll(kampusList)
        mSpinnerAdapter.notifyDataSetChanged()
    }

    override fun onLoadListKampusFailed() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showEmptyListKampus() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showListJurusan(jurusanList: List<Ranking>) {
        mListAdapter.mRankingList = jurusanList
    }

    override fun onLoadListJurusanFailed() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showEmptyListJurusan() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun setLoadingIndicator(active: Boolean) {
        if (active) mLoadingDialog.show()
        else mLoadingDialog.dismiss()
    }

}
