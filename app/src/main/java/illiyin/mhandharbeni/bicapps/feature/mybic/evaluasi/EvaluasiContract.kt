package illiyin.mhandharbeni.bicapps.feature.mybic.evaluasi

import android.support.annotation.StringRes
import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView

interface EvaluasiContract {

    interface Presenter : BasePresenter {

        fun loadListEvaluasi()
        fun goToSubListPage(evaluasi: String)
    }

    interface View : BaseView<Presenter> {

        fun showListEvaluasi(list: List<String>)
        fun showEmptyListMessage()
        fun onLoadListEvaluasiFailed(@StringRes messageRes: Int)
        fun openSubListPage(evaluasi: String)
    }
}