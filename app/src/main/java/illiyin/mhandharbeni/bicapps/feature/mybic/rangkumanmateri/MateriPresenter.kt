package illiyin.mhandharbeni.bicapps.feature.mybic.rangkumanmateri

import illiyin.mhandharbeni.bicapps.data.repository.MateriRepository

class MateriPresenter(
        private val mView: MateriContract.View,
        private val mMateriRepo: MateriRepository
) : MateriContract.Presenter {

    override fun loadListMateri() {
        mView.setLoadingIndicator(true)
        val list = mMateriRepo.getListMateri()

        mView.setLoadingIndicator(false)
        if (list.isEmpty()) mView.showEmptyListMessage()
        else mView.showListMateri(list)
    }

    override fun goToSubListPage(materi: String) {
        mView.openSubListPage(materi)
    }

    override fun start() = loadListMateri()
}