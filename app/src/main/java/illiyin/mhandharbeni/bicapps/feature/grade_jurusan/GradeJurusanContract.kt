package illiyin.mhandharbeni.bicapps.feature.grade_jurusan

import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView
import illiyin.mhandharbeni.bicapps.data.model.Kampus
import illiyin.mhandharbeni.bicapps.data.model.Ranking

interface GradeJurusanContract {

    interface Presenter : BasePresenter {

        fun bind()
        fun loadListKampus()
        fun loadListJurusan(kampus: Kampus)
    }

    interface View : BaseView<Presenter> {

        fun showListKampus(kampusList: List<Kampus>)
        fun onLoadListKampusFailed()
        fun showEmptyListKampus()
        fun showListJurusan(jurusanList: List<Ranking>)
        fun onLoadListJurusanFailed()
        fun showEmptyListJurusan()
    }
}