package illiyin.mhandharbeni.bicapps.feature.tryout.detail

import android.support.annotation.StringRes
import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView
import illiyin.mhandharbeni.bicapps.data.model.Question
import illiyin.mhandharbeni.bicapps.util.TestType

interface DetailTryOutContract {

    interface Presenter : BasePresenter {

        fun loadListQuestion()
        fun goToTestPage()
    }

    interface View : BaseView<Presenter> {

        fun onLoadListQuestionSucceed(duration: Long)
        fun onLoadListQuestionFailed(@StringRes messageRes: Int)
        fun openTestPage(type: TestType, list: ArrayList<Question>)
    }
}