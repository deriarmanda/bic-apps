package illiyin.mhandharbeni.bicapps.feature.program.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.model.Program
import illiyin.mhandharbeni.bicapps.feature.registration.RegistrationActivity
import kotlinx.android.synthetic.main.activity_detail_program.*

class DetailProgramActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_PROGRAM = "program"

        fun getIntent(context: Context, program: Program): Intent {
            val intent = Intent(context, DetailProgramActivity::class.java)
            intent.putExtra(EXTRA_PROGRAM, program)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_program)

        val program = intent.getParcelableExtra<Program>(EXTRA_PROGRAM)
        fetch(program)
    }

    private fun fetch(program: Program) {
        supportActionBar?.title = getString(R.string.program_msg_title, program.judul)
        text_title.text = getString(R.string.program_msg_title, program.judul)
        text_content.text = program.deskripsi

        button_daftar.setOnClickListener {
            startActivity(RegistrationActivity.getIntent(this@DetailProgramActivity))
        }
    }
}
