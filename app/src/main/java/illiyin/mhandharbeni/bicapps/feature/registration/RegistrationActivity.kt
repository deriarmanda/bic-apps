package illiyin.mhandharbeni.bicapps.feature.registration

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.DatePicker
import android.widget.Toast
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.model.Program
import illiyin.mhandharbeni.bicapps.data.model.RegistrationForm
import illiyin.mhandharbeni.bicapps.data.repository.RegistrationRepository
import illiyin.mhandharbeni.bicapps.feature.registration.payment.PaymentActivity
import illiyin.mhandharbeni.bicapps.util.DateFormatter
import illiyin.mhandharbeni.bicapps.util.DialogBuilder
import illiyin.mhandharbeni.bicapps.util.RequestCode
import kotlinx.android.synthetic.main.activity_registration.*
import kotlinx.android.synthetic.main.partial_registration_form_orangtua.*
import kotlinx.android.synthetic.main.partial_registration_form_pribadi.*
import kotlinx.android.synthetic.main.partial_registration_form_program.*
import java.util.*

class RegistrationActivity : AppCompatActivity(), RegistrationContract.View, DatePickerDialog.OnDateSetListener {

    override lateinit var presenter: RegistrationPresenter

    companion object {
        fun getIntent(context: Context) = Intent(context, RegistrationActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        presenter = RegistrationPresenter(this, RegistrationRepository.getInstance())

        button_daftar.setOnClickListener {
            presenter.validateForm(RegistrationForm(
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    spinner_program.selectedItem.toString(),
                    spinner_jurusan_1.selectedItem.toString(),
                    spinner_jurusan_2.selectedItem.toString(),
                    spinner_jurusan_3.selectedItem.toString(),
                    spinner_univ_1.selectedItem.toString(),
                    spinner_univ_2.selectedItem.toString(),
                    spinner_univ_3.selectedItem.toString()
            ))
        }

        button_upload_foto_pribadi.setOnClickListener {
            presenter.pickImageFromStorage(RequestCode.PICK_PERSONAL_PHOTO)
        }
        button_upload_foto_ayah.setOnClickListener {
            presenter.pickImageFromStorage(RequestCode.PICK_FATHERS_PHOTO)
        }
        button_upload_foto_ibu.setOnClickListener {
            presenter.pickImageFromStorage(RequestCode.PICK_MOTHERS_PHOTO)
        }

        button_datepicker.setOnClickListener {
            DialogBuilder(this@RegistrationActivity)
                    .buildDatePickerDialog(this@RegistrationActivity)
                    .show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter.processSelectedImage(requestCode, data!!)
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onLoadListProgramSucceed(list: List<Program>) {}

    override fun onLoadListJurusanSucceed(list: List<String>) {}

    override fun onLoadListUnivSucceed(list: List<String>) {}

    override fun onLoadDataForSpinnersFailed(messageRes: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showPersonalImagePath(path: String) {
        text_path_foto_pribadi.text = path
    }

    override fun showFathersImagePath(path: String) {
        text_path_foto_ayah.text = path
    }

    override fun showMothersImagePath(path: String) {
        text_path_foto_ibu.text = path
    }

    override fun openStorage(requestCode: Int) {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "image/*"
        startActivityForResult(Intent.createChooser(
                intent,
                getString(R.string.general_action_pilih_gambar)
        ), requestCode)
    }

    override fun onRegistrationSucceed() {
        DialogBuilder(this).showInformationToast(
                R.string.general_msg_under_development,
                Toast.LENGTH_LONG
        )

        startActivity(PaymentActivity.getIntent(
                this,
                "Pembayaran DP",
                "Program ${spinner_program.selectedItem}"
        ))
        finish()
    }

    override fun onRegistrationFailed(messageRes: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun setLoadingIndicator(active: Boolean) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onDateSet(p0: DatePicker?, year: Int, month: Int, day: Int) {
        val calendar = Calendar.getInstance()
        calendar.set(year, month, day)
        form_tanggal_lahir.text?.insert(0, DateFormatter.format(calendar.time))
    }
}
