package illiyin.mhandharbeni.bicapps.feature.mybic.bukupretes.sublist

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import illiyin.mhandharbeni.bicapps.R
import kotlinx.android.synthetic.main.base_item_list_bimbel.view.*

class SubPretesRvAdapter(
        private val mContext: Context,
        private val mClickListener: OnItemClickListener
) : RecyclerView.Adapter<SubPretesRvAdapter.ItemViewHolder>() {

    var mSubPretesList: List<String> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.base_item_list_bimbel, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.fetch(mSubPretesList[position])
    }

    override fun getItemCount() = mSubPretesList.size

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun fetch(subPretes: String) {
            itemView.text_title.text = subPretes
            itemView.text_subtitle.text = mContext.getString(
                    R.string.sub_pretes_subtitle_list,
                    subPretes
            )

            itemView.setOnClickListener { mClickListener.onItemClick(subPretes) }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(subPretes: String)
    }
}