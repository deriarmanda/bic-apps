package illiyin.mhandharbeni.bicapps.feature.tryout.ranking

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.model.Ranking
import illiyin.mhandharbeni.bicapps.data.repository.TryOutRepository
import illiyin.mhandharbeni.bicapps.feature.grade_jurusan.GradeJurusanRvAdapter
import kotlinx.android.synthetic.main.activity_ranking.*

class RankingTryOutActivity : AppCompatActivity(), RankingTryOutContract.View {

    override lateinit var presenter: RankingTryOutContract.Presenter
    private val mAdapter = GradeJurusanRvAdapter()

    companion object {
        fun getIntent(context: Context) = Intent(context, RankingTryOutActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ranking)

        presenter = RankingTryOutPresenter(this, TryOutRepository.getInstance())

        with(refresh_layout) {
            setColorSchemeResources(R.color.primary)
            setOnRefreshListener { presenter.loadListRanking() }
        }
        with(list_ranking) {
            layoutManager = LinearLayoutManager(this@RankingTryOutActivity)
            adapter = mAdapter
        }
        button_selesai.setOnClickListener { finish() }
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun showListRanking(arrayList: ArrayList<Ranking>) {
        mAdapter.mRankingList = arrayList
    }

    override fun showEmptyListMessage() {
        //TODO: implement showEmptyListMessage function laters.
    }

    override fun onLoadListRankingFailed(messageRes: Int) {
        //TODO: implement onLoadListRankingFailed function laters.
    }

    override fun setLoadingIndicator(active: Boolean) {
        refresh_layout.isRefreshing = active
    }
}
