package illiyin.mhandharbeni.bicapps.feature.klinik.tesiq

import android.support.annotation.StringRes
import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView
import illiyin.mhandharbeni.bicapps.data.model.Question
import illiyin.mhandharbeni.bicapps.util.TestType

interface TesIQContract {

    interface Presenter : BasePresenter {

        fun loadListQuestion()
        fun goToTestPage()
    }

    interface View : BaseView<Presenter> {

        fun onLoadListQuestionSucceed(duration: Long)
        fun onLoadListQuestionFailed(@StringRes messageRes: Int)
        fun openTestPage(testType: TestType, list: ArrayList<Question>)
    }
}