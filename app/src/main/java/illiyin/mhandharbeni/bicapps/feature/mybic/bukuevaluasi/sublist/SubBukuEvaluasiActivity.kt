package illiyin.mhandharbeni.bicapps.feature.mybic.bukuevaluasi.sublist

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.repository.BukuEvaluasiRepository
import illiyin.mhandharbeni.bicapps.feature.mybic.bukuevaluasi.sublist.SubBukuEvaluasiRvAdapter.OnItemClickListener
import illiyin.mhandharbeni.bicapps.feature.pdf.PdfActivity
import illiyin.mhandharbeni.bicapps.util.DummyPdf
import illiyin.mhandharbeni.bicapps.util.PdfSourceMode
import illiyin.mhandharbeni.bicapps.util.ProfilPage
import kotlinx.android.synthetic.main.base_activity_list.*

class SubBukuEvaluasiActivity : AppCompatActivity(), SubBukuEvaluasiContract.View {

    override lateinit var presenter: SubBukuEvaluasiPresenter

    private lateinit var mSelectedBukuEvaluasi: String
    private val mClickListener = object : OnItemClickListener {
        override fun onItemClick(subBukuEvaluasi: String) {
            presenter.goToDetailPage(subBukuEvaluasi)
        }
    }
    private val mAdapter = SubBukuEvaluasiRvAdapter(this, mClickListener)

    companion object {
        private const val EXTRA_SELECTED_BUKU_EVALUASI = "buku_evaluasi"

        fun getIntent(context: Context, bukuEvaluasi: String): Intent {
            val intent = Intent(context, SubBukuEvaluasiActivity::class.java)
            intent.putExtra(EXTRA_SELECTED_BUKU_EVALUASI, bukuEvaluasi)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.base_activity_list)

        mSelectedBukuEvaluasi = intent.getStringExtra(EXTRA_SELECTED_BUKU_EVALUASI)
        presenter = SubBukuEvaluasiPresenter(
                this,
                BukuEvaluasiRepository.getInstance(this),
                mSelectedBukuEvaluasi
        )

        with(refresh_layout) {
            setColorSchemeResources(R.color.primary)
            setOnRefreshListener { presenter.loadSubList() }
        }
        with(recycler_view) {
            layoutManager = LinearLayoutManager(this@SubBukuEvaluasiActivity)
            adapter = mAdapter
        }

        text_empty_list.text = getString(
                R.string.sub_buku_evaluasi_msg_empty,
                mSelectedBukuEvaluasi
        )
        supportActionBar?.title = getString(
                R.string.buku_evaluasi_subtitle_list,
                mSelectedBukuEvaluasi
        )
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun showSubList(list: List<String>) {
        mAdapter.mSubBukuEvaluasiList = list
        text_empty_list.visibility = View.GONE
    }

    override fun showEmptySubListMessage() {
        text_empty_list.visibility = View.VISIBLE
    }

    override fun onLoadSubListFailed(messageRes: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun openDetailPage(subBukuEvaluasi: String) {
        val page = ProfilPage.BUKU_EVALUASI
        startActivity(PdfActivity.getIntent(
                this,
                page.titleRes,
                PdfSourceMode.FROM_LOCAL_FILE,
                DummyPdf.getFile(page)!!))
    }

    override fun setLoadingIndicator(active: Boolean) {
        refresh_layout.isRefreshing = active
    }
}