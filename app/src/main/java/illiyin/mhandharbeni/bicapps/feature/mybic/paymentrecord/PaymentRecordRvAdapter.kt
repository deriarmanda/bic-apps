package illiyin.mhandharbeni.bicapps.feature.mybic.paymentrecord

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.model.PaymentRecord
import kotlinx.android.synthetic.main.item_payment.view.*

class PaymentRecordRvAdapter(
        private val mClickListener: OnItemClickListener
) : RecyclerView.Adapter<PaymentRecordRvAdapter.ItemViewHolder>() {

    var mPaymentList: List<PaymentRecord> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_payment, parent, false)
        when (viewType) {
            0 -> {
                view.vertical_line_top.visibility = View.GONE
                view.vertical_line_bottom.visibility = View.VISIBLE
            }
            1 -> {
                view.vertical_line_top.visibility = View.VISIBLE
                view.vertical_line_bottom.visibility = View.GONE
            }
            else -> {
                view.vertical_line_top.visibility = View.VISIBLE
                view.vertical_line_bottom.visibility = View.VISIBLE
            }
        }

        return ItemViewHolder(view)
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> 0
            mPaymentList.size - 1 -> 1
            else -> 2
        }
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.fetch(mPaymentList[position])
    }

    override fun getItemCount() = mPaymentList.size

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun fetch(record: PaymentRecord) {
            itemView.text_title.text = record.title
            itemView.text_biaya.text = record.price
            itemView.image_checked.visibility = if (record.isDone) View.VISIBLE else View.GONE
            itemView.image_indicator.setImageResource(
                    if (record.isDone) R.drawable.ic_indctr1
                    else R.drawable.ic_indctr2
            )

            itemView.button_bayar.visibility = if (record.isDone) View.GONE else View.VISIBLE
            itemView.button_bayar.setOnClickListener { mClickListener.onItemClick(record) }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(record: PaymentRecord)
    }
}
