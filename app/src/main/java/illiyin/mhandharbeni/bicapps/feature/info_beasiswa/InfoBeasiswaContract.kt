package illiyin.mhandharbeni.bicapps.feature.info_beasiswa

import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView
import illiyin.mhandharbeni.bicapps.data.model.Beasiswa

interface InfoBeasiswaContract {

    interface Presenter : BasePresenter {

        fun loadListBeasiswa(isRefreshed: Boolean = false)
        fun goToDetailBeasiswaPage(beasiswa: Beasiswa)
    }

    interface View : BaseView<Presenter> {

        fun showListBeasiswa(beasiswaList: List<Beasiswa>)
        fun showEmptyListMessage()
        fun onLoadListBeasiswaFailed(message: String)
        fun openDetailBeasiswaPage(beasiswa: Beasiswa)
    }
}