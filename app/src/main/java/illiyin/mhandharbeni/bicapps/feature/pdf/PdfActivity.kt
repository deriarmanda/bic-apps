package illiyin.mhandharbeni.bicapps.feature.pdf

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.github.barteksc.pdfviewer.PDFView
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.util.PdfSourceMode
import kotlinx.android.synthetic.main.activity_pdf.*
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL

class PdfActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_APP_BAR_TITLE = "title"
        private const val EXTRA_PDF_SOURCE_MODE = "source_mode"
        private const val EXTRA_PDF_FILE = "file"

        fun getIntent(
                context: Context,
                @StringRes title: Int,
                mode: PdfSourceMode,
                source: String
        ): Intent {
            val intent = Intent(context, PdfActivity::class.java)
            intent.putExtra(EXTRA_APP_BAR_TITLE, title)
            intent.putExtra(EXTRA_PDF_SOURCE_MODE, mode)
            intent.putExtra(EXTRA_PDF_FILE, source)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pdf)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setTitle(intent.getIntExtra(EXTRA_APP_BAR_TITLE, R.string.app_name))

        val mode = intent.getSerializableExtra(EXTRA_PDF_SOURCE_MODE) as PdfSourceMode
        val file = intent.getStringExtra(EXTRA_PDF_FILE)
        if (mode == PdfSourceMode.FROM_LOCAL_FILE) configPdfView(pdf_view.fromAsset(file))
        else {
            LoadPdfTask(object : LoadPdfTask.LoadPdfCallback {
                override fun onLoadSucceed(stream: InputStream) {
                    configPdfView(pdf_view.fromStream(stream))
                }
            }).execute(file)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun configPdfView(configurator: PDFView.Configurator) {
        configurator.load()
    }

    private class LoadPdfTask(private val callback: LoadPdfCallback) : AsyncTask<String, Void, InputStream>() {

        override fun doInBackground(vararg urls: String?): InputStream {
            val url = URL(urls[0])
            val httpConnection = url.openConnection() as HttpURLConnection
            httpConnection.connect()

            return httpConnection.inputStream
        }

        override fun onPostExecute(result: InputStream?) {
            super.onPostExecute(result)
            callback.onLoadSucceed(result!!)
        }

        interface LoadPdfCallback {
            fun onLoadSucceed(stream: InputStream)
        }
    }
}
