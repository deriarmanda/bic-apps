package illiyin.mhandharbeni.bicapps.feature.mybic.bukupretes.sublist

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.repository.PretesRepository
import illiyin.mhandharbeni.bicapps.feature.mybic.bukupretes.sublist.SubPretesRvAdapter.OnItemClickListener
import illiyin.mhandharbeni.bicapps.feature.pdf.PdfActivity
import illiyin.mhandharbeni.bicapps.util.DummyPdf
import illiyin.mhandharbeni.bicapps.util.PdfSourceMode
import illiyin.mhandharbeni.bicapps.util.ProfilPage
import kotlinx.android.synthetic.main.base_activity_list.*

class SubPretesActivity : AppCompatActivity(), SubPretesContract.View {

    override lateinit var presenter: SubPretesPresenter

    private lateinit var mSelectedPretes: String
    private val mClickListener = object : OnItemClickListener {
        override fun onItemClick(subPretes: String) {
            presenter.goToDetailPage(subPretes)
        }
    }
    private val mAdapter = SubPretesRvAdapter(this, mClickListener)

    companion object {
        private const val EXTRA_SELECTED_PRETES = "pretes"

        fun getIntent(context: Context, pretes: String): Intent {
            val intent = Intent(context, SubPretesActivity::class.java)
            intent.putExtra(EXTRA_SELECTED_PRETES, pretes)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.base_activity_list)

        mSelectedPretes = intent.getStringExtra(EXTRA_SELECTED_PRETES)
        presenter = SubPretesPresenter(
                this,
                PretesRepository.getInstance(this),
                mSelectedPretes
        )

        with(refresh_layout) {
            setColorSchemeResources(R.color.primary)
            setOnRefreshListener { presenter.loadSubList() }
        }
        with(recycler_view) {
            layoutManager = LinearLayoutManager(this@SubPretesActivity)
            adapter = mAdapter
        }

        text_empty_list.text = getString(R.string.sub_pretes_msg_empty, mSelectedPretes)
        supportActionBar?.title = getString(R.string.pretes_subtitle_list, mSelectedPretes)
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun showSubList(list: List<String>) {
        mAdapter.mSubPretesList = list
        text_empty_list.visibility = View.GONE
    }

    override fun showEmptySubListMessage() {
        text_empty_list.visibility = View.VISIBLE
    }

    override fun onLoadSubListFailed(messageRes: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun openDetailPage(subPretes: String) {
        val page = ProfilPage.BUKU_PRETES
        startActivity(PdfActivity.getIntent(
                this,
                page.titleRes,
                PdfSourceMode.FROM_LOCAL_FILE,
                DummyPdf.getFile(page)!!))
    }

    override fun setLoadingIndicator(active: Boolean) {
        refresh_layout.isRefreshing = active
    }
}