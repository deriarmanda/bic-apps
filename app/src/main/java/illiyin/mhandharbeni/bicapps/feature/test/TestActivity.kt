package illiyin.mhandharbeni.bicapps.feature.test

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.model.Question
import illiyin.mhandharbeni.bicapps.util.TestType
import kotlinx.android.synthetic.main.activity_test.*
import kotlinx.android.synthetic.main.item_test_page_1.*
import kotlinx.android.synthetic.main.item_test_page_2.*
import java.util.*

class TestActivity : AppCompatActivity(), TestContract.View {

    override lateinit var presenter: TestPresenter

    private lateinit var mTestType: TestType
    private lateinit var mTimer: CountDownTimer
    private lateinit var mMenuNumber: MenuItem

    companion object {
        const val EXTRA_TEST_TYPE = "type"
        const val EXTRA_QUESTIONS = "questions"

        fun getIntent(context: Context, type: TestType, list: ArrayList<Question>): Intent {
            val intent = Intent(context, TestActivity::class.java)
            intent.putExtra(EXTRA_TEST_TYPE, type)
            intent.putParcelableArrayListExtra(EXTRA_QUESTIONS, list)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)

        mTestType = intent.getSerializableExtra(EXTRA_TEST_TYPE) as TestType
        val list = intent.getParcelableArrayListExtra<Question>(EXTRA_QUESTIONS)
        presenter = TestPresenter(this, mTestType, list)

        supportActionBar?.title = mTestType.title

        with(view_flipper) {
            isAutoStart = false
            setInAnimation(this@TestActivity, R.anim.in_from_bottom)
            setOutAnimation(this@TestActivity, R.anim.out_to_top)
        }

        radio_answer_true_odd.setOnCheckedChangeListener { _, checked ->
            if (checked) presenter.checkAnswer("benar")
        }
        radio_answer_false_odd.setOnCheckedChangeListener { _, checked ->
            if (checked) presenter.checkAnswer("salah")
        }

        radio_answer_true_even.setOnCheckedChangeListener { _, checked ->
            if (checked) presenter.checkAnswer("benar")
        }
        radio_answer_false_even.setOnCheckedChangeListener { _, checked ->
            if (checked) presenter.checkAnswer("salah")
        }

        mTimer = object : CountDownTimer(mTestType.duration, 1000) {
            override fun onFinish() {
                presenter.checkAnswer("kosong")
            }

            override fun onTick(millis: Long) {
                text_timer.text = (millis / 1000).toString()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_test, menu)
        mMenuNumber = menu?.findItem(R.id.menu_number)!!

        presenter.start()
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        forceStopTimer()
    }

    override fun showEvenQuestion(question: Question) {
        text_question_even.text = question.question
        radio_answer_true_even.isChecked = false
        radio_answer_false_even.isChecked = false

        view_flipper.showNext()
    }

    override fun showOddQuestion(question: Question) {
        text_question_odd.text = question.question
        radio_answer_true_odd.isChecked = false
        radio_answer_false_odd.isChecked = false

        view_flipper.showNext()
    }

    override fun updateTestNumber(currentNo: Int, totalNo: Int) {
        mMenuNumber.title = getString(R.string.test_msg_number, currentNo, totalNo)
    }

    override fun startTimer() {
        mTimer.start()
    }

    override fun forceStopTimer() {
        mTimer.cancel()
    }

    override fun onTestFinished(test: TestType) {
        startActivity(Intent(this, test.resultClazz))
        finish()
    }

    override fun setLoadingIndicator(active: Boolean) {}
}
