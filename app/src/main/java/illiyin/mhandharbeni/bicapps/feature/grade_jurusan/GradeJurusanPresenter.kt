package illiyin.mhandharbeni.bicapps.feature.grade_jurusan

import illiyin.mhandharbeni.bicapps.data.model.Kampus
import illiyin.mhandharbeni.bicapps.data.repository.GradeJurusanRepository

class GradeJurusanPresenter(
        private val mView: GradeJurusanContract.View,
        private val mGradeJurusanRepo: GradeJurusanRepository
) : GradeJurusanContract.Presenter {

    init {
        bind()
    }

    override fun bind() {
        (mView as GradeJurusanFragment).presenter = this
    }

    override fun loadListKampus() {
        mView.setLoadingIndicator(true)
        val list = mGradeJurusanRepo.getListKampus()

        mView.setLoadingIndicator(false)
        if (list.isEmpty()) mView.showEmptyListKampus()
        else mView.showListKampus(list)
    }

    override fun loadListJurusan(kampus: Kampus) {
        if (kampus.listRanking.isEmpty()) mView.showEmptyListJurusan()
        else mView.showListJurusan(kampus.listRanking)
    }

    override fun start() = loadListKampus()
}