package illiyin.mhandharbeni.bicapps.feature.home

import android.support.annotation.DrawableRes
import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView

interface HomeContract {

    interface Presenter : BasePresenter {

        fun checkUserSession()
        fun loadImageForSlider()
        fun signOut()
    }

    interface View : BaseView<Presenter> {

        fun onUserAlreadySignIn()
        fun onUserNotSignedIn()
        fun onUserSignedOut()
        fun showImageForSlider(
                urls: List<String>,
                @DrawableRes placeholderDrawableRes: Int,
                @DrawableRes errorDrawableRes: Int
        )
    }
}