package illiyin.mhandharbeni.bicapps.feature.klinik.tesiq.result

import illiyin.mhandharbeni.bicapps.data.repository.TesIQRepository
import illiyin.mhandharbeni.bicapps.util.TestType

class ResultTesIQPresenter(
        private val mView: ResultTesIQContract.View,
        private val mTesIQRepo: TesIQRepository
) : ResultTesIQContract.Presenter {

    private val mTest: TestType = TestType.PSIKOTES

    override fun loadResultDescription() {
        mView.setLoadingIndicator(true)
        val desc = mTesIQRepo.getResultDescription()
        mTest.score = 95

        mView.onLoadResultSucceed(mTest, desc)
        mView.setLoadingIndicator(false)
    }

    override fun start() = loadResultDescription()
}