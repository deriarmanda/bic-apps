package illiyin.mhandharbeni.bicapps.feature.mybic.evaluasi.detail

import illiyin.mhandharbeni.bicapps.data.model.Question
import illiyin.mhandharbeni.bicapps.data.repository.EvaluasiRepository
import illiyin.mhandharbeni.bicapps.util.TestType

class DetailEvaluasiPresenter(
        private val mView: DetailEvaluasiContract.View,
        private val mEvaluasiRepo: EvaluasiRepository,
        private val mSelectedEvaluasi: String
) : DetailEvaluasiContract.Presenter {

    private lateinit var mQuestionList: ArrayList<Question>
    private val test: TestType = TestType.EVALUASI

    init {
        test.title = mSelectedEvaluasi
    }

    override fun loadListQuestion() {
        mView.setLoadingIndicator(true)
        mQuestionList = mEvaluasiRepo.getListQuestion(mSelectedEvaluasi)

        test.duration = 30000

        mView.onLoadListQuestionSucceed(test.duration)
        mView.setLoadingIndicator(false)
    }

    override fun goToTestPage() {
        mView.openTestPage(test, mQuestionList)
    }

    override fun start() = loadListQuestion()
}