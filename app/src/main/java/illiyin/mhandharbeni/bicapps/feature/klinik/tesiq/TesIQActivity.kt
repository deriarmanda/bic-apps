package illiyin.mhandharbeni.bicapps.feature.klinik.tesiq

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.model.Question
import illiyin.mhandharbeni.bicapps.data.repository.TesIQRepository
import illiyin.mhandharbeni.bicapps.feature.test.TestActivity
import illiyin.mhandharbeni.bicapps.util.DialogBuilder
import illiyin.mhandharbeni.bicapps.util.TestType
import kotlinx.android.synthetic.main.base_activity_init_test.*
import java.util.*

class TesIQActivity : AppCompatActivity(), TesIQContract.View {

    override lateinit var presenter: TesIQPresenter

    private lateinit var mTitle: String
    private lateinit var mLoadingDialog: DialogBuilder

    companion object {
        fun getIntent(context: Context) = Intent(context, TesIQActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.base_activity_init_test)

        presenter = TesIQPresenter(this, TesIQRepository.getInstance())
        mTitle = getString(R.string.title_tes_iq)
        mLoadingDialog = DialogBuilder(this)
                .buildProgressDialog(R.string.test_msg_loading)

        with(button_mulai) {
            text = getString(R.string.test_action_mulai, mTitle)
            setOnClickListener { presenter.goToTestPage() }
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onLoadListQuestionSucceed(duration: Long) {
        text_rules.text = getString(R.string.test_msg_rules, mTitle, duration)
    }

    override fun onLoadListQuestionFailed(messageRes: Int) {
        //TODO: implement onLoadListQuestionFailed function laters.
    }

    override fun openTestPage(testType: TestType, list: ArrayList<Question>) {
        startActivity(TestActivity.getIntent(
                this,
                testType,
                list
        ))
    }

    override fun setLoadingIndicator(active: Boolean) {
        if (active) mLoadingDialog.show()
        else mLoadingDialog.dismiss()
    }
}
