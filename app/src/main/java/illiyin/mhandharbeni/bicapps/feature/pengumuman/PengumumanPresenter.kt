package illiyin.mhandharbeni.bicapps.feature.pengumuman

import illiyin.mhandharbeni.bicapps.base.BaseCallback
import illiyin.mhandharbeni.bicapps.data.model.Pengumuman
import illiyin.mhandharbeni.bicapps.data.repository.PengumumanRepository

class PengumumanPresenter(
        private val mView: PengumumanContract.View,
        private val mPengumumanRepo: PengumumanRepository
) : PengumumanContract.Presenter {

    override fun loadListPengumuman(isRefreshed: Boolean) {
        mView.setLoadingIndicator(true)
        mPengumumanRepo.getListPengumuman(
                isRefreshed,
                object : BaseCallback<List<Pengumuman>> {
                    override fun onError(message: String) {
                        mView.setLoadingIndicator(false)
                        mView.onLoadListPengumumanFailed(message)
                    }

                    override fun onSuccess(data: List<Pengumuman>) {
                        mView.setLoadingIndicator(false)
                        if (data.isEmpty()) mView.showEmptyListMessage()
                        else mView.showListPengumuman(data)
                    }
                }
        )
    }

    override fun goToDetailPengumumanPage(pengumuman: Pengumuman) {
        mView.openDetailPengumumanPage(pengumuman)
    }

    override fun start() = loadListPengumuman()
}