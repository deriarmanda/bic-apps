package illiyin.mhandharbeni.bicapps.feature.mybic.grafik

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.BaseAdapter
import android.widget.TextView
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.repository.EvaluasiRepository
import kotlinx.android.synthetic.main.activity_grafik.*
import java.util.*

class GrafikActivity : AppCompatActivity(), GrafikContract.View {

    override lateinit var presenter: GrafikPresenter
    private val mSpinnerAdapter = SpinnerCustomAdapter()

    companion object {
        fun getIntent(context: Context) = Intent(context, GrafikActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grafik)

        presenter = GrafikPresenter(this, EvaluasiRepository.getInstance(this))

        with(refresh_layout) {
            setColorSchemeResources(R.color.primary)
            setOnRefreshListener { presenter.loadListEvaluasi() }
        }
        with(spinner_mapel) {
            adapter = mSpinnerAdapter
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {}

                override fun onItemSelected(
                        spinner: AdapterView<*>?,
                        item: View?,
                        position: Int,
                        id: Long
                ) = presenter.loadListNilai(spinner?.selectedItem as String)
            }
        }
        with(line_chart) {
            description = null
            isDoubleTapToZoomEnabled = false
            extraLeftOffset = 24f
            extraRightOffset = 24f
            isScaleXEnabled = true
            isScaleYEnabled = false
            setPinchZoom(false)

            xAxis.apply {
                granularity = 1f
                textSize = 14f
                position = XAxis.XAxisPosition.BOTTOM
                setDrawGridLines(false)
            }
            axisLeft.apply {
                spaceBottom = 24f
                setDrawGridLines(false)
                setDrawLabels(false)
                setDrawAxisLine(false)
            }
            axisRight.apply {
                setDrawGridLines(false)
                setDrawLabels(false)
                setDrawAxisLine(false)
            }
        }

        button_selesai.setOnClickListener { finish() }
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun showListEvaluasi(list: List<String>) {
        mSpinnerAdapter.mEvaluasiList = list
        setLoadingIndicator(false)
    }

    override fun onLoadListEvaluasiFailed(messageRes: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showListNilai(map: Map<String, Int>, label: String) {
        line_chart.data = LineData(convertMapToDataSet(
                map,
                getString(R.string.grafik_msg_label_chart, label)
        ))

        val xLabelSet = map.keys
        val xValueFormatter = IAxisValueFormatter { value, _ ->
            xLabelSet.elementAt(value.toInt())
        }

        with(line_chart) {
            xAxis.valueFormatter = xValueFormatter
            setVisibleXRangeMaximum(3f)

            notifyDataSetChanged()
            invalidate()
        }
        setLoadingIndicator(false)
    }

    override fun onLoadListNilaiFailed(messageRes: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showEmptyListMessage(messageRes: Int) {
        //TODO: show message when spinner is empty
        line_chart.setNoDataText(getString(messageRes))
    }

    override fun setLoadingIndicator(active: Boolean) {
        refresh_layout.isRefreshing = active
    }

    private fun convertMapToDataSet(map: Map<String, Int>, label: String): LineDataSet {
        val entryList = ArrayList<Entry>()
        for ((index, nilai) in map.values.withIndex()) {
            entryList.add(Entry(index.toFloat(), nilai.toFloat()))
        }

        return LineDataSet(entryList, label).apply {
            axisDependency = YAxis.AxisDependency.LEFT
            color = ResourcesCompat.getColor(resources, R.color.primary, theme)
            isHighlightEnabled = false
            lineWidth = 2f
            valueTextSize = 14f
            setCircleColor(color)
        }
    }

    inner class SpinnerCustomAdapter : BaseAdapter() {

        var mEvaluasiList: List<String> = emptyList()
            set(value) {
                field = value
                notifyDataSetChanged()
            }

        override fun getView(position: Int, recycledView: View?, parent: ViewGroup?): View {
            val view = (recycledView ?: layoutInflater.inflate(
                    android.R.layout.simple_list_item_1,
                    parent,
                    false
            )) as TextView
            view.text = mEvaluasiList[position]

            return view
        }

        override fun getItem(position: Int) = mEvaluasiList[position]

        override fun getItemId(position: Int) = position.toLong()

        override fun getCount() = mEvaluasiList.size
    }
}

