package illiyin.mhandharbeni.bicapps.feature.grade_jurusan

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.repository.GradeJurusanRepository
import kotlinx.android.synthetic.main.activity_grade_jurusan.*

class GradeJurusanActivity : AppCompatActivity() {

    private lateinit var mPresenters: Array<GradeJurusanPresenter>
    private lateinit var mFragments: Array<GradeJurusanFragment>

    companion object {
        fun getIntent(context: Context) = Intent(context, GradeJurusanActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grade_jurusan)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mFragments = arrayOf(
                GradeJurusanFragment.getInstance(),
                GradeJurusanFragment.getInstance(),
                GradeJurusanFragment.getInstance()
        )
        val adapter = GradeJurusanPagerAdapter(
                supportFragmentManager,
                mFragments,
                resources.getStringArray(R.array.jurusan_tab_titles)
        )
        view_pager.adapter = adapter
        tab_layout.setupWithViewPager(view_pager)

        mPresenters = arrayOf(
                GradeJurusanPresenter(mFragments[0], GradeJurusanRepository.getInstance()),
                GradeJurusanPresenter(mFragments[1], GradeJurusanRepository.getInstance()),
                GradeJurusanPresenter(mFragments[2], GradeJurusanRepository.getInstance())
        )
    }
}
