package illiyin.mhandharbeni.bicapps.feature.grade_jurusan

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.model.Ranking
import kotlinx.android.synthetic.main.base_item_ranking.view.*

class GradeJurusanRvAdapter : RecyclerView.Adapter<GradeJurusanRvAdapter.ItemViewHolder>() {

    var mRankingList: List<Ranking> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.base_item_ranking, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.fetch((position + 1).toString(), mRankingList[position])
    }

    override fun getItemCount() = mRankingList.size

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun fetch(number: String, ranking: Ranking) {
            itemView.text_number.text = number
            itemView.text_nama.text = ranking.nama
            itemView.text_score.text = ranking.score.toString()
        }
    }
}
