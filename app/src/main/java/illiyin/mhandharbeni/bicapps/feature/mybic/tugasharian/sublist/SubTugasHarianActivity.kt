package illiyin.mhandharbeni.bicapps.feature.mybic.tugasharian.sublist

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.repository.TugasHarianRepository
import illiyin.mhandharbeni.bicapps.feature.mybic.tugasharian.sublist.SubTugasHarianRvAdapter.OnItemClickListener
import illiyin.mhandharbeni.bicapps.feature.pdf.PdfActivity
import illiyin.mhandharbeni.bicapps.util.DummyPdf
import illiyin.mhandharbeni.bicapps.util.PdfSourceMode
import illiyin.mhandharbeni.bicapps.util.ProfilPage
import kotlinx.android.synthetic.main.base_activity_list.*

class SubTugasHarianActivity : AppCompatActivity(), SubTugasHarianContract.View {

    override lateinit var presenter: SubTugasHarianPresenter

    private lateinit var mSelectedTugasHarian: String
    private val mClickListener = object : OnItemClickListener {
        override fun onItemClick(subTugasHarian: String) {
            presenter.goToDetailPage(subTugasHarian)
        }
    }
    private val mAdapter = SubTugasHarianRvAdapter(this, mClickListener)

    companion object {
        private const val EXTRA_SELECTED_TUGAS_HARIAN = "tugas_harian"

        fun getIntent(context: Context, tugasHarian: String): Intent {
            val intent = Intent(context, SubTugasHarianActivity::class.java)
            intent.putExtra(EXTRA_SELECTED_TUGAS_HARIAN, tugasHarian)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.base_activity_list)

        mSelectedTugasHarian = intent.getStringExtra(EXTRA_SELECTED_TUGAS_HARIAN)
        presenter = SubTugasHarianPresenter(
                this,
                TugasHarianRepository.getInstance(this),
                mSelectedTugasHarian
        )

        with(refresh_layout) {
            setColorSchemeResources(R.color.primary)
            setOnRefreshListener { presenter.loadSubList() }
        }
        with(recycler_view) {
            layoutManager = LinearLayoutManager(this@SubTugasHarianActivity)
            adapter = mAdapter
        }

        text_empty_list.text = getString(
                R.string.sub_tugas_harian_msg_empty,
                mSelectedTugasHarian
        )
        supportActionBar?.title = getString(
                R.string.tugas_harian_subtitle_list,
                mSelectedTugasHarian
        )
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun showSubList(list: List<String>) {
        mAdapter.mSubTugasHarianList = list
        text_empty_list.visibility = View.GONE
    }

    override fun showEmptySubListMessage() {
        text_empty_list.visibility = View.VISIBLE
    }

    override fun onLoadSubListFailed(messageRes: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun openDetailPage(subTugasHarian: String) {
        val page = ProfilPage.TUGAS_HARIAN
        startActivity(PdfActivity.getIntent(
                this,
                page.titleRes,
                PdfSourceMode.FROM_LOCAL_FILE,
                DummyPdf.getFile(page)!!))
    }

    override fun setLoadingIndicator(active: Boolean) {
        refresh_layout.isRefreshing = active
    }
}