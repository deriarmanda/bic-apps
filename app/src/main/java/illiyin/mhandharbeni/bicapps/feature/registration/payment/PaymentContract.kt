package illiyin.mhandharbeni.bicapps.feature.registration.payment

import android.content.Intent
import android.support.annotation.StringRes
import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView

interface PaymentContract {

    interface Presenter : BasePresenter {

        fun uploadPayment()
        fun pickImageFromStorage()
        fun processSelectedImage(data: Intent)
    }

    interface View : BaseView<Presenter> {

        fun onUploadSucceed()
        fun onUploadFailed(@StringRes messageRes: Int)
        fun openStorage()
        fun showPaymentImagePath(path: String)
    }
}