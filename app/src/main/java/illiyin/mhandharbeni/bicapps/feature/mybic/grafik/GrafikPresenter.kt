package illiyin.mhandharbeni.bicapps.feature.mybic.grafik

import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.repository.EvaluasiRepository

class GrafikPresenter(
        private val mView: GrafikContract.View,
        private val mEvaluasiRepo: EvaluasiRepository
) : GrafikContract.Presenter {

    override fun loadListEvaluasi() {
        mView.setLoadingIndicator(true)
        val list = mEvaluasiRepo.getListEvaluasi()

        //mView.setLoadingIndicator(false)
        if (list.isEmpty()) mView.showEmptyListMessage(R.string.grafik_msg_empty_evaluasi)
        else mView.showListEvaluasi(list)
    }

    override fun loadListNilai(evaluasi: String) {
        mView.setLoadingIndicator(true)
        val map = mEvaluasiRepo.getListNilaiEvaluasi(evaluasi)

        //mView.setLoadingIndicator(false)
        if (map.isEmpty()) mView.showEmptyListMessage(R.string.grafik_msg_empty_nilai)
        else mView.showListNilai(map, evaluasi)
    }

    override fun start() = loadListEvaluasi()
}