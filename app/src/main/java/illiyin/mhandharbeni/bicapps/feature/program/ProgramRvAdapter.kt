package illiyin.mhandharbeni.bicapps.feature.program

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.model.Program
import kotlinx.android.synthetic.main.item_program.view.*

class ProgramRvAdapter(
        private val mClickListener: OnItemClickListener
) : RecyclerView.Adapter<ProgramRvAdapter.ItemViewHolder>() {

    var mProgramList: List<Program> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_program, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.fetch(mProgramList[position])
    }

    override fun getItemCount(): Int = mProgramList.size

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun fetch(program: Program) {
            itemView.text_title.text = program.judul
            itemView.setOnClickListener { mClickListener.onItemClick(program) }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(program: Program)
    }
}
