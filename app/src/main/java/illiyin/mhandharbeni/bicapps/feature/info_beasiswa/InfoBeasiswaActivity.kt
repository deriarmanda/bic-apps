package illiyin.mhandharbeni.bicapps.feature.info_beasiswa

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.model.Beasiswa
import illiyin.mhandharbeni.bicapps.data.repository.InfoBeasiswaRepository
import illiyin.mhandharbeni.bicapps.feature.info_beasiswa.InfoBeasiswaRvAdapter.OnItemClickListener
import illiyin.mhandharbeni.bicapps.feature.info_beasiswa.detail.DetailBeasiswaActivity
import illiyin.mhandharbeni.bicapps.util.buildErrorSnackbar
import kotlinx.android.synthetic.main.base_activity_list.*

class InfoBeasiswaActivity : AppCompatActivity(), InfoBeasiswaContract.View {

    override lateinit var presenter: InfoBeasiswaPresenter

    private val mItemClickListener = object : OnItemClickListener {
        override fun onItemClick(beasiswa: Beasiswa) {
            presenter.goToDetailBeasiswaPage(beasiswa)
        }
    }
    private val mAdapter = InfoBeasiswaRvAdapter(mItemClickListener)
    private lateinit var errorSnackbar: Snackbar

    companion object {
        fun getIntent(context: Context) = Intent(context, InfoBeasiswaActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.base_activity_list)

        presenter = InfoBeasiswaPresenter(this, InfoBeasiswaRepository.getInstance(this))

        with(refresh_layout) {
            setColorSchemeResources(R.color.primary)
            setOnRefreshListener { presenter.loadListBeasiswa(true) }
        }
        with(recycler_view) {
            adapter = mAdapter
            layoutManager = LinearLayoutManager(this@InfoBeasiswaActivity)
        }
        text_empty_list.setText(R.string.beasiswa_msg_empty)

        errorSnackbar = refresh_layout.buildErrorSnackbar(
                R.string.general_error_unknown,
                R.string.general_action_retry
        ) {
            presenter.loadListBeasiswa(true)
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun showListBeasiswa(beasiswaList: List<Beasiswa>) {
        mAdapter.mBeasiswaList = beasiswaList
        text_empty_list.visibility = View.GONE
    }

    override fun showEmptyListMessage() {
        text_empty_list.visibility = View.VISIBLE
    }

    override fun onLoadListBeasiswaFailed(message: String) {
        errorSnackbar.setText(message)
        errorSnackbar.show()
    }

    override fun openDetailBeasiswaPage(beasiswa: Beasiswa) {
        startActivity(DetailBeasiswaActivity.getIntent(this, beasiswa))
    }

    override fun setLoadingIndicator(active: Boolean) {
        refresh_layout.isRefreshing = active
    }
}
