package illiyin.mhandharbeni.bicapps.feature.grade_test.result

import illiyin.mhandharbeni.bicapps.data.repository.GradeTestRepository
import illiyin.mhandharbeni.bicapps.util.TestType

class ResultGradeTestPresenter(
        private val mView: ResultGradeTestContract.View,
        private val mGradeTestRepo: GradeTestRepository
) : ResultGradeTestContract.Presenter {

    private val mTest: TestType = TestType.TES_GRADE

    override fun loadResultDescription() {
        mView.setLoadingIndicator(true)
        val desc = mGradeTestRepo.getResultDescription()
        mTest.score = 90

        mView.onLoadResultSucceed(mTest, desc)
        mView.setLoadingIndicator(false)
    }

    override fun start() = loadResultDescription()
}