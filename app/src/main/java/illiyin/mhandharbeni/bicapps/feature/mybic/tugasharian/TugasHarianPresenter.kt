package illiyin.mhandharbeni.bicapps.feature.mybic.tugasharian

import illiyin.mhandharbeni.bicapps.data.repository.TugasHarianRepository

class TugasHarianPresenter(
        private val mView: TugasHarianContract.View,
        private val mTugasHarianRepo: TugasHarianRepository
) : TugasHarianContract.Presenter {

    override fun loadList() {
        mView.setLoadingIndicator(true)
        val list = mTugasHarianRepo.getList()

        mView.setLoadingIndicator(false)
        if (list.isEmpty()) mView.showEmptyListMessage()
        else mView.showList(list)
    }

    override fun goToSubListPage(tugasHarian: String) {
        mView.openSubListPage(tugasHarian)
    }

    override fun start() = loadList()
}