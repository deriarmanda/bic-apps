package illiyin.mhandharbeni.bicapps.feature.mybic.evaluasi

import illiyin.mhandharbeni.bicapps.data.repository.EvaluasiRepository

class EvaluasiPresenter(
        private val mView: EvaluasiContract.View,
        private val mEvaluasiRepo: EvaluasiRepository
) : EvaluasiContract.Presenter {

    override fun loadListEvaluasi() {
        mView.setLoadingIndicator(true)
        val list = mEvaluasiRepo.getListEvaluasi()

        mView.setLoadingIndicator(false)
        if (list.isEmpty()) mView.showEmptyListMessage()
        else mView.showListEvaluasi(list)
    }

    override fun goToSubListPage(evaluasi: String) {
        mView.openSubListPage(evaluasi)
    }

    override fun start() = loadListEvaluasi()
}