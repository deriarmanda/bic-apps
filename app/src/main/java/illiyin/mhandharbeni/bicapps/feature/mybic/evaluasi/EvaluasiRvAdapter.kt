package illiyin.mhandharbeni.bicapps.feature.mybic.evaluasi

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import illiyin.mhandharbeni.bicapps.R
import kotlinx.android.synthetic.main.base_item_list_bimbel.view.*

class EvaluasiRvAdapter(
        private val mContext: Context,
        private val mClickListener: OnItemClickListener
) : RecyclerView.Adapter<EvaluasiRvAdapter.ItemViewHolder>() {

    var mEvaluasiList: List<String> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.base_item_list_bimbel, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.fetch(mEvaluasiList[position])
    }

    override fun getItemCount() = mEvaluasiList.size

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun fetch(evaluasi: String) {
            itemView.text_title.text = evaluasi
            itemView.text_subtitle.text = mContext.getString(R.string.evaluasi_subtitle_list, evaluasi)
            itemView.setOnClickListener { mClickListener.onItemClick(evaluasi) }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(evaluasi: String)
    }
}