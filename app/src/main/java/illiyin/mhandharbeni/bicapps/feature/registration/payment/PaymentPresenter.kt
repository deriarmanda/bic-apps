package illiyin.mhandharbeni.bicapps.feature.registration.payment

import android.content.Intent

class PaymentPresenter(private val mView: PaymentContract.View) : PaymentContract.Presenter {

    override fun uploadPayment() {
        mView.onUploadSucceed()
    }

    override fun pickImageFromStorage() {
        mView.openStorage()
    }

    override fun processSelectedImage(data: Intent) {
        mView.showPaymentImagePath(data.dataString ?: "-")
    }

    override fun start() {}
}