package illiyin.mhandharbeni.bicapps.feature.tryout.ranking

import android.support.annotation.StringRes
import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView
import illiyin.mhandharbeni.bicapps.data.model.Ranking

interface RankingTryOutContract {

    interface Presenter : BasePresenter {

        fun loadListRanking()
    }

    interface View : BaseView<Presenter> {

        fun showListRanking(arrayList: ArrayList<Ranking>)
        fun showEmptyListMessage()
        fun onLoadListRankingFailed(@StringRes messageRes: Int)
    }
}