package illiyin.mhandharbeni.bicapps.feature.mybic.bukupretes

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.repository.PretesRepository
import illiyin.mhandharbeni.bicapps.feature.mybic.bukupretes.PretesRvAdapter.OnItemClickListener
import illiyin.mhandharbeni.bicapps.feature.mybic.bukupretes.sublist.SubPretesActivity
import kotlinx.android.synthetic.main.base_activity_list.*

class PretesActivity : AppCompatActivity(), PretesContract.View {

    override lateinit var presenter: PretesPresenter

    private val mItemClickListener = object : OnItemClickListener {
        override fun onItemClick(pretes: String) {
            presenter.goToSubListPage(pretes)
        }
    }
    private val mAdapter = PretesRvAdapter(this, mItemClickListener)

    companion object {
        fun getIntent(context: Context) = Intent(context, PretesActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.base_activity_list)

        presenter = PretesPresenter(this, PretesRepository.getInstance(this))

        with(refresh_layout) {
            setColorSchemeResources(R.color.primary)
            setOnRefreshListener { presenter.loadListPretes() }
        }
        with(recycler_view) {
            layoutManager = LinearLayoutManager(this@PretesActivity)
            adapter = mAdapter
        }
        text_empty_list.setText(R.string.pretes_msg_empty)
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun showListPretes(list: List<String>) {
        mAdapter.mPretesList = list
        text_empty_list.visibility = View.GONE
    }

    override fun showEmptyListMessage() {
        text_empty_list.visibility = View.VISIBLE
    }

    override fun onLoadListPretesFailed(messageRes: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun openSubListPage(pretes: String) {
        startActivity(SubPretesActivity.getIntent(this, pretes))
    }

    override fun setLoadingIndicator(active: Boolean) {
        refresh_layout.isRefreshing = active
    }
}