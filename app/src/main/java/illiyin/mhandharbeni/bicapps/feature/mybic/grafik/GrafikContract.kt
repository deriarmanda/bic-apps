package illiyin.mhandharbeni.bicapps.feature.mybic.grafik

import android.support.annotation.StringRes
import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView

interface GrafikContract {

    interface Presenter : BasePresenter {

        fun loadListEvaluasi()
        fun loadListNilai(evaluasi: String)
    }

    interface View : BaseView<Presenter> {

        fun showListEvaluasi(list: List<String>)
        fun onLoadListEvaluasiFailed(@StringRes messageRes: Int)
        fun showListNilai(map: Map<String, Int>, label: String)
        fun onLoadListNilaiFailed(@StringRes messageRes: Int)
        fun showEmptyListMessage(@StringRes messageRes: Int)
    }
}