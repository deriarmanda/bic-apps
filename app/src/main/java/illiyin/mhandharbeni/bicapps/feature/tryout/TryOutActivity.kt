package illiyin.mhandharbeni.bicapps.feature.tryout

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.repository.TryOutRepository
import illiyin.mhandharbeni.bicapps.feature.tryout.TryOutRvAdapter.OnItemClickListener
import illiyin.mhandharbeni.bicapps.feature.tryout.detail.DetailTryOutActivity
import kotlinx.android.synthetic.main.base_activity_list.*

class TryOutActivity : AppCompatActivity(), TryOutContract.View {

    override lateinit var presenter: TryOutPresenter

    private val mItemClickListener = object : OnItemClickListener {
        override fun onItemClick(tryout: String) {
            presenter.goToDetailTryOutPage(tryout)
        }
    }
    private val mAdapter = TryOutRvAdapter(mItemClickListener)

    companion object {
        fun getIntent(context: Context) = Intent(context, TryOutActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.base_activity_list)

        presenter = TryOutPresenter(this, TryOutRepository.getInstance())

        with(refresh_layout) {
            setColorSchemeResources(R.color.primary)
            setOnRefreshListener { presenter.loadListTryOut() }
        }
        with(recycler_view) {
            layoutManager = LinearLayoutManager(this@TryOutActivity)
            adapter = mAdapter
        }
        text_empty_list.setText(R.string.tryout_msg_empty)
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun showListTryOut(list: List<String>) {
        mAdapter.mTryOutList = list
        text_empty_list.visibility = View.GONE
    }

    override fun showEmptyListMessage() {
        text_empty_list.visibility = View.VISIBLE
    }

    override fun onLoadListTryOutFailed(messageRes: Int) {
        //TODO: implement onLoadListTryOutFailed function laters.
    }

    override fun openDetailTryOutPage(tryout: String) {
        startActivity(DetailTryOutActivity.getIntent(this, tryout))
    }

    override fun setLoadingIndicator(active: Boolean) {
        refresh_layout.isRefreshing = active
    }
}
