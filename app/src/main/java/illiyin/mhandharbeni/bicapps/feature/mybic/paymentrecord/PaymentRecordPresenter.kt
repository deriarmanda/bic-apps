package illiyin.mhandharbeni.bicapps.feature.mybic.paymentrecord

import illiyin.mhandharbeni.bicapps.data.model.PaymentRecord
import illiyin.mhandharbeni.bicapps.data.model.Program
import illiyin.mhandharbeni.bicapps.data.repository.PaymentRecordRepository
import illiyin.mhandharbeni.bicapps.data.repository.UserRepository

class PaymentRecordPresenter(
        private val mView: PaymentRecordContract.View,
        private val mUserRepo: UserRepository,
        private val mPayCordRepo: PaymentRecordRepository
) : PaymentRecordContract.Presenter {

    override fun loadProfil() {
        mView.setLoadingIndicator(true)
        mUserRepo.getUserProfile()

        mView.setLoadingIndicator(false)
        mView.onLoadProfilSucceed()
    }

    override fun loadPaidPrograms() {
        mView.setLoadingIndicator(true)
        val list = mPayCordRepo.getPaidPrograms()

        mView.setLoadingIndicator(false)
        if (list.isEmpty()) mView.showEmptyPaidProgramsMessage()
        else mView.showPaidPrograms(list)
    }

    override fun loadPaymentRecord(program: Program) {
        mView.setLoadingIndicator(true)
        val list = mPayCordRepo.getPaymentRecords(program)

        mView.setLoadingIndicator(false)
        mView.showPaymentRecord(list)
    }

    override fun goToPaymentPage(record: PaymentRecord) {
        mView.openPaymentPage(record)
    }

    override fun start() = loadProfil()
}