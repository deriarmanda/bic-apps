package illiyin.mhandharbeni.bicapps.feature.mybic.tugasharian.sublist

import illiyin.mhandharbeni.bicapps.data.repository.TugasHarianRepository

class SubTugasHarianPresenter(
        private val mView: SubTugasHarianContract.View,
        private val mTugasHarianRepo: TugasHarianRepository,
        private val mSelectedTugasHarian: String
) : SubTugasHarianContract.Presenter {

    override fun loadSubList() {
        mView.setLoadingIndicator(true)
        val list = mTugasHarianRepo.getSubList(mSelectedTugasHarian)

        mView.setLoadingIndicator(false)
        if (list.isEmpty()) mView.showEmptySubListMessage()
        else mView.showSubList(list)
    }

    override fun goToDetailPage(subTugasHarian: String) {
        mView.openDetailPage(subTugasHarian)
    }

    override fun start() = loadSubList()
}