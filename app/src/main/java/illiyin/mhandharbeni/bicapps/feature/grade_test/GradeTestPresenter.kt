package illiyin.mhandharbeni.bicapps.feature.grade_test

import illiyin.mhandharbeni.bicapps.data.model.Question
import illiyin.mhandharbeni.bicapps.data.repository.GradeTestRepository
import illiyin.mhandharbeni.bicapps.util.TestType

class GradeTestPresenter(
        private val mView: GradeTestContract.View,
        private val mGradeTestRepo: GradeTestRepository
) : GradeTestContract.Presenter {

    private lateinit var mQuestionList: ArrayList<Question>
    private val mTest: TestType = TestType.TES_GRADE

    override fun checkPageStatus() {
        mView.setLoadingIndicator(true)
        val allowed = mGradeTestRepo.getPageStatus()

        if (allowed) mView.onPageAllowed()
        else mView.onPageRestricted()
        mView.setLoadingIndicator(false)
    }

    override fun loadListQuestion() {
        mView.setLoadingIndicator(true)
        mQuestionList = mGradeTestRepo.getListQuestion()

        mTest.duration = 30000

        mView.onLoadListQuestionSucceed(mTest.duration)
        mView.setLoadingIndicator(false)
    }

    override fun goToRegistrationPage() {
        mView.openRegistrationPage()
    }

    override fun goToTestPage() {
        mView.openTestPage(mTest, mQuestionList)
    }

    override fun start() = checkPageStatus()
}