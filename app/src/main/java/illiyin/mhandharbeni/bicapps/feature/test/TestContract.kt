package illiyin.mhandharbeni.bicapps.feature.test

import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView
import illiyin.mhandharbeni.bicapps.data.model.Question
import illiyin.mhandharbeni.bicapps.util.TestType

interface TestContract {

    interface Presenter : BasePresenter {

        fun processNextQuestion()
        fun checkAnswer(answer: String)
    }

    interface View : BaseView<Presenter> {

        fun showEvenQuestion(question: Question)
        fun showOddQuestion(question: Question)
        fun updateTestNumber(currentNo: Int, totalNo: Int)
        fun startTimer()
        fun forceStopTimer()
        fun onTestFinished(test: TestType)
    }
}