package illiyin.mhandharbeni.bicapps.feature.mybic.bukuevaluasi

import illiyin.mhandharbeni.bicapps.data.repository.BukuEvaluasiRepository

class BukuEvaluasiPresenter(
        private val mView: BukuEvaluasiContract.View,
        private val mBukuEvaluasiRepo: BukuEvaluasiRepository
) : BukuEvaluasiContract.Presenter {

    override fun loadList() {
        mView.setLoadingIndicator(true)
        val list = mBukuEvaluasiRepo.getList()

        mView.setLoadingIndicator(false)
        if (list.isEmpty()) mView.showEmptyListMessage()
        else mView.showList(list)
    }

    override fun goToSubListPage(bukuEvaluasi: String) {
        mView.openSubListPage(bukuEvaluasi)
    }

    override fun start() = loadList()
}