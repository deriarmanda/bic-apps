package illiyin.mhandharbeni.bicapps.feature.klinik.psikotes.result

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.repository.PsikotesRepository
import illiyin.mhandharbeni.bicapps.util.DialogBuilder
import illiyin.mhandharbeni.bicapps.util.TestType
import kotlinx.android.synthetic.main.base_activity_result_test.*

class ResultPsikotesActivity : AppCompatActivity(), ResultPsikotesContract.View {

    override lateinit var presenter: ResultPsikotesContract.Presenter
    private lateinit var mLoadingDialog: DialogBuilder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.base_activity_result_test)

        presenter = ResultPsikotesPresenter(this, PsikotesRepository.getInstance())

        mLoadingDialog = DialogBuilder(this)
                .buildProgressDialog(R.string.test_msg_loading)

        text_title.text = getString(R.string.result_msg_title_psikotes)
        text_recap.visibility = View.GONE
        with(button_action) {
            text = getString(R.string.general_action_selesai)
            setOnClickListener { finish() }
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onLoadResultSucceed(test: TestType, description: String) {
        text_score.text = test.score.toString()
        text_description.text = description
    }

    override fun onLoadResultFailed(messageRes: Int) {
        //TODO: implement onLoadResultFailed function laters.
    }

    override fun setLoadingIndicator(active: Boolean) {
        if (active) mLoadingDialog.show()
        else mLoadingDialog.dismiss()
    }
}