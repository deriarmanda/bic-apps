package illiyin.mhandharbeni.bicapps.feature.mybic.rangkumanmateri.sublist

import illiyin.mhandharbeni.bicapps.data.repository.MateriRepository

class SubMateriPresenter(
        private val mView: SubMateriContract.View,
        private val mMateriRepo: MateriRepository,
        private val mSelectedMateri: String
) : SubMateriContract.Presenter {

    override fun loadSubListMateri() {
        mView.setLoadingIndicator(true)
        val list = mMateriRepo.getSubListMateri(mSelectedMateri)

        mView.setLoadingIndicator(false)
        if (list.isEmpty()) mView.showEmptySubListMessage()
        else mView.showSubListMateri(list)
    }

    override fun goToDetailMateriPage(subMateri: String) {
        mView.openDetailMateriPage(subMateri)
    }

    override fun start() = loadSubListMateri()
}