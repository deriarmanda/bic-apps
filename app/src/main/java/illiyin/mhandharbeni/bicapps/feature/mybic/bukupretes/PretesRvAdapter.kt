package illiyin.mhandharbeni.bicapps.feature.mybic.bukupretes

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import illiyin.mhandharbeni.bicapps.R
import kotlinx.android.synthetic.main.base_item_list_bimbel.view.*

class PretesRvAdapter(
        private val mContext: Context,
        private val mClickListener: OnItemClickListener
) : RecyclerView.Adapter<PretesRvAdapter.ItemViewHolder>() {

    var mPretesList: List<String> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.base_item_list_bimbel, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.fetch(mPretesList[position])
    }

    override fun getItemCount() = mPretesList.size

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun fetch(pretes: String) {
            itemView.text_title.text = pretes
            itemView.text_subtitle.text = mContext.getString(R.string.pretes_subtitle_list, pretes)
            itemView.setOnClickListener { mClickListener.onItemClick(pretes) }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(pretes: String)
    }
}
