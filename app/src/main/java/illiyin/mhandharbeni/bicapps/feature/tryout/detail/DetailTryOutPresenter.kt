package illiyin.mhandharbeni.bicapps.feature.tryout.detail

import illiyin.mhandharbeni.bicapps.data.model.Question
import illiyin.mhandharbeni.bicapps.data.repository.TryOutRepository
import illiyin.mhandharbeni.bicapps.util.TestType

class DetailTryOutPresenter(
        private val mView: DetailTryOutContract.View,
        private val mTryOutRepo: TryOutRepository,
        private val mSelectedTryOut: String
) : DetailTryOutContract.Presenter {

    private lateinit var mQuestionList: ArrayList<Question>
    private val test: TestType = TestType.TRY_OUT

    init {
        test.title = mSelectedTryOut
    }

    override fun loadListQuestion() {
        mView.setLoadingIndicator(true)
        mQuestionList = mTryOutRepo.getListQuestion(mSelectedTryOut)

        test.duration = 30000

        mView.onLoadListQuestionSucceed(test.duration)
        mView.setLoadingIndicator(false)
    }

    override fun goToTestPage() {
        mView.openTestPage(test, mQuestionList)
    }

    override fun start() = loadListQuestion()
}