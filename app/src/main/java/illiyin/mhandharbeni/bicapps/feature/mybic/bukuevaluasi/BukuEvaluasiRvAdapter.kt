package illiyin.mhandharbeni.bicapps.feature.mybic.bukuevaluasi

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import illiyin.mhandharbeni.bicapps.R
import kotlinx.android.synthetic.main.base_item_list_bimbel.view.*

class BukuEvaluasiRvAdapter(
        private val mContext: Context,
        private val mClickListener: OnItemClickListener
) : RecyclerView.Adapter<BukuEvaluasiRvAdapter.ItemViewHolder>() {

    var mBukuEvaluasiList: List<String> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.base_item_list_bimbel, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.fetch(mBukuEvaluasiList[position])
    }

    override fun getItemCount() = mBukuEvaluasiList.size

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun fetch(bukuEvaluasi: String) {
            itemView.text_title.text = bukuEvaluasi
            itemView.text_subtitle.text = mContext.getString(
                    R.string.buku_evaluasi_subtitle_list,
                    bukuEvaluasi
            )
            itemView.setOnClickListener { mClickListener.onItemClick(bukuEvaluasi) }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(bukuEvaluasi: String)
    }
}
