package illiyin.mhandharbeni.bicapps.feature.program

import illiyin.mhandharbeni.bicapps.base.BaseCallback
import illiyin.mhandharbeni.bicapps.data.model.Program
import illiyin.mhandharbeni.bicapps.data.repository.ProgramRepository

class ProgramPresenter(
        private val mView: ProgramContract.View,
        private val mProgramRepo: ProgramRepository
) : ProgramContract.Presenter {

    override fun loadListProgram(isRefreshed: Boolean) {
        mView.setLoadingIndicator(true)
        mProgramRepo.getListProgram(
                isRefreshed,
                object : BaseCallback<List<Program>> {
                    override fun onError(message: String) {
                        mView.setLoadingIndicator(false)
                        mView.onLoadListProgramFailed(message)
                    }

                    override fun onSuccess(data: List<Program>) {
                        mView.setLoadingIndicator(false)
                        if (data.isEmpty()) mView.showEmptyListMessage()
                        else mView.showListProgram(data)
                    }
                }
        )
    }

    override fun goToDetailProgramPage(program: Program) = mView.openDetailProgramPage(program)

    override fun goToPerbandinganProgramPage() = mView.openPerbandinganProgramPage()

    override fun start() = loadListProgram()
}