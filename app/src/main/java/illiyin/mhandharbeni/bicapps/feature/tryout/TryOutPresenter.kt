package illiyin.mhandharbeni.bicapps.feature.tryout

import illiyin.mhandharbeni.bicapps.data.repository.TryOutRepository

class TryOutPresenter(
        private val mView: TryOutContract.View,
        private val mTryOutRepo: TryOutRepository
) : TryOutContract.Presenter {

    override fun loadListTryOut() {
        mView.setLoadingIndicator(true)
        val list = mTryOutRepo.getListTryOut()

        if (list.isEmpty()) mView.showEmptyListMessage()
        else mView.showListTryOut(list)
        mView.setLoadingIndicator(false)
    }

    override fun goToDetailTryOutPage(tryout: String) {
        mView.openDetailTryOutPage(tryout)
    }

    override fun start() = loadListTryOut()
}