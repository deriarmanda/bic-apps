package illiyin.mhandharbeni.bicapps.feature.mybic.tugasharian

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import illiyin.mhandharbeni.bicapps.R
import kotlinx.android.synthetic.main.base_item_list_bimbel.view.*

class TugasHarianRvAdapter(
        private val mContext: Context,
        private val mClickListener: OnItemClickListener
) : RecyclerView.Adapter<TugasHarianRvAdapter.ItemViewHolder>() {

    var mTugasHarianList: List<String> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.base_item_list_bimbel, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.fetch(mTugasHarianList[position])
    }

    override fun getItemCount() = mTugasHarianList.size

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun fetch(tugasHarian: String) {
            itemView.text_title.text = tugasHarian
            itemView.text_subtitle.text = mContext.getString(
                    R.string.tugas_harian_subtitle_list,
                    tugasHarian
            )
            itemView.setOnClickListener { mClickListener.onItemClick(tugasHarian) }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(tugasHarian: String)
    }
}