package illiyin.mhandharbeni.bicapps.feature.mybic.paymentrecord

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.BaseAdapter
import android.widget.TextView
import com.squareup.picasso.Picasso
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.model.PaymentRecord
import illiyin.mhandharbeni.bicapps.data.model.Program
import illiyin.mhandharbeni.bicapps.data.repository.PaymentRecordRepository
import illiyin.mhandharbeni.bicapps.data.repository.UserRepository
import illiyin.mhandharbeni.bicapps.feature.mybic.paymentrecord.PaymentRecordRvAdapter.OnItemClickListener
import illiyin.mhandharbeni.bicapps.feature.registration.payment.PaymentActivity
import kotlinx.android.synthetic.main.activity_payment_record.*

class PaymentRecordActivity : AppCompatActivity(), PaymentRecordContract.View {

    override lateinit var presenter: PaymentRecordPresenter

    private val mListItemClickListener = object : OnItemClickListener {
        override fun onItemClick(record: PaymentRecord) {
            presenter.goToPaymentPage(record)
        }
    }
    private val mListAdapter = PaymentRecordRvAdapter(mListItemClickListener)
    private val mSpinnerAdapter = SpinnerCustomAdapter()

    companion object {
        fun getIntent(context: Context) = Intent(context, PaymentRecordActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_record)

        presenter = PaymentRecordPresenter(
                this,
                UserRepository.getInstance(applicationContext),
                PaymentRecordRepository.getInstance()
        )

        with(refresh_layout) {
            setColorSchemeResources(R.color.primary)
            setOnRefreshListener { presenter.loadPaidPrograms() }
        }
        with(spinner_program) {
            adapter = mSpinnerAdapter
            onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {}

                override fun onItemSelected(
                        spinner: AdapterView<*>?,
                        item: View?,
                        position: Int,
                        id: Long
                ) = presenter.loadPaymentRecord(spinner?.selectedItem as Program)
            }
        }
        with(list_record) {
            layoutManager = LinearLayoutManager(this@PaymentRecordActivity)
            adapter = mListAdapter
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onLoadProfilSucceed() {
        Picasso.get().load(R.drawable.img_materi).into(image_profil)
        text_fullname.text = getString(R.string.app_name)

        presenter.loadPaidPrograms()
    }

    override fun onLoadProfilFailed(messageRes: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showPaidPrograms(list: List<Program>) {
        mSpinnerAdapter.mListProgram = list
    }

    override fun showEmptyPaidProgramsMessage() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onLoadPaidProgramsFailed(messageRes: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showPaymentRecord(list: List<PaymentRecord>) {
        mListAdapter.mPaymentList = list
    }

    override fun onLoadPaymentRecordFailed(messageRes: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun openPaymentPage(record: PaymentRecord) {
        startActivity(PaymentActivity.getIntent(
                this,
                record.title,
                spinner_program.selectedItem.toString()
        ))
    }

    override fun setLoadingIndicator(active: Boolean) {
        refresh_layout.isRefreshing = active
    }

    inner class SpinnerCustomAdapter : BaseAdapter() {

        var mListProgram: List<Program> = emptyList()
            set(value) {
                field = value
                notifyDataSetChanged()
            }

        override fun getView(position: Int, recycledView: View?, parent: ViewGroup?): View {
            val view = (recycledView ?: layoutInflater.inflate(
                    android.R.layout.simple_list_item_1,
                    parent,
                    false
            )) as TextView
            view.text = mListProgram[position].judul

            return view
        }

        override fun getItem(position: Int) = mListProgram[position]

        override fun getItemId(position: Int) = position.toLong()

        override fun getCount() = mListProgram.size
    }

}
