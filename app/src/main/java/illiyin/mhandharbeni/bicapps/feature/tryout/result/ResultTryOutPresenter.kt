package illiyin.mhandharbeni.bicapps.feature.tryout.result

import illiyin.mhandharbeni.bicapps.data.repository.TryOutRepository
import illiyin.mhandharbeni.bicapps.util.TestType

class ResultTryOutPresenter(
        private val mView: ResultTryOutContract.View,
        private val mTryOutRepo: TryOutRepository
) : ResultTryOutContract.Presenter {

    private val mTest: TestType = TestType.PSIKOTES

    override fun loadResultDescription() {
        mView.setLoadingIndicator(true)
        val desc = mTryOutRepo.getResultDescription()
        mTest.score = 100

        mView.onLoadResultSucceed(mTest, desc)
        mView.setLoadingIndicator(false)
    }

    override fun start() = loadResultDescription()
}