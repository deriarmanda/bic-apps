package illiyin.mhandharbeni.bicapps.feature.mybic.bukuevaluasi

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.repository.BukuEvaluasiRepository
import illiyin.mhandharbeni.bicapps.feature.mybic.bukuevaluasi.BukuEvaluasiRvAdapter.OnItemClickListener
import illiyin.mhandharbeni.bicapps.feature.mybic.bukuevaluasi.sublist.SubBukuEvaluasiActivity
import kotlinx.android.synthetic.main.base_activity_list.*

class BukuEvaluasiActivity : AppCompatActivity(), BukuEvaluasiContract.View {

    override lateinit var presenter: BukuEvaluasiPresenter

    private val mItemClickListener = object : OnItemClickListener {
        override fun onItemClick(bukuEvaluasi: String) {
            presenter.goToSubListPage(bukuEvaluasi)
        }
    }
    private val mAdapter = BukuEvaluasiRvAdapter(this, mItemClickListener)

    companion object {
        fun getIntent(context: Context) = Intent(context, BukuEvaluasiActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.base_activity_list)

        presenter = BukuEvaluasiPresenter(this, BukuEvaluasiRepository.getInstance(this))

        with(refresh_layout) {
            setColorSchemeResources(R.color.primary)
            setOnRefreshListener { presenter.loadList() }
        }
        with(recycler_view) {
            layoutManager = LinearLayoutManager(this@BukuEvaluasiActivity)
            adapter = mAdapter
        }
        text_empty_list.setText(R.string.buku_evaluasi_msg_empty)
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun showList(list: List<String>) {
        mAdapter.mBukuEvaluasiList = list
        text_empty_list.visibility = View.GONE
    }

    override fun showEmptyListMessage() {
        text_empty_list.visibility = View.VISIBLE
    }

    override fun onLoadListFailed(messageRes: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun openSubListPage(bukuEvaluasi: String) {
        startActivity(SubBukuEvaluasiActivity.getIntent(this, bukuEvaluasi))
    }

    override fun setLoadingIndicator(active: Boolean) {
        refresh_layout.isRefreshing = active
    }
}
