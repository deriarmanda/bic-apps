package illiyin.mhandharbeni.bicapps.feature.grade_jurusan

import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class GradeJurusanPagerAdapter(
        mFragmentManager: FragmentManager,
        private val mFragments: Array<GradeJurusanFragment>,
        private val mTitles: Array<String>
) : FragmentPagerAdapter(mFragmentManager) {

    override fun getPageTitle(position: Int) = mTitles[position]

    override fun getItem(position: Int) = mFragments[position]

    override fun getCount() = mFragments.size

}