package illiyin.mhandharbeni.bicapps.feature.mybic.evaluasi.result

import illiyin.mhandharbeni.bicapps.data.repository.EvaluasiRepository
import illiyin.mhandharbeni.bicapps.util.TestType

class ResultEvaluasiPresenter(
        private val mView: ResultEvaluasiContract.View,
        private val mEvaluasiRepo: EvaluasiRepository
) : ResultEvaluasiContract.Presenter {

    private val mTest: TestType = TestType.PSIKOTES

    override fun loadResultDescription() {
        mView.setLoadingIndicator(true)
        val desc = mEvaluasiRepo.getResultDescription()
        mTest.score = 77

        mView.onLoadResultSucceed(mTest, desc)
        mView.setLoadingIndicator(false)
    }

    override fun start() = loadResultDescription()
}