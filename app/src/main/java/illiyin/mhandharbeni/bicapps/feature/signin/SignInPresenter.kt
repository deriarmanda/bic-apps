package illiyin.mhandharbeni.bicapps.feature.signin

import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.base.BaseCallback
import illiyin.mhandharbeni.bicapps.data.repository.UserRepository

class SignInPresenter(
        private val mView: SignInContract.View,
        private val mRepository: UserRepository
) : SignInContract.Presenter {

    override fun validateForm(username: String, password: String) {
        mView.setLoadingIndicator(true)
        var valid = true

        with(username) {
            if (isBlank()) {
                valid = false
                mView.onUsernameValidationError(R.string.general_error_blankform)
            }
        }
        with(password) {
            if (isBlank()) {
                valid = false
                mView.onPasswordValidationError(R.string.general_error_blankform)
            }
        }

        if (valid) {
            mView.onValidationSucceed()
            signIn(username, password)
        }
        else mView.setLoadingIndicator(false)
    }

    override fun signIn(username: String, password: String) {
        mView.setLoadingIndicator(true)
        mRepository.authenticate(
                username,
                password,
                object : BaseCallback<Unit> {
                    override fun onError(message: String) {
                        mView.setLoadingIndicator(false)
                        mView.onSignInFailed(message)
                    }

                    override fun onSuccess(data: Unit) {
                        mView.setLoadingIndicator(false)
                        mView.onSignInSucceed()
                    }
                }
        )
    }

    override fun goToSignUpPage() = mView.openSignUpPage()

    override fun start() { }

}