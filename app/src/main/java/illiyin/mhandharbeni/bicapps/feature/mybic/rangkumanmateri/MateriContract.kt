package illiyin.mhandharbeni.bicapps.feature.mybic.rangkumanmateri

import android.support.annotation.StringRes
import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView

interface MateriContract {

    interface Presenter : BasePresenter {

        fun loadListMateri()
        fun goToSubListPage(materi: String)
    }

    interface View : BaseView<Presenter> {

        fun showListMateri(list: List<String>)
        fun showEmptyListMessage()
        fun onLoadListMateriFailed(@StringRes messageRes: Int)
        fun openSubListPage(materi: String)
    }
}