package illiyin.mhandharbeni.bicapps.feature.mybic.paymentrecord

import android.support.annotation.StringRes
import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView
import illiyin.mhandharbeni.bicapps.data.model.PaymentRecord
import illiyin.mhandharbeni.bicapps.data.model.Program

interface PaymentRecordContract {

    interface Presenter : BasePresenter {

        fun loadProfil()
        fun loadPaidPrograms()
        fun loadPaymentRecord(program: Program)
        fun goToPaymentPage(record: PaymentRecord)
    }

    interface View : BaseView<Presenter> {

        fun onLoadProfilSucceed()
        fun onLoadProfilFailed(@StringRes messageRes: String)
        fun showPaidPrograms(list: List<Program>)
        fun showEmptyPaidProgramsMessage()
        fun onLoadPaidProgramsFailed(@StringRes messageRes: String)
        fun showPaymentRecord(list: List<PaymentRecord>)
        fun onLoadPaymentRecordFailed(@StringRes messageRes: String)
        fun openPaymentPage(record: PaymentRecord)
    }
}