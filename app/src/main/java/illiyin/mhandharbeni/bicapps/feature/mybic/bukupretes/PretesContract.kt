package illiyin.mhandharbeni.bicapps.feature.mybic.bukupretes

import android.support.annotation.StringRes
import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView

interface PretesContract {

    interface Presenter : BasePresenter {

        fun loadListPretes()
        fun goToSubListPage(pretes: String)
    }

    interface View : BaseView<Presenter> {

        fun showListPretes(list: List<String>)
        fun showEmptyListMessage()
        fun onLoadListPretesFailed(@StringRes messageRes: Int)
        fun openSubListPage(pretes: String)
    }
}