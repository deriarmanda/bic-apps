package illiyin.mhandharbeni.bicapps.feature.mybic

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.feature.pdf.PdfActivity
import illiyin.mhandharbeni.bicapps.util.DialogBuilder
import illiyin.mhandharbeni.bicapps.util.DummyPdf
import illiyin.mhandharbeni.bicapps.util.PdfSourceMode
import illiyin.mhandharbeni.bicapps.util.ProfilPage
import kotlinx.android.synthetic.main.base_activity_grid.*

class ProfilActivity : AppCompatActivity() {

    private val mMenuList = ProfilPage.values()

    companion object {
        fun getIntent(context: Context) = Intent(context, ProfilActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profil)

        with(grid_view) {
            adapter = GridMenuAdapter()
            setOnItemClickListener { _, _, position, _ ->
                openPage(mMenuList[position])
            }
        }
    }

    private fun openPage(page: ProfilPage) {
        when {
            page.clazz == ProfilActivity::class.java ->
                DialogBuilder(this).showUnderDevelopmentDialog()
            page.clazz == PdfActivity::class.java -> startActivity(PdfActivity.getIntent(
                    this,
                    page.titleRes,
                    PdfSourceMode.FROM_LOCAL_FILE,
                    DummyPdf.getFile(page)!!))
            else -> startActivity(Intent(this, page.clazz))
        }
    }

    inner class GridMenuAdapter : BaseAdapter() {

        override fun getView(position: Int, recycledView: View?, parent: ViewGroup?): View {
            val view = (recycledView ?: layoutInflater
                    .inflate(R.layout.base_item_grid_menu, parent, false)) as TextView

            view.setText(mMenuList[position].titleRes)

            val drawableTop = ResourcesCompat.getDrawable(
                    resources, mMenuList[position].logoRes, theme)
            view.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    null, drawableTop, null, null)

            return view
        }

        override fun getItem(position: Int) = mMenuList[position]

        override fun getItemId(position: Int) = position.toLong()

        override fun getCount() = mMenuList.size
    }

}
