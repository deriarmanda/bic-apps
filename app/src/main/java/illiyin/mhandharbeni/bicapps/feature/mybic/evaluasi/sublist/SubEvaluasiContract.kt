package illiyin.mhandharbeni.bicapps.feature.mybic.evaluasi.sublist

import android.support.annotation.StringRes
import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView

interface SubEvaluasiContract {

    interface Presenter : BasePresenter {

        fun loadSubListEvaluasi()
        fun goToDetailEvaluasiPage(subEvaluasi: String)
    }

    interface View : BaseView<Presenter> {

        fun showSubListEvaluasi(list: List<String>)
        fun showEmptySubListMessage()
        fun onLoadSubListEvaluasiFailed(@StringRes messageRes: Int)
        fun openDetailEvaluasiPage(subEvaluasi: String)
    }
}