package illiyin.mhandharbeni.bicapps.feature.mybic.rangkumanmateri.sublist

import android.support.annotation.StringRes
import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView

interface SubMateriContract {

    interface Presenter : BasePresenter {

        fun loadSubListMateri()
        fun goToDetailMateriPage(subMateri: String)
    }

    interface View : BaseView<Presenter> {

        fun showSubListMateri(list: List<String>)
        fun showEmptySubListMessage()
        fun onLoadSubListMateriFailed(@StringRes messageRes: Int)
        fun openDetailMateriPage(subMateri: String)
    }
}