package illiyin.mhandharbeni.bicapps.feature.mybic.evaluasi.sublist

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import illiyin.mhandharbeni.bicapps.R
import kotlinx.android.synthetic.main.base_item_list_bimbel.view.*

class SubEvaluasiRvAdapter(
        private val mContext: Context,
        private val mClickListener: OnItemClickListener
) : RecyclerView.Adapter<SubEvaluasiRvAdapter.ItemViewHolder>() {

    var mSubEvaluasiList: List<String> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.base_item_list_bimbel, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.fetch(mSubEvaluasiList[position])
    }

    override fun getItemCount() = mSubEvaluasiList.size

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun fetch(subEvaluasi: String) {
            itemView.text_title.text = subEvaluasi
            itemView.text_subtitle.text = mContext.getString(
                    R.string.sub_evaluasi_subtitle_list,
                    subEvaluasi
            )

            itemView.setOnClickListener { mClickListener.onItemClick(subEvaluasi) }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(subEvaluasi: String)
    }
}