package illiyin.mhandharbeni.bicapps.feature.program.perbandingan

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import illiyin.mhandharbeni.bicapps.R

class PerbandinganProgramActivity : AppCompatActivity() {

    companion object {
        fun getIntent(context: Context) = Intent(context, PerbandinganProgramActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_perbandingan_program)
    }
}
