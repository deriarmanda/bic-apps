package illiyin.mhandharbeni.bicapps.feature.info_beasiswa

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.model.Beasiswa
import kotlinx.android.synthetic.main.item_notification.view.*

class InfoBeasiswaRvAdapter constructor(
        private val mClickListener: OnItemClickListener
) : RecyclerView.Adapter<InfoBeasiswaRvAdapter.ItemViewHolder>() {

    var mBeasiswaList: List<Beasiswa> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_notification, parent, false)
        view.image_icon.setImageResource(R.drawable.ic_informasi)

        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.fetch(mBeasiswaList[position])
    }

    override fun getItemCount(): Int = mBeasiswaList.size

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun fetch(beasiswa: Beasiswa) {
            itemView.text_title.text = beasiswa.title
            itemView.text_subtitle.text = beasiswa.subtitle
            itemView.setOnClickListener { mClickListener.onItemClick(beasiswa) }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(beasiswa: Beasiswa)
    }
}
