package illiyin.mhandharbeni.bicapps.feature.mybic.rangkumanmateri

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import illiyin.mhandharbeni.bicapps.R
import kotlinx.android.synthetic.main.base_item_list_bimbel.view.*

class MateriRvAdapter(
        private val mContext: Context,
        private val mClickListener: OnItemClickListener
) : RecyclerView.Adapter<MateriRvAdapter.ItemViewHolder>() {

    var mMateriList: List<String> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.base_item_list_bimbel, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.fetch(mMateriList[position])
    }

    override fun getItemCount() = mMateriList.size

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun fetch(materi: String) {
            itemView.text_title.text = materi
            itemView.text_subtitle.text = mContext.getString(R.string.materi_subtitle_list, materi)
            itemView.setOnClickListener { mClickListener.onItemClick(materi) }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(materi: String)
    }
}
