package illiyin.mhandharbeni.bicapps.feature.klinik.psikotes

import android.support.annotation.StringRes
import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView
import illiyin.mhandharbeni.bicapps.data.model.Question
import illiyin.mhandharbeni.bicapps.util.TestType

interface PsikotesContract {

    interface Presenter : BasePresenter {

        fun checkPageStatus()
        fun loadListQuestion()
        fun goToRegistrationPage()
        fun goToTestPage()
    }

    interface View : BaseView<Presenter> {

        fun onPageRestricted()
        fun onPageAllowed()
        fun onCheckPageStatusFailed(@StringRes messageRes: Int)
        fun onLoadListQuestionSucceed(duration: Long)
        fun onLoadListQuestionFailed(@StringRes messageRes: Int)
        fun openRegistrationPage()
        fun openTestPage(type: TestType, list: ArrayList<Question>)
    }
}