package illiyin.mhandharbeni.bicapps.feature.tryout

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import illiyin.mhandharbeni.bicapps.R
import kotlinx.android.synthetic.main.item_program.view.*

class TryOutRvAdapter(
        private val mClickListener: OnItemClickListener
) : RecyclerView.Adapter<TryOutRvAdapter.ItemViewHolder>() {

    var mTryOutList: List<String> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_program, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.fetch(mTryOutList[position])
    }

    override fun getItemCount() = mTryOutList.size

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun fetch(tryout: String) {
            itemView.text_title.text = tryout
            itemView.setOnClickListener { mClickListener.onItemClick(tryout) }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(tryout: String)
    }
}
