package illiyin.mhandharbeni.bicapps.feature.mybic.bukuevaluasi.sublist

import android.support.annotation.StringRes
import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView

interface SubBukuEvaluasiContract {

    interface Presenter : BasePresenter {

        fun loadSubList()
        fun goToDetailPage(subBukuEvaluasi: String)
    }

    interface View : BaseView<Presenter> {

        fun showSubList(list: List<String>)
        fun showEmptySubListMessage()
        fun onLoadSubListFailed(@StringRes messageRes: Int)
        fun openDetailPage(subBukuEvaluasi: String)
    }
}