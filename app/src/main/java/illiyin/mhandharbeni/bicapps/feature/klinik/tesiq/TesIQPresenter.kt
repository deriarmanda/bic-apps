package illiyin.mhandharbeni.bicapps.feature.klinik.tesiq

import illiyin.mhandharbeni.bicapps.data.model.Question
import illiyin.mhandharbeni.bicapps.data.repository.TesIQRepository
import illiyin.mhandharbeni.bicapps.util.TestType

class TesIQPresenter(
        private val mView: TesIQContract.View,
        private val mTesIQRepo: TesIQRepository
) : TesIQContract.Presenter {

    private lateinit var mQuestionList: ArrayList<Question>
    private val test: TestType = TestType.TES_IQ

    override fun loadListQuestion() {
        mView.setLoadingIndicator(true)
        mQuestionList = mTesIQRepo.getListQuestion()

        test.duration = 30000

        mView.onLoadListQuestionSucceed(test.duration)
        mView.setLoadingIndicator(false)
    }

    override fun goToTestPage() {
        mView.openTestPage(test, mQuestionList)
    }

    override fun start() = loadListQuestion()
}