package illiyin.mhandharbeni.bicapps.feature.pengumuman

import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView
import illiyin.mhandharbeni.bicapps.data.model.Pengumuman

interface PengumumanContract {

    interface Presenter : BasePresenter {

        fun loadListPengumuman(isRefreshed: Boolean = false)
        fun goToDetailPengumumanPage(pengumuman: Pengumuman)
    }

    interface View : BaseView<Presenter> {

        fun showListPengumuman(pengumumanList: List<Pengumuman>)
        fun showEmptyListMessage()
        fun onLoadListPengumumanFailed(errorMsg: String)
        fun openDetailPengumumanPage(pengumuman: Pengumuman)
    }
}