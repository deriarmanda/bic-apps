package illiyin.mhandharbeni.bicapps.feature.mybic.evaluasi.sublist

import illiyin.mhandharbeni.bicapps.data.repository.EvaluasiRepository

class SubEvaluasiPresenter(
        private val mView: SubEvaluasiContract.View,
        private val mEvaluasiRepo: EvaluasiRepository,
        private val mSelectedEvaluasi: String
) : SubEvaluasiContract.Presenter {

    override fun loadSubListEvaluasi() {
        mView.setLoadingIndicator(true)
        val list = mEvaluasiRepo.getSubListEvaluasi(mSelectedEvaluasi)

        mView.setLoadingIndicator(false)
        if (list.isEmpty()) mView.showEmptySubListMessage()
        else mView.showSubListEvaluasi(list)
    }

    override fun goToDetailEvaluasiPage(subEvaluasi: String) {
        mView.openDetailEvaluasiPage(subEvaluasi)
    }

    override fun start() = loadSubListEvaluasi()
}