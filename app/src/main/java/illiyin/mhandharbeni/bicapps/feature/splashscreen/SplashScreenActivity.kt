package illiyin.mhandharbeni.bicapps.feature.splashscreen

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import illiyin.mhandharbeni.bicapps.feature.home.HomeActivity

class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        startActivity(HomeActivity.getIntent(this))
        finish()
    }

    override fun onPause() {
        super.onPause()
        finish()
    }
}
