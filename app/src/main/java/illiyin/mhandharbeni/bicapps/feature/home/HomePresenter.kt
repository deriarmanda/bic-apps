package illiyin.mhandharbeni.bicapps.feature.home

import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.repository.HomeRepository
import illiyin.mhandharbeni.bicapps.data.repository.UserRepository

class HomePresenter(
        private val mView: HomeContract.View,
        private val mUserRepo: UserRepository,
        private val mHomeRepo: HomeRepository
) : HomeContract.Presenter {

    override fun checkUserSession() {
        val userHasSession = mUserRepo.isUserAlreadyLogin()

        if (userHasSession) mView.onUserAlreadySignIn()
        else mView.onUserNotSignedIn()
    }

    override fun loadImageForSlider() {
        val urls = mHomeRepo.getImageSliderUrls()
        val placeholderRes = R.drawable.img_beasiswa
        val errorRes = R.drawable.img_materi

        mView.showImageForSlider(urls, placeholderRes, errorRes)
    }

    override fun signOut() {
        mUserRepo.clearUserSession()
        mView.onUserSignedOut()
    }

    override fun start() = checkUserSession()
}