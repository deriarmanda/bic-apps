package illiyin.mhandharbeni.bicapps.feature.tryout

import android.support.annotation.StringRes
import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView

interface TryOutContract {

    interface Presenter : BasePresenter {

        fun loadListTryOut()
        fun goToDetailTryOutPage(tryout: String)
    }

    interface View : BaseView<Presenter> {

        fun showListTryOut(list: List<String>)
        fun showEmptyListMessage()
        fun onLoadListTryOutFailed(@StringRes messageRes: Int)
        fun openDetailTryOutPage(tryout: String)
    }
}