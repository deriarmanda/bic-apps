package illiyin.mhandharbeni.bicapps.feature.pengumuman

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.model.Pengumuman
import illiyin.mhandharbeni.bicapps.data.repository.PengumumanRepository
import illiyin.mhandharbeni.bicapps.feature.pengumuman.PengumumanRvAdapter.OnItemClickListener
import illiyin.mhandharbeni.bicapps.feature.pengumuman.detail.DetailPengumumanActivity
import illiyin.mhandharbeni.bicapps.util.buildErrorSnackbar
import kotlinx.android.synthetic.main.base_activity_list.*

class PengumumanActivity : AppCompatActivity(), PengumumanContract.View {

    override lateinit var presenter: PengumumanPresenter

    private val mItemClickListener = object : OnItemClickListener {
        override fun onItemClick(pengumuman: Pengumuman) {
            presenter.goToDetailPengumumanPage(pengumuman)
        }
    }
    private val mAdapter = PengumumanRvAdapter(mItemClickListener)
    private lateinit var errorSnackbar: Snackbar

    companion object {
        fun getIntent(context: Context) = Intent(context, PengumumanActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.base_activity_list)

        presenter = PengumumanPresenter(this, PengumumanRepository.getInstance(this))

        with(refresh_layout) {
            setColorSchemeResources(R.color.primary)
            setOnRefreshListener { presenter.loadListPengumuman(true) }
        }
        with(recycler_view) {
            adapter = mAdapter
            layoutManager = LinearLayoutManager(this@PengumumanActivity)
        }
        text_empty_list.setText(R.string.pengumuman_msg_empty)

        errorSnackbar = refresh_layout.buildErrorSnackbar(
                R.string.general_error_unknown,
                R.string.general_action_retry
        ) {
            presenter.loadListPengumuman(true)
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun showListPengumuman(pengumumanList: List<Pengumuman>) {
        mAdapter.mPengumumanList = pengumumanList
        text_empty_list.visibility = View.GONE
    }

    override fun showEmptyListMessage() {
        text_empty_list.visibility = View.VISIBLE
    }

    override fun onLoadListPengumumanFailed(errorMsg: String) {
        errorSnackbar.setText(errorMsg)
        errorSnackbar.show()
    }

    override fun openDetailPengumumanPage(pengumuman: Pengumuman) {
        startActivity(DetailPengumumanActivity.getIntent(this, pengumuman))
    }

    override fun setLoadingIndicator(active: Boolean) {
        refresh_layout.isRefreshing = active
    }
}
