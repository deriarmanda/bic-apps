package illiyin.mhandharbeni.bicapps.feature.registration

import android.content.Intent
import illiyin.mhandharbeni.bicapps.data.model.RegistrationForm
import illiyin.mhandharbeni.bicapps.data.repository.RegistrationRepository
import illiyin.mhandharbeni.bicapps.util.RequestCode

class RegistrationPresenter(
        private val mView: RegistrationContract.View,
        private val mRegistrationRepo: RegistrationRepository
) : RegistrationContract.Presenter {

    override fun loadDataForSpinners() {
        mView.onLoadListProgramSucceed(mRegistrationRepo.getListProgram())
        mView.onLoadListJurusanSucceed(mRegistrationRepo.getListJurusan())
        mView.onLoadListUnivSucceed(mRegistrationRepo.getListUniv())
    }

    override fun validateForm(form: RegistrationForm) {
        //mView.setLoadingIndicator(true)
        submitForm(form)
    }

    override fun submitForm(form: RegistrationForm) {
        //mView.setLoadingIndicator(false)
        mView.onRegistrationSucceed()
    }

    override fun pickImageFromStorage(requestCode: Int) {
        mView.openStorage(requestCode)
    }

    override fun processSelectedImage(requestCode: Int, data: Intent) {
        when (requestCode) {
            RequestCode.PICK_PERSONAL_PHOTO -> mView.showPersonalImagePath(data.dataString ?: "-")
            RequestCode.PICK_FATHERS_PHOTO -> mView.showFathersImagePath(data.dataString ?: "-")
            RequestCode.PICK_MOTHERS_PHOTO -> mView.showMothersImagePath(data.dataString ?: "-")
        }
    }

    override fun start() = loadDataForSpinners()
}