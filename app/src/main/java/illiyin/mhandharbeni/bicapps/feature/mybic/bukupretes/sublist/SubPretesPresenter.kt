package illiyin.mhandharbeni.bicapps.feature.mybic.bukupretes.sublist

import illiyin.mhandharbeni.bicapps.data.repository.PretesRepository

class SubPretesPresenter(
        private val mView: SubPretesContract.View,
        private val mPretesRepo: PretesRepository,
        private val mSelectedPretes: String
) : SubPretesContract.Presenter {

    override fun loadSubList() {
        mView.setLoadingIndicator(true)
        val list = mPretesRepo.getSubListPretes(mSelectedPretes)

        mView.setLoadingIndicator(false)
        if (list.isEmpty()) mView.showEmptySubListMessage()
        else mView.showSubList(list)
    }

    override fun goToDetailPage(subPretes: String) {
        mView.openDetailPage(subPretes)
    }

    override fun start() = loadSubList()
}