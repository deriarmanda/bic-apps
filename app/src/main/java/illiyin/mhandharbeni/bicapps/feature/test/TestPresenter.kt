package illiyin.mhandharbeni.bicapps.feature.test

import illiyin.mhandharbeni.bicapps.data.model.Question
import illiyin.mhandharbeni.bicapps.util.TestType

class TestPresenter(
        private val mView: TestContract.View,
        private val mTestType: TestType,
        private val mQuestionList: List<Question>
) : TestContract.Presenter {

    companion object {
        var sCurrIndex: Int = -1   //Nomor soal sekarang
        var sTAnswerCount: Int = 0 //Jumlah jawaban benar
        var sFAnswerCount: Int = 0 //Jumlah jawaban salah
        var sEAnswerCount: Int = 0 //Jumlah jawaban kosong
    }

    override fun processNextQuestion() {
        mView.forceStopTimer()
        sCurrIndex++
        if (sCurrIndex >= mQuestionList.size) {
            mTestType.trueAnsCount = sTAnswerCount
            mTestType.falseAnsCount = sFAnswerCount
            mTestType.emptyAnsCount = sEAnswerCount

            mView.onTestFinished(mTestType)
            return
        }

        if (sCurrIndex % 2 == 0) mView.showEvenQuestion(mQuestionList[sCurrIndex])
        else mView.showOddQuestion(mQuestionList[sCurrIndex])

        mView.updateTestNumber(sCurrIndex + 1, mQuestionList.size)
        mView.startTimer()
    }

    override fun checkAnswer(answer: String) {
        if (answer == "kosong") sEAnswerCount++
        else {
            if (mQuestionList[sCurrIndex].answer == answer) sTAnswerCount++
            else sFAnswerCount++
        }

        processNextQuestion()
    }

    override fun start() = processNextQuestion()
}