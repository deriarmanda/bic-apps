package illiyin.mhandharbeni.bicapps.feature.mybic.evaluasi.result

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.repository.EvaluasiRepository
import illiyin.mhandharbeni.bicapps.feature.mybic.grafik.GrafikActivity
import illiyin.mhandharbeni.bicapps.util.DialogBuilder
import illiyin.mhandharbeni.bicapps.util.TestType
import kotlinx.android.synthetic.main.base_activity_result_test.*

class ResultEvaluasiActivity : AppCompatActivity(), ResultEvaluasiContract.View {

    override lateinit var presenter: ResultEvaluasiContract.Presenter
    private lateinit var mLoadingDialog: DialogBuilder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.base_activity_result_test)

        presenter = ResultEvaluasiPresenter(this, EvaluasiRepository.getInstance(this))

        mLoadingDialog = DialogBuilder(this)
                .buildProgressDialog(R.string.test_msg_loading)

        with(button_action) {
            text = getString(R.string.result_action_view_chart)
            setOnClickListener {
                startActivity(GrafikActivity.getIntent(this@ResultEvaluasiActivity))
            }
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onLoadResultSucceed(test: TestType, description: String) {
        text_title.text = getString(R.string.result_msg_title_general, test.title)
        text_score.text = test.score.toString()
        text_recap.text = getString(
                R.string.result_msg_test_recap,
                test.trueAnsCount,
                test.falseAnsCount,
                test.emptyAnsCount
        )
        text_description.text = description
    }

    override fun onLoadResultFailed(messageRes: Int) {
        //TODO: implement onLoadResultFailed function laters.
    }

    override fun setLoadingIndicator(active: Boolean) {
        if (active) mLoadingDialog.show()
        else mLoadingDialog.dismiss()
    }
}
