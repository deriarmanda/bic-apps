package illiyin.mhandharbeni.bicapps.feature.mybic.tugasharian.sublist

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import illiyin.mhandharbeni.bicapps.R
import kotlinx.android.synthetic.main.base_item_list_bimbel.view.*

class SubTugasHarianRvAdapter(
        private val mContext: Context,
        private val mClickListener: OnItemClickListener
) : RecyclerView.Adapter<SubTugasHarianRvAdapter.ItemViewHolder>() {

    var mSubTugasHarianList: List<String> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.base_item_list_bimbel, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.fetch(mSubTugasHarianList[position])
    }

    override fun getItemCount() = mSubTugasHarianList.size

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun fetch(subTugasHarian: String) {
            itemView.text_title.text = subTugasHarian
            itemView.text_subtitle.text = mContext.getString(
                    R.string.sub_tugas_harian_subtitle_list,
                    subTugasHarian
            )

            itemView.setOnClickListener { mClickListener.onItemClick(subTugasHarian) }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(subTugasHarian: String)
    }
}