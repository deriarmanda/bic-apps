package illiyin.mhandharbeni.bicapps.feature.program

import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView
import illiyin.mhandharbeni.bicapps.data.model.Program

class ProgramContract {

    interface Presenter : BasePresenter {

        fun loadListProgram(isRefreshed: Boolean = false)
        fun goToDetailProgramPage(program: Program)
        fun goToPerbandinganProgramPage()
    }

    interface View : BaseView<Presenter> {

        fun showListProgram(programList: List<Program>)
        fun showEmptyListMessage()
        fun onLoadListProgramFailed(message: String)
        fun openDetailProgramPage(program: Program)
        fun openPerbandinganProgramPage()
    }
}