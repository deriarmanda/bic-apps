package illiyin.mhandharbeni.bicapps.feature.mybic.bukuevaluasi

import android.support.annotation.StringRes
import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView

interface BukuEvaluasiContract {

    interface Presenter : BasePresenter {

        fun loadList()
        fun goToSubListPage(bukuEvaluasi: String)
    }

    interface View : BaseView<Presenter> {

        fun showList(list: List<String>)
        fun showEmptyListMessage()
        fun onLoadListFailed(@StringRes messageRes: Int)
        fun openSubListPage(bukuEvaluasi: String)
    }
}