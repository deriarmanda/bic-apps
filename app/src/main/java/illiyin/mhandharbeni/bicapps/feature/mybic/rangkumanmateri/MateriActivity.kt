package illiyin.mhandharbeni.bicapps.feature.mybic.rangkumanmateri

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.repository.MateriRepository
import illiyin.mhandharbeni.bicapps.feature.mybic.rangkumanmateri.MateriRvAdapter.OnItemClickListener
import illiyin.mhandharbeni.bicapps.feature.mybic.rangkumanmateri.sublist.SubMateriActivity
import kotlinx.android.synthetic.main.base_activity_list.*

class MateriActivity : AppCompatActivity(), MateriContract.View {

    override lateinit var presenter: MateriPresenter

    private val mItemClickListener = object : OnItemClickListener {
        override fun onItemClick(materi: String) {
            presenter.goToSubListPage(materi)
        }
    }
    private val mAdapter = MateriRvAdapter(this, mItemClickListener)

    companion object {
        fun getIntent(context: Context) = Intent(context, MateriActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.base_activity_list)

        presenter = MateriPresenter(this, MateriRepository.getInstance(this))

        with(refresh_layout) {
            setColorSchemeResources(R.color.primary)
            setOnRefreshListener { presenter.loadListMateri() }
        }
        with(recycler_view) {
            layoutManager = LinearLayoutManager(this@MateriActivity)
            adapter = mAdapter
        }
        text_empty_list.setText(R.string.materi_msg_empty)
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun showListMateri(list: List<String>) {
        mAdapter.mMateriList = list
        text_empty_list.visibility = View.GONE
    }

    override fun showEmptyListMessage() {
        text_empty_list.visibility = View.VISIBLE
    }

    override fun onLoadListMateriFailed(messageRes: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun openSubListPage(materi: String) {
        startActivity(SubMateriActivity.getIntent(this, materi))
    }

    override fun setLoadingIndicator(active: Boolean) {
        refresh_layout.isRefreshing = active
    }
}
