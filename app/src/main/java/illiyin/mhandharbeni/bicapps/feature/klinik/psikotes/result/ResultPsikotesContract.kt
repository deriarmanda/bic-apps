package illiyin.mhandharbeni.bicapps.feature.klinik.psikotes.result

import android.support.annotation.StringRes
import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView
import illiyin.mhandharbeni.bicapps.util.TestType

interface ResultPsikotesContract {

    interface Presenter : BasePresenter {

        fun loadResultDescription()
    }

    interface View : BaseView<Presenter> {

        fun onLoadResultSucceed(test: TestType, description: String)
        fun onLoadResultFailed(@StringRes messageRes: Int)
    }
}