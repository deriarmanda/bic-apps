package illiyin.mhandharbeni.bicapps.feature.mybic.tugasharian

import android.support.annotation.StringRes
import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView

interface TugasHarianContract {

    interface Presenter : BasePresenter {

        fun loadList()
        fun goToSubListPage(tugasHarian: String)
    }

    interface View : BaseView<Presenter> {

        fun showList(list: List<String>)
        fun showEmptyListMessage()
        fun onLoadListFailed(@StringRes messageRes: Int)
        fun openSubListPage(tugasHarian: String)
    }
}