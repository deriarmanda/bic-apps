package illiyin.mhandharbeni.bicapps.feature.pengumuman.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.model.Pengumuman
import kotlinx.android.synthetic.main.activity_detail_beasiswa.*

class DetailPengumumanActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_PENGUMUMAN = "pengumuman"

        fun getIntent(context: Context, pengumuman: Pengumuman): Intent {
            val intent = Intent(context, DetailPengumumanActivity::class.java)
            intent.putExtra(EXTRA_PENGUMUMAN, pengumuman)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_pengumuman)

        val pengumuman = intent.getParcelableExtra<Pengumuman>(EXTRA_PENGUMUMAN)
        fetch(pengumuman)
    }

    private fun fetch(pengumuman: Pengumuman) {
        text_title.text = pengumuman.title
        text_content.text = pengumuman.content
    }
}
