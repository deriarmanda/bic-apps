package illiyin.mhandharbeni.bicapps.feature.program

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.model.Program
import illiyin.mhandharbeni.bicapps.data.repository.ProgramRepository
import illiyin.mhandharbeni.bicapps.feature.pdf.PdfActivity
import illiyin.mhandharbeni.bicapps.feature.program.ProgramRvAdapter.OnItemClickListener
import illiyin.mhandharbeni.bicapps.feature.program.detail.DetailProgramActivity
import illiyin.mhandharbeni.bicapps.util.PdfSourceMode
import illiyin.mhandharbeni.bicapps.util.buildErrorSnackbar
import kotlinx.android.synthetic.main.activity_program.*

class ProgramActivity : AppCompatActivity(), ProgramContract.View {

    override lateinit var presenter: ProgramPresenter

    private val mItemClickListener = object : OnItemClickListener {
        override fun onItemClick(program: Program) {
            presenter.goToDetailProgramPage(program)
        }
    }
    private val mAdapter = ProgramRvAdapter(mItemClickListener)
    private lateinit var errorSnackbar: Snackbar

    companion object {
        fun getIntent(context: Context) = Intent(context, ProgramActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_program)

        presenter = ProgramPresenter(this, ProgramRepository.getInstance(this))

        with(refresh_layout) {
            setColorSchemeResources(R.color.primary)
            setOnRefreshListener { presenter.loadListProgram(true) }
        }
        with(recycler_view) {
            adapter = mAdapter
            layoutManager = LinearLayoutManager(this@ProgramActivity)
        }
        button_bandingkan.setOnClickListener { presenter.goToPerbandinganProgramPage() }
        text_empty_list.setText(R.string.program_msg_empty)

        errorSnackbar = refresh_layout.buildErrorSnackbar(
                R.string.general_error_unknown,
                R.string.general_action_retry
        ) {
            presenter.loadListProgram(true)
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun showListProgram(programList: List<Program>) {
        mAdapter.mProgramList = programList
        text_empty_list.visibility = View.GONE
    }

    override fun showEmptyListMessage() {
        text_empty_list.visibility = View.VISIBLE
    }

    override fun onLoadListProgramFailed(message: String) {
        errorSnackbar.setText(message)
        errorSnackbar.show()
    }

    override fun openDetailProgramPage(program: Program) {
        startActivity(DetailProgramActivity.getIntent(this, program))
    }

    override fun openPerbandinganProgramPage() {
        startActivity(PdfActivity.getIntent(
                this,
                R.string.title_perbandinganprogram,
//                PdfSourceMode.FROM_URL,
//                "http://bicprograms.com/wp-content/uploads/2018/04/MANDIRI-PTN-PTS.pdf"
                PdfSourceMode.FROM_LOCAL_FILE,
                "perbandingan_program.pdf"
        ))
    }

    override fun setLoadingIndicator(active: Boolean) {
        refresh_layout.isRefreshing = active
        errorSnackbar.dismiss()
    }
}
