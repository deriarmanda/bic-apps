package illiyin.mhandharbeni.bicapps.feature.mybic.rangkumanmateri.sublist

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.repository.MateriRepository
import illiyin.mhandharbeni.bicapps.feature.mybic.rangkumanmateri.detail.DetailMateriActivity
import illiyin.mhandharbeni.bicapps.feature.mybic.rangkumanmateri.sublist.SubMateriRvAdapter.OnItemClickListener
import kotlinx.android.synthetic.main.base_activity_list.*

class SubMateriActivity : AppCompatActivity(), SubMateriContract.View {

    override lateinit var presenter: SubMateriPresenter

    private lateinit var mSelectedMateri: String
    private val mClickListener = object : OnItemClickListener {
        override fun onItemClick(subMateri: String) {
            presenter.goToDetailMateriPage(subMateri)
        }
    }
    private val mAdapter = SubMateriRvAdapter(this, mClickListener)

    companion object {
        private const val EXTRA_SELECTED_MATERI = "materi"

        fun getIntent(context: Context, materi: String): Intent {
            val intent = Intent(context, SubMateriActivity::class.java)
            intent.putExtra(EXTRA_SELECTED_MATERI, materi)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.base_activity_list)

        mSelectedMateri = intent.getStringExtra(EXTRA_SELECTED_MATERI)
        presenter = SubMateriPresenter(
                this,
                MateriRepository.getInstance(this),
                mSelectedMateri
        )

        with(refresh_layout) {
            setColorSchemeResources(R.color.primary)
            setOnRefreshListener { presenter.loadSubListMateri() }
        }
        with(recycler_view) {
            layoutManager = LinearLayoutManager(this@SubMateriActivity)
            adapter = mAdapter
        }

        text_empty_list.text = getString(R.string.sub_materi_msg_empty, mSelectedMateri)
        supportActionBar?.title = getString(R.string.materi_subtitle_list, mSelectedMateri)
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun showSubListMateri(list: List<String>) {
        mAdapter.mSubMateriList = list
        text_empty_list.visibility = View.GONE
    }

    override fun showEmptySubListMessage() {
        text_empty_list.visibility = View.VISIBLE
    }

    override fun onLoadSubListMateriFailed(messageRes: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun openDetailMateriPage(subMateri: String) {
        startActivity(DetailMateriActivity.getIntent(this, subMateri))
    }

    override fun setLoadingIndicator(active: Boolean) {
        refresh_layout.isRefreshing = active
    }
}
