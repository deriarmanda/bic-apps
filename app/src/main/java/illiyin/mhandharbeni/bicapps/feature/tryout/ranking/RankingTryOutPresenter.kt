package illiyin.mhandharbeni.bicapps.feature.tryout.ranking

import illiyin.mhandharbeni.bicapps.data.repository.TryOutRepository

class RankingTryOutPresenter(
        private val mView: RankingTryOutContract.View,
        private val mTryOutRepo: TryOutRepository
) : RankingTryOutContract.Presenter {

    override fun loadListRanking() {
        mView.setLoadingIndicator(true)
        val list = mTryOutRepo.getListRanking()

        if (list.isEmpty()) mView.showEmptyListMessage()
        else mView.showListRanking(list)
        mView.setLoadingIndicator(false)
    }

    override fun start() = loadListRanking()
}