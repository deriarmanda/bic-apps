package illiyin.mhandharbeni.bicapps.feature.info_beasiswa

import illiyin.mhandharbeni.bicapps.base.BaseCallback
import illiyin.mhandharbeni.bicapps.data.model.Beasiswa
import illiyin.mhandharbeni.bicapps.data.repository.InfoBeasiswaRepository

class InfoBeasiswaPresenter(
        private val mView: InfoBeasiswaContract.View,
        private val mInfoBeasiswaRepo: InfoBeasiswaRepository
) : InfoBeasiswaContract.Presenter {

    override fun loadListBeasiswa(isRefreshed: Boolean) {
        mView.setLoadingIndicator(true)
        mInfoBeasiswaRepo.getListBeasiswa(
                isRefreshed,
                object : BaseCallback<List<Beasiswa>> {
                    override fun onError(message: String) {
                        mView.setLoadingIndicator(false)
                        mView.onLoadListBeasiswaFailed(message)
                    }

                    override fun onSuccess(data: List<Beasiswa>) {
                        mView.setLoadingIndicator(false)
                        if (data.isEmpty()) mView.showEmptyListMessage()
                        else mView.showListBeasiswa(data)
                    }
                }
        )
    }

    override fun goToDetailBeasiswaPage(beasiswa: Beasiswa) {
        mView.openDetailBeasiswaPage(beasiswa)
    }

    override fun start() = loadListBeasiswa()
}