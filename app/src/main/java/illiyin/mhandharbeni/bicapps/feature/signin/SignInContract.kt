package illiyin.mhandharbeni.bicapps.feature.signin

import android.support.annotation.StringRes
import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView

interface SignInContract {

    interface Presenter : BasePresenter {

        fun validateForm(username: String, password: String)
        fun signIn(username: String, password: String)
        fun goToSignUpPage()
    }

    interface View : BaseView<Presenter> {

        fun onValidationSucceed()
        fun onUsernameValidationError(@StringRes messageRes: Int)
        fun onPasswordValidationError(@StringRes messageRes: Int)
        fun onSignInSucceed()
        fun onSignInFailed(message: String)
        fun openSignUpPage()
    }
}