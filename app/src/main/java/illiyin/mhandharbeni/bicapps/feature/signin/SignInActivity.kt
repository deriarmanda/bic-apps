package illiyin.mhandharbeni.bicapps.feature.signin

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.repository.UserRepository
import illiyin.mhandharbeni.bicapps.feature.signup.SignUpActivity
import illiyin.mhandharbeni.bicapps.util.RequestCode
import illiyin.mhandharbeni.bicapps.util.buildErrorSnackbar
import illiyin.mhandharbeni.bicapps.util.showToast
import kotlinx.android.synthetic.main.activity_sign_in.*
import kotlinx.android.synthetic.main.partial_horizontal_progress_bar.*

class SignInActivity : AppCompatActivity(), SignInContract.View {

    override lateinit var presenter: SignInPresenter
    private lateinit var errorSnackbar: Snackbar

    companion object {
        fun getIntent(context: Context) = Intent(context, SignInActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)

        presenter = SignInPresenter(this, UserRepository.getInstance(applicationContext))

        errorSnackbar = root.buildErrorSnackbar(
                R.string.general_error_unknown,
                R.string.general_action_retry
        ) {
            button_login.performClick()
        }

        text_daftar.setOnClickListener { presenter.goToSignUpPage() }
        button_login.setOnClickListener {
            presenter.validateForm(
                    form_username.text.toString(),
                    form_password.text.toString()
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RequestCode.SIGN_UP && resultCode == RESULT_OK) {
            setResult(RESULT_OK)
            finish()
        } else super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onBackPressed() {
        setResult(RESULT_CANCELED)
        finish()
    }

    override fun setLoadingIndicator(active: Boolean) {
        form_username.isEnabled = !active
        form_password.isEnabled = !active
        button_login.isEnabled = !active
        text_daftar.isClickable = !active

        progress_bar.visibility = if (active) View.VISIBLE else View.GONE
        if (active && errorSnackbar.isShown) errorSnackbar.dismiss()
    }

    override fun onValidationSucceed() {
        input_layout_username.isErrorEnabled = false
        input_layout_password.isErrorEnabled = false
    }

    override fun onUsernameValidationError(@StringRes messageRes: Int) {
        input_layout_username.isErrorEnabled = true
        input_layout_username.error = getString(messageRes)
    }

    override fun onPasswordValidationError(@StringRes messageRes: Int) {
        input_layout_password.isErrorEnabled = true
        input_layout_password.error = getString(messageRes)
    }

    override fun onSignInSucceed() {
        showToast(R.string.account_msg_signin_success)
        setResult(RESULT_OK)
        finish()
    }

    override fun onSignInFailed(message: String) {
        errorSnackbar.setText(message)
        errorSnackbar.show()
    }

    override fun openSignUpPage() {
        startActivityForResult(
                SignUpActivity.getIntent(this),
                RequestCode.SIGN_UP
        )
    }

}
