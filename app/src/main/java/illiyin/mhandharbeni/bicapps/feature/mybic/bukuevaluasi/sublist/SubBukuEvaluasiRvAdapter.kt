package illiyin.mhandharbeni.bicapps.feature.mybic.bukuevaluasi.sublist

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import illiyin.mhandharbeni.bicapps.R
import kotlinx.android.synthetic.main.base_item_list_bimbel.view.*

class SubBukuEvaluasiRvAdapter(
        private val mContext: Context,
        private val mClickListener: OnItemClickListener
) : RecyclerView.Adapter<SubBukuEvaluasiRvAdapter.ItemViewHolder>() {

    var mSubBukuEvaluasiList: List<String> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.base_item_list_bimbel, parent, false)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.fetch(mSubBukuEvaluasiList[position])
    }

    override fun getItemCount() = mSubBukuEvaluasiList.size

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun fetch(subBukuEvaluasi: String) {
            itemView.text_title.text = subBukuEvaluasi
            itemView.text_subtitle.text = mContext.getString(
                    R.string.sub_buku_evaluasi_subtitle_list,
                    subBukuEvaluasi
            )

            itemView.setOnClickListener { mClickListener.onItemClick(subBukuEvaluasi) }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(subBukuEvaluasi: String)
    }
}