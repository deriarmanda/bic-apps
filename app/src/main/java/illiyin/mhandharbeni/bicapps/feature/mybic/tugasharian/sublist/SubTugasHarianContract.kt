package illiyin.mhandharbeni.bicapps.feature.mybic.tugasharian.sublist

import android.support.annotation.StringRes
import illiyin.mhandharbeni.bicapps.base.BasePresenter
import illiyin.mhandharbeni.bicapps.base.BaseView

interface SubTugasHarianContract {

    interface Presenter : BasePresenter {

        fun loadSubList()
        fun goToDetailPage(subTugasHarian: String)
    }

    interface View : BaseView<Presenter> {

        fun showSubList(list: List<String>)
        fun showEmptySubListMessage()
        fun onLoadSubListFailed(@StringRes messageRes: Int)
        fun openDetailPage(subTugasHarian: String)
    }
}