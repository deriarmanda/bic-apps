package illiyin.mhandharbeni.bicapps.feature.signup

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.repository.UserRepository
import illiyin.mhandharbeni.bicapps.util.buildErrorSnackbar
import illiyin.mhandharbeni.bicapps.util.showToast
import kotlinx.android.synthetic.main.activity_sign_up.*
import kotlinx.android.synthetic.main.partial_horizontal_progress_bar.*

class SignUpActivity : AppCompatActivity(), SignUpContract.View {

    override lateinit var presenter: SignUpPresenter
    private lateinit var errorSnackbar: Snackbar

    companion object {
        fun getIntent(context: Context) = Intent(context, SignUpActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        presenter = SignUpPresenter(this, UserRepository.getInstance(applicationContext))

        errorSnackbar = root.buildErrorSnackbar(
                R.string.general_error_unknown,
                R.string.general_action_retry
        ) {
            button_daftar.performClick()
        }

        text_login.setOnClickListener { presenter.goToSignInPage() }
        button_daftar.setOnClickListener {
            presenter.validateForm(
                    form_name.text.toString(),
                    form_username.text.toString(),
                    form_email.text.toString(),
                    form_password.text.toString()
            )
        }
    }

    override fun onValidationSucceed() {
        input_layout_name.isErrorEnabled = false
        input_layout_username.isErrorEnabled = false
        input_layout_email.isErrorEnabled = false
        input_layout_password.isErrorEnabled = false
    }

    override fun onFullnameValidationError(@StringRes messageRes: Int) {
        input_layout_name.isErrorEnabled = true
        input_layout_name.error = getString(messageRes)
    }

    override fun onUsernameValidationError(@StringRes messageRes: Int) {
        input_layout_username.isErrorEnabled = true
        input_layout_username.error = getString(messageRes)
    }

    override fun onEmailValidationError(@StringRes messageRes: Int) {
        input_layout_email.isErrorEnabled = true
        input_layout_email.error = getString(messageRes)
    }

    override fun onPasswordValidationError(@StringRes messageRes: Int) {
        input_layout_password.isErrorEnabled = true
        input_layout_password.error = getString(messageRes)
    }

    override fun onSignUpSucceed(name: String) {
        showToast(getString(R.string.account_msg_signup_success, name), Toast.LENGTH_LONG)
        setResult(RESULT_OK)
        finish()
    }

    override fun onSignUpFailed(message: String) {
        errorSnackbar.setText(message)
        errorSnackbar.show()
    }

    override fun openSignInPage() {
        setResult(RESULT_CANCELED)
        finish()
    }

    override fun setLoadingIndicator(active: Boolean) {
        form_name.isEnabled = !active
        form_username.isEnabled = !active
        form_email.isEnabled = !active
        form_password.isEnabled = !active
        button_daftar.isEnabled = !active
        text_login.isClickable = !active

        progress_bar.visibility = if (active) View.VISIBLE else View.GONE
        if (active && errorSnackbar.isShown) errorSnackbar.dismiss()
    }

}
