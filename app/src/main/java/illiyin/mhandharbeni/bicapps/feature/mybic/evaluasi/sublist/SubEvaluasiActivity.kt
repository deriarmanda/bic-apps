package illiyin.mhandharbeni.bicapps.feature.mybic.evaluasi.sublist

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.repository.EvaluasiRepository
import illiyin.mhandharbeni.bicapps.feature.mybic.evaluasi.detail.DetailEvaluasiActivity
import illiyin.mhandharbeni.bicapps.feature.mybic.evaluasi.sublist.SubEvaluasiRvAdapter.OnItemClickListener
import kotlinx.android.synthetic.main.base_activity_list.*

class SubEvaluasiActivity : AppCompatActivity(), SubEvaluasiContract.View {

    override lateinit var presenter: SubEvaluasiPresenter
    private lateinit var mSelectedEvaluasi: String
    private val mClickListener = object : OnItemClickListener {
        override fun onItemClick(subEvaluasi: String) {
            presenter.goToDetailEvaluasiPage(subEvaluasi)
        }
    }
    private val mAdapter = SubEvaluasiRvAdapter(this, mClickListener)

    companion object {
        private const val EXTRA_SELECTED_EVALUASI = "evaluasi"

        fun getIntent(context: Context, evaluasi: String): Intent {
            val intent = Intent(context, SubEvaluasiActivity::class.java)
            intent.putExtra(EXTRA_SELECTED_EVALUASI, evaluasi)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.base_activity_list)

        mSelectedEvaluasi = intent.getStringExtra(EXTRA_SELECTED_EVALUASI)
        presenter = SubEvaluasiPresenter(
                this,
                EvaluasiRepository.getInstance(this),
                mSelectedEvaluasi
        )

        with(refresh_layout) {
            setColorSchemeResources(R.color.primary)
            setOnRefreshListener { presenter.loadSubListEvaluasi() }
        }
        with(recycler_view) {
            layoutManager = LinearLayoutManager(this@SubEvaluasiActivity)
            adapter = mAdapter
        }

        text_empty_list.text = getString(R.string.sub_evaluasi_msg_empty, mSelectedEvaluasi)
        supportActionBar?.title = getString(R.string.evaluasi_subtitle_list, mSelectedEvaluasi)
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun showSubListEvaluasi(list: List<String>) {
        mAdapter.mSubEvaluasiList = list
        text_empty_list.visibility = View.GONE
    }

    override fun showEmptySubListMessage() {
        text_empty_list.visibility = View.VISIBLE
    }

    override fun onLoadSubListEvaluasiFailed(messageRes: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun openDetailEvaluasiPage(subEvaluasi: String) {
        startActivity(DetailEvaluasiActivity.getIntent(this, subEvaluasi))
    }

    override fun setLoadingIndicator(active: Boolean) {
        refresh_layout.isRefreshing = active
    }
}