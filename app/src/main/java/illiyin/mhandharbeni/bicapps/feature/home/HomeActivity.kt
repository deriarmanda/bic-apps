package illiyin.mhandharbeni.bicapps.feature.home

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.repository.HomeRepository
import illiyin.mhandharbeni.bicapps.data.repository.UserRepository
import illiyin.mhandharbeni.bicapps.feature.grade_jurusan.GradeJurusanActivity
import illiyin.mhandharbeni.bicapps.feature.grade_test.GradeTestActivity
import illiyin.mhandharbeni.bicapps.feature.home.slider.HomeImageLoadingService
import illiyin.mhandharbeni.bicapps.feature.home.slider.HomeSliderAdapter
import illiyin.mhandharbeni.bicapps.feature.info_beasiswa.InfoBeasiswaActivity
import illiyin.mhandharbeni.bicapps.feature.klinik.KlinikActivity
import illiyin.mhandharbeni.bicapps.feature.mybic.ProfilActivity
import illiyin.mhandharbeni.bicapps.feature.pengumuman.PengumumanActivity
import illiyin.mhandharbeni.bicapps.feature.program.ProgramActivity
import illiyin.mhandharbeni.bicapps.feature.registration.RegistrationActivity
import illiyin.mhandharbeni.bicapps.feature.signin.SignInActivity
import illiyin.mhandharbeni.bicapps.feature.tryout.TryOutActivity
import illiyin.mhandharbeni.bicapps.util.RequestCode
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.partial_home_menu_daftar_bimbingan.*
import kotlinx.android.synthetic.main.partial_home_menu_grade_jurusan.*
import kotlinx.android.synthetic.main.partial_home_menu_info_beasiswa.*
import kotlinx.android.synthetic.main.partial_home_menu_pengumuman.*
import kotlinx.android.synthetic.main.partial_home_menu_profil.*
import kotlinx.android.synthetic.main.partial_home_menu_programbic.*
import kotlinx.android.synthetic.main.partial_home_menu_tes_grade.*
import kotlinx.android.synthetic.main.partial_home_menu_tespsikologi.*
import kotlinx.android.synthetic.main.partial_home_menu_try_out.*
import ss.com.bannerslider.Slider

class HomeActivity : AppCompatActivity(), HomeContract.View {

    override lateinit var presenter: HomePresenter

    companion object {
        fun getIntent(context: Context) = Intent(context, HomeActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        presenter = HomePresenter(
                this,
                UserRepository.getInstance(applicationContext),
                HomeRepository.getInstance()
        )

        Slider.init(HomeImageLoadingService())
        fetchMenus()
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            RequestCode.SIGN_IN -> {
                if (resultCode == RESULT_OK) presenter.loadImageForSlider()
                else finish()
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.activity_home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_logout -> {
                presenter.signOut()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onUserAlreadySignIn() {
        presenter.loadImageForSlider()
    }

    override fun onUserNotSignedIn() {
        startActivityForResult(
                SignInActivity.getIntent(this),
                RequestCode.SIGN_IN)
    }

    override fun onUserSignedOut() {
        startActivityForResult(
                SignInActivity.getIntent(this),
                RequestCode.SIGN_IN)
    }

    override fun showImageForSlider(
            urls: List<String>,
            placeholderDrawableRes: Int,
            errorDrawableRes: Int
    ) {
        val adapter = HomeSliderAdapter(urls, placeholderDrawableRes, errorDrawableRes)
        image_slider.setAdapter(adapter)
        //image_slider.sliderAdapter = adapter
    }

    override fun setLoadingIndicator(active: Boolean) {}

    private fun fetchMenus() {
        // Row 1
        card_profil.setOnClickListener {
            startActivity(ProfilActivity.getIntent(this))
        }
        card_program.setOnClickListener {
            startActivity(ProgramActivity.getIntent(this))
        }
        card_klinik.setOnClickListener {
            startActivity(KlinikActivity.getIntent(this))
        }

        // Row 2
        card_try_out.setOnClickListener {
            startActivity(TryOutActivity.getIntent(this))
        }
        card_grade_jurusan.setOnClickListener {
            startActivity(GradeJurusanActivity.getIntent(this))
        }
        card_tes_grade.setOnClickListener {
            startActivity(GradeTestActivity.getIntent(this))
        }

        // Row 3
        card_pengumuman.setOnClickListener {
            startActivity(PengumumanActivity.getIntent(this))
        }
        card_info_beasiswa.setOnClickListener {
            startActivity(InfoBeasiswaActivity.getIntent(this))
        }
        card_daftar_bimbingan.setOnClickListener {
            startActivity(RegistrationActivity.getIntent(this))
        }
    }

}
