package illiyin.mhandharbeni.bicapps.feature.mybic.evaluasi

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.repository.EvaluasiRepository
import illiyin.mhandharbeni.bicapps.feature.mybic.evaluasi.EvaluasiRvAdapter.OnItemClickListener
import illiyin.mhandharbeni.bicapps.feature.mybic.evaluasi.sublist.SubEvaluasiActivity
import kotlinx.android.synthetic.main.base_activity_list.*

class EvaluasiActivity : AppCompatActivity(), EvaluasiContract.View {

    override lateinit var presenter: EvaluasiPresenter

    private val mItemClickListener = object : OnItemClickListener {
        override fun onItemClick(evaluasi: String) {
            presenter.goToSubListPage(evaluasi)
        }
    }
    private val mAdapter = EvaluasiRvAdapter(this, mItemClickListener)

    companion object {
        fun getIntent(context: Context) = Intent(context, EvaluasiActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.base_activity_list)

        presenter = EvaluasiPresenter(this, EvaluasiRepository.getInstance(this))

        with(refresh_layout) {
            setColorSchemeResources(R.color.primary)
            setOnRefreshListener { presenter.loadListEvaluasi() }
        }
        with(recycler_view) {
            layoutManager = LinearLayoutManager(this@EvaluasiActivity)
            adapter = mAdapter
        }
        text_empty_list.setText(R.string.evaluasi_msg_empty)
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun showListEvaluasi(list: List<String>) {
        mAdapter.mEvaluasiList = list
        text_empty_list.visibility = View.GONE
    }

    override fun showEmptyListMessage() {
        text_empty_list.visibility = View.VISIBLE
    }

    override fun onLoadListEvaluasiFailed(messageRes: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun openSubListPage(evaluasi: String) {
        startActivity(SubEvaluasiActivity.getIntent(this, evaluasi))
    }

    override fun setLoadingIndicator(active: Boolean) {
        refresh_layout.isRefreshing = active
    }
}