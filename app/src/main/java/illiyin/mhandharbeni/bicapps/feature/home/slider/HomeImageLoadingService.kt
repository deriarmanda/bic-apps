package illiyin.mhandharbeni.bicapps.feature.home.slider

import android.widget.ImageView

import com.squareup.picasso.Picasso

import ss.com.bannerslider.ImageLoadingService

class HomeImageLoadingService : ImageLoadingService {

    override fun loadImage(url: String, imageView: ImageView) {
        Picasso.get().load(url).into(imageView)
    }

    override fun loadImage(res: Int, imageView: ImageView) {
        Picasso.get().load(res).into(imageView)
    }

    override fun loadImage(url: String, placeholderRes: Int, errorRes: Int, imageView: ImageView) {
        Picasso.get().load(url)
                .placeholder(placeholderRes)
                .error(errorRes)
                .into(imageView)
    }
}
