package illiyin.mhandharbeni.bicapps.feature.grade_test

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.model.Question
import illiyin.mhandharbeni.bicapps.data.repository.GradeTestRepository
import illiyin.mhandharbeni.bicapps.feature.registration.RegistrationActivity
import illiyin.mhandharbeni.bicapps.feature.test.TestActivity
import illiyin.mhandharbeni.bicapps.util.DialogBuilder
import illiyin.mhandharbeni.bicapps.util.TestType
import kotlinx.android.synthetic.main.activity_grade_test.*
import kotlinx.android.synthetic.main.base_activity_init_test.*
import kotlinx.android.synthetic.main.base_activity_restricted.*
import java.util.*

class GradeTestActivity : AppCompatActivity(), GradeTestContract.View {

    override lateinit var presenter: GradeTestPresenter

    private lateinit var mTitle: String
    private lateinit var mLoadingDialog: DialogBuilder

    companion object {
        fun getIntent(context: Context) = Intent(context, GradeTestActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grade_test)

        presenter = GradeTestPresenter(this, GradeTestRepository.getInstance())
        mTitle = getString(R.string.title_tesgrade)
        mLoadingDialog = DialogBuilder(this)
                .buildProgressDialog(R.string.test_msg_loading)

        button_daftar.setOnClickListener { presenter.goToRegistrationPage() }
        with(button_mulai) {
            text = getString(R.string.test_action_mulai, mTitle)
            setOnClickListener { presenter.goToTestPage() }
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onPageRestricted() {
        layout_rules.visibility = View.GONE
        layout_restricted_info.visibility = View.VISIBLE
    }

    override fun onPageAllowed() {
        layout_rules.visibility = View.VISIBLE
        layout_restricted_info.visibility = View.GONE

        presenter.loadListQuestion()
    }

    override fun onCheckPageStatusFailed(messageRes: Int) {
        //TODO: implement onCheckPageStatusFailed function laters.
    }

    override fun onLoadListQuestionSucceed(duration: Long) {
        text_rules.text = getString(R.string.test_msg_rules, mTitle, duration)
    }

    override fun onLoadListQuestionFailed(messageRes: Int) {
        //TODO: implement onLoadListQuestionFailed function laters.
    }

    override fun openRegistrationPage() {
        startActivity(RegistrationActivity.getIntent(this))
    }

    override fun openTestPage(type: TestType, list: ArrayList<Question>) {
        startActivity(TestActivity.getIntent(
                this,
                type,
                list
        ))
    }

    override fun setLoadingIndicator(active: Boolean) {
        if (active) mLoadingDialog.show()
        else mLoadingDialog.dismiss()
    }
}