package illiyin.mhandharbeni.bicapps.feature.mybic.bukupretes

import illiyin.mhandharbeni.bicapps.data.repository.PretesRepository

class PretesPresenter(
        private val mView: PretesContract.View,
        private val mPretesRepo: PretesRepository
) : PretesContract.Presenter {

    override fun loadListPretes() {
        mView.setLoadingIndicator(true)
        val list = mPretesRepo.getListPretes()

        mView.setLoadingIndicator(false)
        if (list.isEmpty()) mView.showEmptyListMessage()
        else mView.showListPretes(list)
    }

    override fun goToSubListPage(pretes: String) {
        mView.openSubListPage(pretes)
    }

    override fun start() = loadListPretes()
}