package illiyin.mhandharbeni.bicapps.feature.mybic.bukuevaluasi.sublist

import illiyin.mhandharbeni.bicapps.data.repository.BukuEvaluasiRepository

class SubBukuEvaluasiPresenter(
        private val mView: SubBukuEvaluasiContract.View,
        private val mPretesRepo: BukuEvaluasiRepository,
        private val mSelectedBukuEvaluasi: String
) : SubBukuEvaluasiContract.Presenter {

    override fun loadSubList() {
        mView.setLoadingIndicator(true)
        val list = mPretesRepo.getSubList(mSelectedBukuEvaluasi)

        mView.setLoadingIndicator(false)
        if (list.isEmpty()) mView.showEmptySubListMessage()
        else mView.showSubList(list)
    }

    override fun goToDetailPage(subBukuEvaluasi: String) {
        mView.openDetailPage(subBukuEvaluasi)
    }

    override fun start() = loadSubList()
}