package illiyin.mhandharbeni.bicapps.feature.pengumuman

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.model.Pengumuman
import kotlinx.android.synthetic.main.item_notification.view.*

class PengumumanRvAdapter(
        private val mClickListener: OnItemClickListener
) : RecyclerView.Adapter<PengumumanRvAdapter.ItemViewHolder>() {

    var mPengumumanList: List<Pengumuman> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_notification, parent, false)
        view.image_icon.setImageResource(R.drawable.ic_pengumuman)
        return ItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.fetch(mPengumumanList[position])
    }

    override fun getItemCount(): Int = mPengumumanList.size

    inner class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun fetch(pengumuman: Pengumuman) {
            itemView.text_title.text = pengumuman.title
            itemView.text_subtitle.text = pengumuman.content
            itemView.setOnClickListener { mClickListener.onItemClick(pengumuman) }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(pengumuman: Pengumuman)
    }
}
