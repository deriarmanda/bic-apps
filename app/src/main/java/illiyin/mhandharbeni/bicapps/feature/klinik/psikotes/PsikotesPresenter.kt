package illiyin.mhandharbeni.bicapps.feature.klinik.psikotes

import illiyin.mhandharbeni.bicapps.data.model.Question
import illiyin.mhandharbeni.bicapps.data.repository.PsikotesRepository
import illiyin.mhandharbeni.bicapps.util.TestType

class PsikotesPresenter(
        private val mView: PsikotesContract.View,
        private val mPsikotesRepo: PsikotesRepository
) : PsikotesContract.Presenter {

    private lateinit var mQuestionList: ArrayList<Question>
    private val test: TestType = TestType.PSIKOTES

    override fun checkPageStatus() {
        mView.setLoadingIndicator(true)
        val allowed = mPsikotesRepo.getPageStatus()

        if (allowed) mView.onPageAllowed()
        else mView.onPageRestricted()
        mView.setLoadingIndicator(false)
    }

    override fun loadListQuestion() {
        mView.setLoadingIndicator(true)
        mQuestionList = mPsikotesRepo.getListQuestion()

        test.duration = 30000

        mView.onLoadListQuestionSucceed(test.duration)
        mView.setLoadingIndicator(false)
    }

    override fun goToRegistrationPage() {
        mView.openRegistrationPage()
    }

    override fun goToTestPage() {
        mView.openTestPage(test, mQuestionList)
    }

    override fun start() = checkPageStatus()
}