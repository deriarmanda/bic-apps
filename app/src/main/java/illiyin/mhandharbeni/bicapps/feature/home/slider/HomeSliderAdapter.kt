package illiyin.mhandharbeni.bicapps.feature.home.slider

import android.support.annotation.DrawableRes
import ss.com.bannerslider.adapters.SliderAdapter
import ss.com.bannerslider.viewholder.ImageSlideViewHolder

class HomeSliderAdapter(
        private val urls: List<String>,
        @DrawableRes private val placeholderRes: Int,
        @DrawableRes private val errorRes: Int
) : SliderAdapter() {

    override fun getItemCount(): Int = urls.size

    override fun onBindImageSlide(position: Int, viewHolder: ImageSlideViewHolder) {
        viewHolder.bindImageSlide(
                urls[position],
                placeholderRes,
                errorRes
        )
    }
}
