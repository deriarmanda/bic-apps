package illiyin.mhandharbeni.bicapps.config

import android.content.Context
import android.content.SharedPreferences

class ApplicationPreference(private val mContext: Context) {

    companion object {
        private const val BIC_APPS_PREFERENCE = "bic-app_preference"
        private const val PREF_USER_TOKEN = "user_token"
    }

    val mSharedPref: SharedPreferences
        get() = mContext.getSharedPreferences(BIC_APPS_PREFERENCE, Context.MODE_PRIVATE)

    fun isUserHasToken() = mSharedPref.contains(PREF_USER_TOKEN)

    fun getUserToken() = mSharedPref.getString(PREF_USER_TOKEN, "")

    fun saveUserToken(token: String) {
        val editor = mSharedPref.edit()
        editor.putString(PREF_USER_TOKEN, token)
        editor.apply()
    }

    fun clearUserToken() {
        val editor = mSharedPref.edit()
        editor.remove(PREF_USER_TOKEN)
        editor.apply()
    }
}