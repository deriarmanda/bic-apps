package illiyin.mhandharbeni.bicapps.config

object ApplicationConfig {
    const val BASE_URL = "${API.STAGING}/${API.PREFIX}/"

    private object API {
        const val STAGING = "http://45.32.105.117:5081"
        const val PREFIX = "api"
    }
}