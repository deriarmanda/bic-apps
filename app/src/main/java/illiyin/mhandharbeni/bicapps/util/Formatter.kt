package illiyin.mhandharbeni.bicapps.util

import java.text.SimpleDateFormat
import java.util.*

object DateFormatter {

    fun format(date: Date): String {
        return SimpleDateFormat("d MMMM yyyy", Locale.GERMANY).format(date)
    }
}

object TimeFormatter {

    fun millisToMinutes(millis: Long) = millis / 60000
}
