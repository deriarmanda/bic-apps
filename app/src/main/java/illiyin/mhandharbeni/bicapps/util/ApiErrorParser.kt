package illiyin.mhandharbeni.bicapps.util

import org.json.JSONObject

object ApiErrorParser {

    fun getStatusCode(errorBody: String): Int {
        return if (errorBody.isBlank()) -1
        else {
            val json = JSONObject(errorBody).getJSONObject("data")
            json.getInt("status")
        }
    }

    fun getMessage(errorBody: String): String {
        return if (errorBody.isBlank()) "Unknown Error Occured"
        else {
            val json = JSONObject(errorBody).getJSONObject("data")
            json.getString("message")
        }
    }

    fun getMessageForAuth(errorBody: String): String {
        return if (errorBody.isBlank()) "Unknown Error Occured"
        else {
            val json = JSONObject(errorBody).getJSONObject("data")
            json.getString("messsage")
        }
    }
}