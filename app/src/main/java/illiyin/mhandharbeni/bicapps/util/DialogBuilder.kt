package illiyin.mhandharbeni.bicapps.util

import android.app.DatePickerDialog
import android.content.Context
import android.support.annotation.StringRes
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import illiyin.mhandharbeni.bicapps.R
import java.util.*

@Deprecated("Use MessageHelper instead.")
class DialogBuilder(private val mContext: Context) {

    private var dialog: MaterialDialog? = null

    fun show() = dialog?.show()

    fun dismiss() = dialog?.dismiss()

    fun buildProgressDialog(@StringRes contentRes: Int): DialogBuilder {
        val builder = MaterialDialog.Builder(mContext)
                .content(contentRes)
                .progress(true, 99)
                .cancelable(false)
                .widgetColorRes(R.color.primary)
        dialog = builder.build()

        return this
    }

    fun buildMaterialProgressDialog(@StringRes contentRes: Int): MaterialDialog {
        return MaterialDialog.Builder(mContext)
                .content(contentRes)
                .progress(true, 99)
                .cancelable(false)
                .widgetColorRes(R.color.primary)
                .build()
    }

    fun buildInformationDialog(
            @StringRes contentRes: Int,
            title: String? = null,
            cancelable: Boolean = false
    ): DialogBuilder {
        val builder = MaterialDialog.Builder(mContext)
                .content(contentRes)
                .cancelable(cancelable)
                .widgetColorRes(R.color.primary)
        if (title != null) builder.title(title)
        dialog = builder.build()

        return this
    }

    fun buildInformationDialog(
            content: String,
            title: String? = null,
            cancelable: Boolean = false
    ): DialogBuilder {
        val builder = MaterialDialog.Builder(mContext)
                .content(content)
                .cancelable(cancelable)
                .widgetColorRes(R.color.primary)
        if (title != null) builder.title(title)
        dialog = builder.build()

        return this
    }

    fun buildDatePickerDialog(
            listener: DatePickerDialog.OnDateSetListener
    ): DatePickerDialog {
        val c = Calendar.getInstance()
        return DatePickerDialog(
                mContext,
                listener,
                c.get(Calendar.YEAR),
                c.get(Calendar.MONTH),
                c.get(Calendar.DAY_OF_MONTH)
        )
    }

    fun showInformationToast(
            @StringRes msgRes: Int,
            duration: Int = Toast.LENGTH_SHORT
    ) = Toast.makeText(mContext, msgRes, duration).show()

    fun showInformationToast(
            message: String,
            duration: Int = Toast.LENGTH_SHORT
    ) = Toast.makeText(mContext, message, duration).show()

    fun showUnderDevelopmentDialog() {
        MaterialDialog.Builder(mContext)
                .title(R.string.general_title_under_development)
                .content(R.string.general_msg_under_development)
                .positiveText(R.string.general_action_tutup)
                .positiveColorRes(R.color.primary)
                .onPositive { _, _ -> dismiss() }
                .build().show()
    }
}