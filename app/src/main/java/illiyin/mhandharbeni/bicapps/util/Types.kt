package illiyin.mhandharbeni.bicapps.util

import android.support.annotation.DrawableRes
import android.support.annotation.StringRes
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.feature.grade_test.result.ResultGradeTestActivity
import illiyin.mhandharbeni.bicapps.feature.klinik.KlinikActivity
import illiyin.mhandharbeni.bicapps.feature.klinik.psikotes.PsikotesActivity
import illiyin.mhandharbeni.bicapps.feature.klinik.psikotes.result.ResultPsikotesActivity
import illiyin.mhandharbeni.bicapps.feature.klinik.tesiq.TesIQActivity
import illiyin.mhandharbeni.bicapps.feature.klinik.tesiq.result.ResultTesIQActivity
import illiyin.mhandharbeni.bicapps.feature.mybic.ProfilActivity
import illiyin.mhandharbeni.bicapps.feature.mybic.bukuevaluasi.BukuEvaluasiActivity
import illiyin.mhandharbeni.bicapps.feature.mybic.bukupretes.PretesActivity
import illiyin.mhandharbeni.bicapps.feature.mybic.evaluasi.EvaluasiActivity
import illiyin.mhandharbeni.bicapps.feature.mybic.evaluasi.result.ResultEvaluasiActivity
import illiyin.mhandharbeni.bicapps.feature.mybic.grafik.GrafikActivity
import illiyin.mhandharbeni.bicapps.feature.mybic.paymentrecord.PaymentRecordActivity
import illiyin.mhandharbeni.bicapps.feature.mybic.rangkumanmateri.MateriActivity
import illiyin.mhandharbeni.bicapps.feature.mybic.tugasharian.TugasHarianActivity
import illiyin.mhandharbeni.bicapps.feature.pdf.PdfActivity
import illiyin.mhandharbeni.bicapps.feature.signin.SignInActivity
import illiyin.mhandharbeni.bicapps.feature.tryout.result.ResultTryOutActivity

object RequestCode {
    const val SIGN_IN = 1
    const val SIGN_UP = 2
    const val PICK_PHOTO = 30
    const val PICK_PERSONAL_PHOTO = 31
    const val PICK_FATHERS_PHOTO = 32
    const val PICK_MOTHERS_PHOTO = 33
}

object DummyPdf {
    private val MAP_DUMMY_PDF = mapOf(
            Pair(ProfilPage.KALENDER_AKADEMIK, "dummy_kalender_akademik.pdf"),
            Pair(ProfilPage.BUKU_PRETES, "dummy_buku_pretes.pdf"),
            Pair(ProfilPage.BUKU_EVALUASI, "dummy_buku_evaluasi.pdf"),
            Pair(ProfilPage.TUGAS_HARIAN, "dummy_tugas_harian.pdf")
    )

    fun getFile(page: ProfilPage) = MAP_DUMMY_PDF[page]
}

enum class PdfSourceMode {
    FROM_URL,
    FROM_LOCAL_FILE
}

enum class TestType(
        var title: String,
        val resultClazz: Class<*>,
        var duration: Long = 0,
        var trueAnsCount: Int = 0,
        var falseAnsCount: Int = 0,
        var emptyAnsCount: Int = 0,
        var score: Int = 0
) {
    TRY_OUT("Try Out", ResultTryOutActivity::class.java),
    TES_GRADE("Tes Grade", ResultGradeTestActivity::class.java),
    EVALUASI("Evaluasi", ResultEvaluasiActivity::class.java),
    PSIKOTES("Psikotes", ResultPsikotesActivity::class.java),
    TES_IQ("Tes IQ", ResultTesIQActivity::class.java)
}

enum class ProfilPage(
        @StringRes val titleRes: Int,
        @DrawableRes val logoRes: Int,
        val clazz: Class<*>
) {
    PEMBAYARAN(
            titleRes = R.string.title_pembayaran,
            logoRes = R.drawable.ic_program,
            clazz = PaymentRecordActivity::class.java),
    KALENDER_AKADEMIK(
            titleRes = R.string.title_kalender_akademik,
            logoRes = R.drawable.ic_program,
            clazz = PdfActivity::class.java),
    RANGKUMAN_MATERI(
            titleRes = R.string.title_rangkuman_materi,
            logoRes = R.drawable.ic_program,
            clazz = MateriActivity::class.java),
    BUKU_PRETES(
            titleRes = R.string.title_buku_pretes,
            logoRes = R.drawable.ic_program,
            clazz = PretesActivity::class.java),
    BUKU_EVALUASI(
            titleRes = R.string.title_buku_evaluasi,
            logoRes = R.drawable.ic_program,
            clazz = BukuEvaluasiActivity::class.java),
    TUGAS_HARIAN(
            titleRes = R.string.title_tugas_harian,
            logoRes = R.drawable.ic_program,
            clazz = TugasHarianActivity::class.java),
    EVALUASI(
            titleRes = R.string.title_evaluasi,
            logoRes = R.drawable.ic_program,
            clazz = EvaluasiActivity::class.java),
    TRY_OUT_MINGGUAN(
            titleRes = R.string.title_tryout_mingguan,
            logoRes = R.drawable.ic_program,
            clazz = ProfilActivity::class.java),
    PERKEMBANGAN_BELAJAR(
            titleRes = R.string.title_perkembangan_belajar,
            logoRes = R.drawable.ic_program,
            clazz = GrafikActivity::class.java)
}

enum class KlinikPage(
        @StringRes val titleRes: Int,
        @DrawableRes val logoRes: Int,
        val clazz: Class<*>
) {
    KONSULTASI_MATERI(
            titleRes = R.string.title_konsultasi_materi,
            logoRes = R.drawable.ic_program,
            clazz = SignInActivity::class.java),
    KONSULTASI_SOAL(
            titleRes = R.string.title_konsultasi_soal,
            logoRes = R.drawable.ic_program,
            clazz = KlinikActivity::class.java),
    KONSULTASI_JURUSAN(
            titleRes = R.string.title_konsultasi_jurusan,
            logoRes = R.drawable.ic_program,
            clazz = KlinikActivity::class.java),
    KONSULTASI_UNIVERSITAS(
            titleRes = R.string.title_konsultasi_univ,
            logoRes = R.drawable.ic_program,
            clazz = KlinikActivity::class.java),
    PSIKOTES(
            titleRes = R.string.title_psikotes,
            logoRes = R.drawable.ic_program,
            clazz = PsikotesActivity::class.java),
    PREDIKSI_TPA(
            titleRes = R.string.title_prediksi_tpa,
            logoRes = R.drawable.ic_program,
            clazz = KlinikActivity::class.java),
    PREDIKSI_TOEFL(
            titleRes = R.string.title_prediksi_toefl,
            logoRes = R.drawable.ic_program,
            clazz = KlinikActivity::class.java),
    TES_IQ(
            titleRes = R.string.title_tes_iq,
            logoRes = R.drawable.ic_program,
            clazz = TesIQActivity::class.java),
    TRY_OUT_UN(
            titleRes = R.string.title_tryout_un,
            logoRes = R.drawable.ic_program,
            clazz = KlinikActivity::class.java)
}