package illiyin.mhandharbeni.bicapps.util

import android.app.Activity
import android.content.Context
import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.view.View
import android.widget.Toast
import illiyin.mhandharbeni.bicapps.R

// Message with Toast
fun Context.showToast(message: String, duration: Int = Toast.LENGTH_SHORT) {
    val toast = Toast.makeText(this, message, duration)
    toast.show()
}

fun Context.showToast(@StringRes messageRes: Int, duration: Int = Toast.LENGTH_SHORT) {
    val toast = Toast.makeText(this, messageRes, duration)
    toast.show()
}

// Message with Snackbar
fun Activity.showUnderDevelopmentSnackbar() {
    Snackbar.make(
            findViewById(android.R.id.content),
            R.string.general_msg_under_development,
            Snackbar.LENGTH_INDEFINITE
    ).setAction(R.string.general_action_kembali) {
        finish()
    }.show()
}

fun View.showSnackbar(message: String, duration: Int = Snackbar.LENGTH_SHORT) {
    val snackbar = Snackbar.make(this, message, duration)
    snackbar.show()
}

fun View.showSnackbar(@StringRes messageRes: Int, duration: Int = Snackbar.LENGTH_SHORT) {
    val snackbar = Snackbar.make(this, messageRes, duration)
    snackbar.show()
}

fun View.showSnackbarWithAction(
        message: String,
        actionText: String,
        duration: Int = Snackbar.LENGTH_SHORT,
        action: ((View) -> Unit)
) {
    val snackbar = Snackbar.make(this, message, duration)
    snackbar.setAction(actionText, action)
    snackbar.show()
}

fun View.showSnackbarWithAction(
        messageRes: Int,
        actionTextRes: Int,
        duration: Int = Snackbar.LENGTH_SHORT,
        action: ((View) -> Unit)
) {
    val snackbar = Snackbar.make(this, messageRes, duration)
    snackbar.setAction(actionTextRes, action)
    snackbar.show()
}

fun View.buildErrorSnackbar(
        message: String,
        actionText: String,
        action: ((View) -> Unit)
): Snackbar {
    val snackbar = Snackbar.make(this, message, Snackbar.LENGTH_INDEFINITE)
    snackbar.setAction(actionText, action)
    return snackbar
}

fun View.buildErrorSnackbar(
        messageRes: Int,
        actionTextRes: Int,
        action: ((View) -> Unit)
): Snackbar {
    val snackbar = Snackbar.make(this, messageRes, Snackbar.LENGTH_INDEFINITE)
    snackbar.setAction(actionTextRes, action)
    return snackbar
}