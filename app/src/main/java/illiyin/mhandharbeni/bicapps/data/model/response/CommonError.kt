package illiyin.mhandharbeni.bicapps.data.model.response

data class AuthError(
        val message: String
)

data class CommonError(
        val status: Int,
        val message: String
)