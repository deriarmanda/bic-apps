package illiyin.mhandharbeni.bicapps.data.model;

public class PaymentRecord {

    private String title;
    private String price;
    private boolean done;

    public PaymentRecord() {
    }

    public PaymentRecord(String title, String price, boolean done) {
        this.title = title;
        this.price = price;
        this.done = done;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }
}
