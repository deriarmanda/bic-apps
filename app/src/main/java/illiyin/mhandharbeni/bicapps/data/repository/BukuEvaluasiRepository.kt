package illiyin.mhandharbeni.bicapps.data.repository

import android.content.Context
import illiyin.mhandharbeni.bicapps.R

class BukuEvaluasiRepository private constructor(private val mContext: Context) {

    companion object {
        fun getInstance(context: Context) = BukuEvaluasiRepository(context)
    }

    fun getList(): List<String> {
        //TODO: get list of materi from api
        return mContext.resources.getStringArray(R.array.list_task_my_bic).toList()
    }

    fun getSubList(evaluasi: String): List<String> {
        //TODO: get sublist of selected materi from api
        println(evaluasi.length)
        return listOf(
                "Buku Evaluasi 1",
                "Buku Evaluasi 2",
                "Buku Evaluasi 3",
                "Buku Evaluasi 4",
                "Buku Evaluasi 5",
                "Buku Evaluasi 6",
                "Buku Evaluasi 7",
                "Buku Evaluasi 8",
                "Buku Evaluasi 9",
                "Buku Evaluasi 10"
        )
    }
}