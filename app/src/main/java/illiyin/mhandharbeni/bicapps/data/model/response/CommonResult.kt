package illiyin.mhandharbeni.bicapps.data.model.response

data class CommonResult<T>(
        val data: T? = null
)