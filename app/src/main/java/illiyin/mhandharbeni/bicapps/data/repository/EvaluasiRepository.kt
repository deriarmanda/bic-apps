package illiyin.mhandharbeni.bicapps.data.repository

import android.content.Context
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.data.model.Question

class EvaluasiRepository private constructor(private val mContext: Context) {

    companion object {
        fun getInstance(context: Context) = EvaluasiRepository(context)
    }

    fun getListEvaluasi(): List<String> {
        //TODO: get list of evaluasi from api
        return mContext.resources.getStringArray(R.array.list_task_my_bic).toList()
    }

    fun getSubListEvaluasi(evaluasi: String): List<String> {
        //TODO: get sublist of selected evaluasi from api
        return listOf(
                "Evaluasi $evaluasi 1",
                "Evaluasi $evaluasi 2",
                "Evaluasi $evaluasi 3",
                "Evaluasi $evaluasi 4",
                "Evaluasi $evaluasi 5",
                "Evaluasi $evaluasi 6",
                "Evaluasi $evaluasi 7",
                "Evaluasi $evaluasi 8",
                "Evaluasi $evaluasi 9",
                "Evaluasi $evaluasi 10"
        )
    }

    fun getListNilaiEvaluasi(evaluasi: String): Map<String, Int> {
        //TODO: get map of nilai evaluasi from api
        return mapOf(
                Pair("$evaluasi 1", 60),
                Pair("$evaluasi 2", 85),
                Pair("$evaluasi 3", 75),
                Pair("$evaluasi 4", 90),
                Pair("$evaluasi 5", 55),
                Pair("$evaluasi 6", 100)
        )
    }

    fun getListQuestion(evaluasi: String): ArrayList<Question> {
        //TODO: get list question of selected evaluasi from api
        return arrayListOf(
                Question("$evaluasi ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "benar"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "benar"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "salah"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "benar"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "salah"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "salah"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "salah"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "benar"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "salah"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "benar"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "salah"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "benar"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "benar"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "salah"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "benar")
        )
    }

    fun getResultDescription(): String {
        //TODO: get result desc from api
        return "Lorem ipsum dolor sit amet, te has omnis delectus, delectus deseruisse in sea.\n" +
                "        Ad vero ipsum salutandi mea. Vix et aeque graeci putent.\n" +
                "        Nec eros sale omnes ei, id persecuti mediocritatem duo. Ad nec sint vulputate."
    }
}