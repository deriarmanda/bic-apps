package illiyin.mhandharbeni.bicapps.data.repository

class HomeRepository private constructor() {

    companion object {
        fun getInstance() = HomeRepository()
    }

    fun getImageSliderUrls(): List<String> {
        return listOf(
                "https://assets.materialup.com/uploads/dcc07ea4-845a-463b-b5f0-4696574da5ed/preview.jpg",
                "https://assets.materialup.com/uploads/20ded50d-cc85-4e72-9ce3-452671cf7a6d/preview.jpg",
                "https://assets.materialup.com/uploads/76d63bbc-54a1-450a-a462-d90056be881b/preview.png")
    }

}