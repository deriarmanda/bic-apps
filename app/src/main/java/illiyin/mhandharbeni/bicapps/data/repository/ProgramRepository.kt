package illiyin.mhandharbeni.bicapps.data.repository

import android.content.Context
import android.util.Log
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.base.BaseCallback
import illiyin.mhandharbeni.bicapps.data.model.Program
import illiyin.mhandharbeni.bicapps.data.model.response.CommonResult
import illiyin.mhandharbeni.bicapps.data.network.ClientBuilder
import illiyin.mhandharbeni.bicapps.data.network.service.ProgramService
import illiyin.mhandharbeni.bicapps.util.ApiErrorParser
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class ProgramRepository private constructor(context: Context) {

    private val resources = context.resources
    private val programService = ClientBuilder.build(ProgramService::class.java)
    private val programList = arrayListOf<Program>()

    companion object {
        private const val TAG = "ProgramRepository"
        private var sInstance: ProgramRepository? = null

        fun getInstance(context: Context): ProgramRepository {
            if (sInstance == null) sInstance = ProgramRepository(context)
            return sInstance!!
        }
    }

    fun getListProgram(isRefreshed: Boolean, callback: BaseCallback<List<Program>>) {
        if (!isRefreshed && programList.isNotEmpty()) callback.onSuccess(programList)
        else {
            programService.getListProgram().enqueue(
                    object : Callback<CommonResult<List<Program>>> {
                        override fun onFailure(
                                call: Call<CommonResult<List<Program>>>,
                                t: Throwable
                        ) {
                            Log.d(TAG, t.toString())
                            if (t is IOException) callback.onError(resources.getString(R.string.general_error_network_connection))
                            else callback.onError(resources.getString(R.string.general_error_unknown))
                        }

                        override fun onResponse(
                                call: Call<CommonResult<List<Program>>>,
                                response: Response<CommonResult<List<Program>>>
                        ) {
                            if (response.isSuccessful) {
                                val data = response.body()?.data
                                if (data != null) {
                                    programList.clear()
                                    for (program in data) programList.add(program)
                                    callback.onSuccess(programList)
                                } else {
                                    Log.d(TAG, "onResponse getListProgram -> statusCode: ${response.code()}, body: ${response.raw()}")
                                    callback.onError(resources.getString(R.string.general_error_unknown))
                                }
                            } else {
                                val errorBody = response.errorBody()?.string()
                                callback.onError(ApiErrorParser.getMessage(errorBody ?: ""))
                                Log.d(TAG, "onResponse getListProgram -> statusCode: $errorBody")
                            }
                        }
                    }
            )
        }
    }
}