package illiyin.mhandharbeni.bicapps.data.repository

import android.content.Context
import illiyin.mhandharbeni.bicapps.R

class PretesRepository private constructor(private val mContext: Context) {

    companion object {
        fun getInstance(context: Context) = PretesRepository(context)
    }

    fun getListPretes(): List<String> {
        //TODO: get list of materi from api
        return mContext.resources.getStringArray(R.array.list_task_my_bic).toList()
    }

    fun getSubListPretes(pretes: String): List<String> {
        //TODO: get sublist of selected materi from api
        println(pretes)
        return listOf(
                "Buku Pretes 1",
                "Buku Pretes 2",
                "Buku Pretes 3",
                "Buku Pretes 4",
                "Buku Pretes 5",
                "Buku Pretes 6",
                "Buku Pretes 7",
                "Buku Pretes 8",
                "Buku Pretes 9",
                "Buku Pretes 10"
        )
    }
}