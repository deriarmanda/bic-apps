package illiyin.mhandharbeni.bicapps.data.repository

import android.content.Context
import android.util.Log
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.base.BaseCallback
import illiyin.mhandharbeni.bicapps.data.model.Pengumuman
import illiyin.mhandharbeni.bicapps.data.model.response.CommonResult
import illiyin.mhandharbeni.bicapps.data.network.ClientBuilder
import illiyin.mhandharbeni.bicapps.data.network.service.PengumumanService
import illiyin.mhandharbeni.bicapps.util.ApiErrorParser
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class PengumumanRepository private constructor(context: Context) {

    private val resources = context.resources
    private val pengumumanService = ClientBuilder.build(PengumumanService::class.java)
    private val pengumumanList = arrayListOf<Pengumuman>()

    companion object {
        private const val TAG = "PengumumanRepository"
        private var sInstance: PengumumanRepository? = null

        fun getInstance(context: Context): PengumumanRepository {
            if (sInstance == null) sInstance = PengumumanRepository(context)
            return sInstance!!
        }
    }

    fun getListPengumuman(isRefreshed: Boolean, callback: BaseCallback<List<Pengumuman>>) {
        if (!isRefreshed && pengumumanList.isNotEmpty()) callback.onSuccess(pengumumanList)
        else {
            pengumumanService.getListPengumuman().enqueue(
                    object : Callback<CommonResult<List<Pengumuman>>> {
                        override fun onFailure(
                                call: Call<CommonResult<List<Pengumuman>>>,
                                t: Throwable
                        ) {
                            Log.d(TAG, t.toString())
                            if (t is IOException) callback.onError(resources.getString(R.string.general_error_network_connection))
                            else callback.onError(resources.getString(R.string.general_error_unknown))
                        }

                        override fun onResponse(
                                call: Call<CommonResult<List<Pengumuman>>>,
                                response: Response<CommonResult<List<Pengumuman>>>
                        ) {
                            if (response.isSuccessful) {
                                val data = response.body()?.data
                                if (data != null) {
                                    pengumumanList.clear()
                                    for (pengumuman in data) pengumumanList.add(pengumuman)
                                    callback.onSuccess(pengumumanList)
                                } else {
                                    Log.d(TAG, "onResponse getListPengumuman -> statusCode: ${response.code()}, body: ${response.raw()}")
                                    callback.onError(resources.getString(R.string.general_error_unknown))
                                }
                            } else {
                                val errorBody = response.errorBody()?.string()
                                callback.onError(ApiErrorParser.getMessage(errorBody ?: ""))
                                Log.d(TAG, "onResponse getListPengumuman -> statusCode: $errorBody, body: $errorBody")
                            }
                        }
                    }
            )
        }
    }
}