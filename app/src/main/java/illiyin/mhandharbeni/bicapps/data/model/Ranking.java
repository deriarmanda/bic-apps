package illiyin.mhandharbeni.bicapps.data.model;

public class Ranking {

    private String nama;
    private int score;

    public Ranking() {
    }

    public Ranking(String nama, int score) {
        this.nama = nama;
        this.score = score;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
