package illiyin.mhandharbeni.bicapps.data.repository

import android.content.Context
import illiyin.mhandharbeni.bicapps.R

class TugasHarianRepository private constructor(private val mContext: Context) {

    companion object {
        fun getInstance(context: Context) = TugasHarianRepository(context)
    }

    fun getList(): List<String> {
        //TODO: get list of materi from api
        return mContext.resources.getStringArray(R.array.list_task_my_bic).toList()
    }

    fun getSubList(tugas: String): List<String> {
        //TODO: get sublist of selected materi from api
        println(tugas)
        return listOf(
                "Tugas Harian 1",
                "Tugas Harian 2",
                "Tugas Harian 3",
                "Tugas Harian 4",
                "Tugas Harian 5",
                "Tugas Harian 6",
                "Tugas Harian 7",
                "Tugas Harian 8",
                "Tugas Harian 9",
                "Tugas Harian 10"
        )
    }
}