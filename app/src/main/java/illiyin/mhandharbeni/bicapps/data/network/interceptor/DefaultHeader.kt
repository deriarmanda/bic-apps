package illiyin.mhandharbeni.bicapps.data.network.interceptor

import okhttp3.Interceptor
import okhttp3.Response

class DefaultHeader : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        return chain.proceed(
                chain.request().newBuilder()
                        .header("Content-Type", "application/json")
                        .build()
        )
    }
}