package illiyin.mhandharbeni.bicapps.data.model

data class RegistrationForm(
        val fotoProfilPath: String = "-",
        val nama: String,
        val tempatLahir: String,
        val tanggalLahir: String,
        val asalSekolah: String,
        val alamat: String,
        val noHp: String,
        val email: String,
        val instagram: String,
        val facebook: String,
        val line: String,

        val namaAyah: String,
        val pekerjaanAyah: String = "-",
        val noHpAyah: String = "-",
        val fotoAyahPath: String = "-",

        val namaIbu: String,
        val pekerjaanIbu: String = "-",
        val noHpIbu: String = "-",
        val fotoIbuPath: String = "-",

        val program: String,
        val jurusan1: String,
        val jurusan2: String = "-",
        val jurusan3: String = "-",
        val univ1: String,
        val univ2: String = "-",
        val univ3: String = "-"
)