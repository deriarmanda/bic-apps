package illiyin.mhandharbeni.bicapps.data.network

import android.util.Log
import illiyin.mhandharbeni.bicapps.BuildConfig
import illiyin.mhandharbeni.bicapps.config.ApplicationConfig
import illiyin.mhandharbeni.bicapps.data.network.interceptor.Authorization
import illiyin.mhandharbeni.bicapps.data.network.interceptor.DefaultHeader
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ClientBuilder {

    fun <T> build(clazz: Class<T>): T {
        return Retrofit.Builder()
                .baseUrl(ApplicationConfig.BASE_URL)
                .client(getHttpClient(clazz.simpleName))
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(clazz)
    }

    private fun getHttpClient(tag: String): OkHttpClient {
        val logger = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
            Log.d(tag, it)
        })
        logger.level =
                if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
                else HttpLoggingInterceptor.Level.BASIC

        val builder = OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(DefaultHeader())
                .addInterceptor(Authorization())
                .addInterceptor(logger)
        return builder.build()
    }
}