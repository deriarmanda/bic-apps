package illiyin.mhandharbeni.bicapps.data.network.interceptor

import illiyin.mhandharbeni.bicapps.data.repository.UserRepository
import okhttp3.Interceptor
import okhttp3.Response

class Authorization : Interceptor {

    private val userRepo = UserRepository.getInstance()

    companion object {
        private const val AUTH_HEADER = "Authorization"
        private const val AUTH_TYPE = "Bearer"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val builder = chain.request().newBuilder()
        if (userRepo != null && userRepo.isUserAlreadyLogin()) {
            builder.addHeader(AUTH_HEADER, "$AUTH_TYPE ${userRepo.getUserToken()}")
        }
        return chain.proceed(builder.build())
    }

}