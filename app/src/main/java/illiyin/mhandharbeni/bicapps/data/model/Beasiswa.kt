package illiyin.mhandharbeni.bicapps.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Beasiswa(
        @SerializedName("id") val id: Int,
        @SerializedName("judul") val title: String,
        @SerializedName("subtitle") val subtitle: String,
        @SerializedName("deskripsi") val content: String,
        @Expose val imageUrl: String
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "")

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(title)
        parcel.writeString(subtitle)
        parcel.writeString(content)
        parcel.writeString(imageUrl)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Beasiswa> {
        override fun createFromParcel(parcel: Parcel): Beasiswa {
            return Beasiswa(parcel)
        }

        override fun newArray(size: Int): Array<Beasiswa?> {
            return arrayOfNulls(size)
        }
    }
}