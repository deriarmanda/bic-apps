package illiyin.mhandharbeni.bicapps.data.network.service

import illiyin.mhandharbeni.bicapps.data.model.Program
import illiyin.mhandharbeni.bicapps.data.model.response.CommonResult
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ProgramService {

    @GET("program")
    fun getListProgram(): Call<CommonResult<List<Program>>>

    @GET("program/{program_id}")
    fun getDetailProgram(@Path("{program_id}") id: Int): Call<CommonResult<Program>>
}