package illiyin.mhandharbeni.bicapps.data.model;

import java.util.ArrayList;

public class Kampus {

    private String nama;
    private ArrayList<Ranking> listRanking;

    public Kampus() {
    }

    public Kampus(String nama, ArrayList<Ranking> listRanking) {
        this.nama = nama;
        this.listRanking = listRanking;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public ArrayList<Ranking> getListRanking() {
        return listRanking;
    }

    public void setListRanking(ArrayList<Ranking> listRanking) {
        this.listRanking = listRanking;
    }

    @Override
    public String toString() {
        return nama;
    }
}
