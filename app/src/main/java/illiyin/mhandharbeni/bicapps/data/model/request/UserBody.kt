package illiyin.mhandharbeni.bicapps.data.model.request

import com.google.gson.annotations.SerializedName

data class LoginUser(
        @SerializedName("username") val username: String,
        @SerializedName("password") val password: String
)

data class RegisterUser(
        @SerializedName("name") val name: String,
        @SerializedName("username") val username: String,
        @SerializedName("email") val email: String,
        @SerializedName("password") val password: String
)