package illiyin.mhandharbeni.bicapps.data.repository

import android.content.Context
import android.util.Log
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.base.BaseCallback
import illiyin.mhandharbeni.bicapps.data.model.Beasiswa
import illiyin.mhandharbeni.bicapps.data.model.response.CommonResult
import illiyin.mhandharbeni.bicapps.data.network.ClientBuilder
import illiyin.mhandharbeni.bicapps.data.network.service.BeasiswaService
import illiyin.mhandharbeni.bicapps.util.ApiErrorParser
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class InfoBeasiswaRepository private constructor(context: Context) {

    private val resources = context.resources
    private val beasiswaService = ClientBuilder.build(BeasiswaService::class.java)
    private val beasiswaList = arrayListOf<Beasiswa>()

    companion object {
        private const val TAG = "BeasiswanRepository"
        private var sInstance: InfoBeasiswaRepository? = null

        fun getInstance(context: Context): InfoBeasiswaRepository {
            if (sInstance == null) sInstance = InfoBeasiswaRepository(context)
            return sInstance!!
        }
    }

    fun getListBeasiswa(isRefreshed: Boolean, callback: BaseCallback<List<Beasiswa>>) {
        if (!isRefreshed && beasiswaList.isNotEmpty()) callback.onSuccess(beasiswaList)
        else {
            beasiswaService.getListBeasiswa().enqueue(
                    object : Callback<CommonResult<List<Beasiswa>>> {
                        override fun onFailure(
                                call: Call<CommonResult<List<Beasiswa>>>,
                                t: Throwable
                        ) {
                            Log.d(TAG, t.toString())
                            if (t is IOException) callback.onError(resources.getString(R.string.general_error_network_connection))
                            else callback.onError(resources.getString(R.string.general_error_unknown))
                        }

                        override fun onResponse(
                                call: Call<CommonResult<List<Beasiswa>>>,
                                response: Response<CommonResult<List<Beasiswa>>>
                        ) {
                            if (response.isSuccessful) {
                                val data = response.body()?.data
                                if (data != null) {
                                    beasiswaList.clear()
                                    for (beasiswa in data) beasiswaList.add(beasiswa)
                                    callback.onSuccess(beasiswaList)
                                } else {
                                    Log.d(TAG, "onResponse getListBeasiswa -> statusCode: ${response.code()}, body: ${response.raw()}")
                                    callback.onError(resources.getString(R.string.general_error_unknown))
                                }
                            } else {
                                val errorBody = response.errorBody()?.string()
                                callback.onError(ApiErrorParser.getMessage(errorBody ?: ""))
                                Log.d(TAG, "onResponse getListBeasiswa -> statusCode: $errorBody, body: $errorBody")
                            }
                        }
                    }
            )
        }
    }
}