package illiyin.mhandharbeni.bicapps.data.repository

import illiyin.mhandharbeni.bicapps.data.model.Kampus
import illiyin.mhandharbeni.bicapps.data.model.Ranking

class GradeJurusanRepository private constructor(){

    companion object {
        fun getInstance() = GradeJurusanRepository()
    }

    fun getListKampus(): List<Kampus> {
        //TODO: get list grade jurusan from api
        return listOf(
                Kampus("Kampus A", arrayListOf(
                        Ranking("Jurusan 1", 90),
                        Ranking("Jurusan 2", 80),
                        Ranking("Jurusan 3", 70),
                        Ranking("Jurusan 4", 60),
                        Ranking("Jurusan 5", 50))),
                Kampus("Kampus B", arrayListOf(
                        Ranking("Jurusan 1", 90),
                        Ranking("Jurusan 2", 80),
                        Ranking("Jurusan 3", 70))),
                Kampus("Kampus C", arrayListOf(
                        Ranking("Jurusan 1", 90),
                        Ranking("Jurusan 2", 80),
                        Ranking("Jurusan 3", 70),
                        Ranking("Jurusan 4", 60)))
        )
    }
}