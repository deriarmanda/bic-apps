package illiyin.mhandharbeni.bicapps.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Program(
        @SerializedName("id") val id: Int,
        @SerializedName("judul") val judul: String,
        @SerializedName("deskripsi") val deskripsi: String,
        @SerializedName("harga") val harga: Long,
        @SerializedName("dp") val dp: Long
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readLong(),
            parcel.readLong())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(judul)
        parcel.writeString(deskripsi)
        parcel.writeLong(harga)
        parcel.writeLong(dp)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Program> {
        override fun createFromParcel(parcel: Parcel): Program {
            return Program(parcel)
        }

        override fun newArray(size: Int): Array<Program?> {
            return arrayOfNulls(size)
        }
    }
}