package illiyin.mhandharbeni.bicapps.data.repository

import android.content.Context
import android.util.Log
import com.google.gson.JsonElement
import illiyin.mhandharbeni.bicapps.R
import illiyin.mhandharbeni.bicapps.base.BaseCallback
import illiyin.mhandharbeni.bicapps.config.ApplicationPreference
import illiyin.mhandharbeni.bicapps.data.model.request.LoginUser
import illiyin.mhandharbeni.bicapps.data.model.request.RegisterUser
import illiyin.mhandharbeni.bicapps.data.network.ClientBuilder
import illiyin.mhandharbeni.bicapps.data.network.service.UserService
import illiyin.mhandharbeni.bicapps.util.ApiErrorParser
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class UserRepository private constructor(context: Context) {

    private val appPreference = ApplicationPreference(context)
    private val resources = context.resources
    private val userService = ClientBuilder.build(UserService::class.java)
    private var userToken: String = ""

    companion object {
        private const val TAG = "UserRepository"
        private var sInstance: UserRepository? = null

        fun getInstance() = sInstance
        fun getInstance(context: Context): UserRepository {
            if (sInstance == null) sInstance = UserRepository(context)
            return sInstance!!
        }
    }

    fun isUserAlreadyLogin() = appPreference.isUserHasToken()

    fun getUserToken(): String {
        if (userToken.isEmpty()) userToken = appPreference.getUserToken()
        return userToken
    }

    fun renewUserToken(token: String) {
        appPreference.saveUserToken(token)
        userToken = token
    }

    fun authenticate(username: String, password: String, callback: BaseCallback<Unit>) {
        val user = LoginUser(username, password)
        userService.login(user).enqueue(
                object : Callback<JsonElement> {
                    override fun onFailure(call: Call<JsonElement>, t: Throwable) {
                        Log.d(TAG, t.toString())
                        if (t is IOException) callback.onError(resources.getString(R.string.general_error_network_connection))
                        else callback.onError(resources.getString(R.string.general_error_unknown))
                    }

                    override fun onResponse(call: Call<JsonElement>, response: Response<JsonElement>) {
                        if (response.isSuccessful) {
                            val data = response.body()?.asJsonObject?.get("data")
                            val token = data?.asJsonObject?.get("token")?.asString

                            if (token != null) {
                                Log.d(TAG, "onResponse authenticate -> token: $token")
                                renewUserToken(token)
                                callback.onSuccess(Unit)
                            } else {
                                Log.d(TAG, "onResponse authenticate -> statusCode: ${response.code()}, body: ${response.raw()}")
                                callback.onError(resources.getString(R.string.general_error_unknown))
                            }
                        } else {
                            val errorBody = response.errorBody()?.string() ?: ""
                            callback.onError(ApiErrorParser.getMessageForAuth(errorBody))
                            Log.d(TAG, "onResponse authenticate -> statusCode: ${response.code()}, errorBody: $errorBody")
                        }
                    }
                }
        )
    }

    fun register(
            fullname: String,
            username: String,
            email: String,
            password: String,
            callback: BaseCallback<String>
    ) {
        val user = RegisterUser(fullname, username, email, password)
        userService.register(user).enqueue(
                object : Callback<JsonElement> {
                    override fun onFailure(call: Call<JsonElement>, t: Throwable) {
                        Log.d(TAG, t.toString())
                        if (t is IOException) callback.onError(resources.getString(R.string.general_error_network_connection))
                        else callback.onError(resources.getString(R.string.general_error_unknown))
                    }

                    override fun onResponse(call: Call<JsonElement>, response: Response<JsonElement>) {
                        if (response.isSuccessful) {
                            val data = response.body()?.asJsonObject?.get("data")
                            val token = data?.asJsonObject?.get("token")?.asString

                            if (token != null) {
                                val name = data.asJsonObject?.get("name")?.asString ?: ""
                                Log.d(TAG, "onResponse register -> name: $name, token: $token")
                                renewUserToken(token)
                                callback.onSuccess(name)
                            } else {
                                Log.d(TAG, "onResponse register -> statusCode: ${response.code()}, body: ${response.raw()}")
                                callback.onError(resources.getString(R.string.general_error_unknown))
                            }
                        } else {
                            val errorBody = response.errorBody()?.string() ?: ""
                            callback.onError(resources.getString(R.string.general_error_unknown))//ApiErrorParser.getMessageForAuth(errorBody))
                            Log.d(TAG, "onResponse register -> statusCode: ${response.code()}, errorBody: $errorBody")
                        }
                    }
                }
        )
    }

    fun clearUserSession() {
        appPreference.clearUserToken()
        userToken = ""
    }

    fun getUserProfile() {
        //TODO: get profil from api
    }
}