package illiyin.mhandharbeni.bicapps.data.repository

import illiyin.mhandharbeni.bicapps.data.model.PaymentRecord
import illiyin.mhandharbeni.bicapps.data.model.Program

class PaymentRecordRepository private constructor() {

    companion object {
        fun getInstance() = PaymentRecordRepository()
    }

    fun getPaidPrograms(): List<Program> {
        //TODO: get list of purchased program from api
        return listOf(
                Program(1, "SMA", "Content 1", 1000, 500),
                Program(2, "Program SBMPTN Saintek", "Content 2", 2000, 1000),
                Program(3, "Mandiri PTN / PTS", "Content 3", 3000, 1500)
        )
    }

    fun getPaymentRecords(program: Program): List<PaymentRecord> {
        //TODO: get payment record of specified program from api
        println(program)
        return listOf(
                PaymentRecord("Pendaftaran", "Rp. 25.000", true),
                PaymentRecord("Pembayaran 1", "Rp. 50.000", true),
                PaymentRecord("Pembayaran 2", "Rp. 150.000", false),
                PaymentRecord("Pembayaran 3", "Rp. 250.000", false)
        )
    }


}