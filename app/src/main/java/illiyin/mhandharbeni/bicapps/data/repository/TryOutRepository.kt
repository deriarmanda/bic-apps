package illiyin.mhandharbeni.bicapps.data.repository

import illiyin.mhandharbeni.bicapps.data.model.Question
import illiyin.mhandharbeni.bicapps.data.model.Ranking

class TryOutRepository private constructor() {

    companion object {
        fun getInstance() = TryOutRepository()
    }

    fun getListTryOut(): List<String> {
        //TODO: get list of try out from api
        return listOf(
                "Try Out 1",
                "Try Out 2",
                "Try Out 3",
                "Try Out 4",
                "Try Out 5"
        )
    }

    fun getListQuestion(tryout: String): ArrayList<Question> {
        //TODO: get list question of try out from api
        return arrayListOf(
                Question("$tryout Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "benar"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "benar"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "salah"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "benar"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "salah"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "salah"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "salah"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "benar"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "salah"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "benar"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "salah"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "benar"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "benar"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "salah"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "benar")
        )
    }

    fun getResultDescription(): String {
        //TODO: get result desc from api
        return "Lorem ipsum dolor sit amet, te has omnis delectus, delectus deseruisse in sea.\n" +
                "        Ad vero ipsum salutandi mea. Vix et aeque graeci putent.\n" +
                "        Nec eros sale omnes ei, id persecuti mediocritatem duo. Ad nec sint vulputate."
    }

    fun getListRanking(): ArrayList<Ranking> {
        //TODO: get list of try out rank from api
        return arrayListOf(
                Ranking("Deri Armanda", 90),
                Ranking("Deri Armanda", 90),
                Ranking("Deri Armanda", 90),
                Ranking("Deri Armanda", 90),
                Ranking("Deri Armanda", 90),
                Ranking("Deri Armanda", 90),
                Ranking("Deri Armanda", 90),
                Ranking("Deri Armanda", 90),
                Ranking("Deri Armanda", 90),
                Ranking("Deri Armanda", 90),
                Ranking("Deri Armanda", 90),
                Ranking("Deri Armanda", 90)
        )
    }
}