package illiyin.mhandharbeni.bicapps.data.network.service

import illiyin.mhandharbeni.bicapps.data.model.Pengumuman
import illiyin.mhandharbeni.bicapps.data.model.response.CommonResult
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface PengumumanService {

    @GET("pengumuman")
    fun getListPengumuman(): Call<CommonResult<List<Pengumuman>>>

    @GET("pengumuman/{pengumuman_id}")
    fun getDetailPengumuman(@Path("{pengumuman_id}") id: Int): Call<CommonResult<Pengumuman>>
}