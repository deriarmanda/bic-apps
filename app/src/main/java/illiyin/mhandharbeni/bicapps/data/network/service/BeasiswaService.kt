package illiyin.mhandharbeni.bicapps.data.network.service

import illiyin.mhandharbeni.bicapps.data.model.Beasiswa
import illiyin.mhandharbeni.bicapps.data.model.response.CommonResult
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface BeasiswaService {

    @GET("beasiswa")
    fun getListBeasiswa(): Call<CommonResult<List<Beasiswa>>>

    @GET("beasiswa/{beasiswa_id}")
    fun getDetailBeasiswa(@Path("{beasiswa_id}") id: Int): Call<CommonResult<Beasiswa>>
}