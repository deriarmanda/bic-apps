package illiyin.mhandharbeni.bicapps.data.repository

import android.content.Context
import illiyin.mhandharbeni.bicapps.R

class MateriRepository private constructor(private val mContext: Context) {

    companion object {
        fun getInstance(context: Context) = MateriRepository(context)
    }

    fun getListMateri(): List<String> {
        //TODO: get list of materi from api
        return mContext.resources.getStringArray(R.array.list_task_my_bic).toList()
    }

    fun getSubListMateri(materi: String): List<String> {
        //TODO: get sublist of selected materi from api
        return listOf(
                "Bab 1 $materi",
                "Bab 2 $materi",
                "Bab 3 $materi",
                "Bab 4 $materi",
                "Bab 5 $materi",
                "Bab 6 $materi",
                "Bab 7 $materi",
                "Bab 8 $materi",
                "Bab 9 $materi",
                "Bab 10 $materi"
        )
    }
}