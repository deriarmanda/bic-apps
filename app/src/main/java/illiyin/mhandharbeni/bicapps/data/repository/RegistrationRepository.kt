package illiyin.mhandharbeni.bicapps.data.repository

import illiyin.mhandharbeni.bicapps.data.model.Program

class RegistrationRepository private constructor() {

    companion object {
        fun getInstance() = RegistrationRepository()
    }

    fun getListProgram() = listOf(Program(1, "Title 1", "Content 1", 1000, 500))

    fun getListJurusan() = listOf("Jurusan A")

    fun getListUniv() = listOf("Univ A")
}