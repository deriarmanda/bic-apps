package illiyin.mhandharbeni.bicapps.data.repository

import illiyin.mhandharbeni.bicapps.data.model.Question

class GradeTestRepository private constructor() {

    companion object {
        fun getInstance() = GradeTestRepository()
    }

    fun getPageStatus(): Boolean {
        //TODO: get payment status of this page
        return false
    }

    fun getListQuestion(): ArrayList<Question> {
        //TODO: get list question of Grade Test from api
        return arrayListOf(
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "benar"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "benar"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "salah"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "benar"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "salah"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "salah"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "salah"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "benar"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "salah"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "benar"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "salah"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "benar"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "benar"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "salah"),
                Question("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                        "benar")
        )
    }

    fun getResultDescription(): String {
        //TODO: get result desc from api
        return "Lorem ipsum dolor sit amet, te has omnis delectus, delectus deseruisse in sea.\n" +
                "        Ad vero ipsum salutandi mea. Vix et aeque graeci putent.\n" +
                "        Nec eros sale omnes ei, id persecuti mediocritatem duo. Ad nec sint vulputate."
    }
}