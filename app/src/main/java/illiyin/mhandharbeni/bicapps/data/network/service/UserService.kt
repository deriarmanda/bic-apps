package illiyin.mhandharbeni.bicapps.data.network.service

import com.google.gson.JsonElement
import illiyin.mhandharbeni.bicapps.data.model.request.LoginUser
import illiyin.mhandharbeni.bicapps.data.model.request.RegisterUser
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface UserService {

    @POST("login")
    fun login(@Body user: LoginUser): Call<JsonElement>

    @POST("register")
    fun register(@Body user: RegisterUser): Call<JsonElement>
}