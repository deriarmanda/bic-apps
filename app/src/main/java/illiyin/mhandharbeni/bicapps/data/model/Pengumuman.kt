package illiyin.mhandharbeni.bicapps.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Pengumuman(
        @SerializedName("id") val id: Int,
        @SerializedName("judul") val title: String,
        @SerializedName("pesan") val content: String
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readString() ?: "",
            parcel.readString() ?: "")

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(title)
        parcel.writeString(content)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Pengumuman> {
        override fun createFromParcel(parcel: Parcel): Pengumuman {
            return Pengumuman(parcel)
        }

        override fun newArray(size: Int): Array<Pengumuman?> {
            return arrayOfNulls(size)
        }
    }
}