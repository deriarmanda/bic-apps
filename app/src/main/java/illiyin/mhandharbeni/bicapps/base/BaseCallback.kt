package illiyin.mhandharbeni.bicapps.base

interface BaseCallback<T> {
    fun onError(message: String)
    fun onSuccess(data: T)
}