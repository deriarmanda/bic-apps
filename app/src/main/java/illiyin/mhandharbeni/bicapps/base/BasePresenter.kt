package illiyin.mhandharbeni.bicapps.base

interface BasePresenter {

    fun start()
}