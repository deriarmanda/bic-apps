package illiyin.mhandharbeni.bicapps.base

interface BaseView<T> {

    val presenter: T
    fun setLoadingIndicator(active: Boolean)
}